#!/usr/bin/env python
import argparse
import configparser
import os
import platform
import json
import sys

def flag(command_list: list[str], flag: str, key = "", value = ""):
	if   key == "" and value == "": command_list.append(f'-{flag}')
	elif key != "" and value == "": command_list.append(f'-{flag}:{key}')
	elif key != "" and value != "": command_list.append(f'-{flag}:{key}={value}')
	else: raise Exception("Unexpected combination of key and value")

DEFAULT_INI_CONTENTS = """[general]
entry_point = src/main
output = bin/main

[collections]
# Default collections, ignored if not generating ols.json
core = C:/bin/odin/core
vendor = C:/bin/odin/vendor

[defines]
TRACK_ALLOCATIONS = true
"""

def get_config():
	config = configparser.ConfigParser()
	config.optionxform = str
	config.read("config.ini")

	for category in ["general", "collections", "defines"]:
		if not category in config: config[category] = {}

	return config

def init():
	with open("config.ini", "w") as f:
		f.write(DEFAULT_INI_CONTENTS)

def ols():
	config = get_config()

	collections = []
	for key in config["collections"]:
		collections.append({
			"name": key,
			"path": config["collections"][key],
		})

	with open("ols.json", "w") as f:
		f.write(json.dumps({
				"collections": collections,
				# Default config, but with semantic tokens
				"enable_document_symbols": True,
				"enable_semantic_tokens": True,
				"enable_hover": True,
				"enable_snippets": True,
			}, indent=4))

def compile(args):
	config = get_config()

	entry = config["general"].get("entry_point", "src")
	command = ["odin", args.action, entry]

	if args.action == "check":
		# flag(command, "vet")
		# flag(command, "vet-extra") # Note: has false positives
		pass
	else:
		out_name = config["general"].get("output", "out")

		if args.profile == "debug":
			out_name += "_deb"
			flag(command, "o", "none")
			flag(command, "debug")
		elif args.profile == "release":
			out_name += "_rel"
			flag(command, "o", "speed")

		if platform.system() == "Windows":
			out_name += ".exe"

		try:
			os.remove(out_name)
		except Exception:
			pass # I don't care, not even a little bit

		flag(command, "out", out_name)

	for key in config["collections"]:
		# Skip core and vendor which may be present for generating ols.json
		if key != "core" and key != "vendor":
			flag(command, "collection", key, config["collections"][key])

	for key in config["defines"]:
		flag(command, "define", key, config["defines"][key])

	command_string = " ".join(command)
	os.system("clear")
	print(command_string + "\n")
	os.environ["ODIN_ERROR_POS_STYLE"] = "unix"
	os.system(command_string)

def main():
	assert(sys.version_info.major == 3 and sys.version_info.minor >= 10)

	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest="action", required=True)

	init_action = subparsers.add_parser("init")
	ols_action = subparsers.add_parser("ols")

	compile_arguments = argparse.ArgumentParser(add_help=False)
	compile_arguments.add_argument("profile", choices=["debug", "release"])

	build_action = subparsers.add_parser("build", parents=[compile_arguments])
	run_action = subparsers.add_parser("run", parents=[compile_arguments])
	check_action = subparsers.add_parser("check", parents=[compile_arguments])

	args = parser.parse_args()

	if args.action == "init":
		init()
	elif args.action == "ols":
		ols()
	elif args.action in ["build", "run", "check"]:
		compile(args)

if __name__ == "__main__": main()
