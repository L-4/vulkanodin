# Only for shaders.
SHADERC := glslc
SHADERC_ARGS := --target-env=vulkan1.1 -g

# shader build
FRAG_FILES := $(shell find shaders_src/ -type f -name "*.frag" | cut -d '/' -f2-)
VERT_FILES := $(shell find shaders_src/ -type f -name "*.vert" | cut -d '/' -f2-)
COMP_FILES := $(shell find shaders_src/ -type f -name "*.comp" | cut -d '/' -f2-)
SPV_FILES := $(patsubst %.frag,shaders/%.frag.spv,$(FRAG_FILES)) $(patsubst %.vert,shaders/%.vert.spv,$(VERT_FILES)) $(patsubst %.comp,shaders/%.comp.spv,$(COMP_FILES))

$(info $(SPV_FILES))

shaders/%.frag.spv: shaders_src/%.frag
	@mkdir -p shaders
	$(SHADERC) $(SHADERC_ARGS) $< -o $@

shaders/%.vert.spv: shaders_src/%.vert
	@mkdir -p shaders
	$(SHADERC) $(SHADERC_ARGS) $< -o $@

shaders/%.comp.spv: shaders_src/%.comp
	@mkdir -p shaders
	$(SHADERC) $(SHADERC_ARGS) $< -o $@

shaders: $(SPV_FILES)

.DEFAULT_GOAL = shaders

clean:
	rm shaders/*.spv

.PHONY: clean
