package nt2

import vk "vendor:vulkan"

MAX_DESCRIPTOR_SETS :: 4

Pipeline :: struct {
	pipeline_layout: vk.PipelineLayout,
	pipeline_handle: vk.Pipeline,
}

graphics_pipeline_create :: proc(
	shader_stages: []vk.PipelineShaderStageCreateInfo,
	in_descriptor_set_layouts: []vk.DescriptorSetLayout,
	$Vertex_Type: typeid,
	push_constant: ^vk.PushConstantRange,
	color_attachment_count: int,
	rt_descriptor: Render_Target_Descriptor,
) -> Pipeline {
	using graphics_pipeline := Pipeline{}

	binding_description: vk.VertexInputBindingDescription
	attribute_descriptions_static: [8]vk.VertexInputAttributeDescription
	attribute_descriptions := attribute_descriptions_static[:]

	fill_vertex_binding_attribute_descriptions(Vertex_Type, &binding_description, &attribute_descriptions)

	vertex_input_info := vk.PipelineVertexInputStateCreateInfo {
		sType                           = .PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		vertexBindingDescriptionCount   = 1,
		pVertexBindingDescriptions      = &binding_description,
		vertexAttributeDescriptionCount = cast(u32)len(attribute_descriptions),
		pVertexAttributeDescriptions    = raw_data(attribute_descriptions),
	}

	input_assembly_state := vk.PipelineInputAssemblyStateCreateInfo {
		sType                  = .PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		topology               = .TRIANGLE_LIST,
		primitiveRestartEnable = false,
	}

	viewport_state := vk.PipelineViewportStateCreateInfo {
		sType         = .PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		viewportCount = 1, // This is set dynamically, but we have to say that we only have one viewport.
		scissorCount  = 1,
		pScissors     = nil, // Dynamic
	}

	rasterization_state := vk.PipelineRasterizationStateCreateInfo {
		sType                   = .PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		depthClampEnable        = false,
		rasterizerDiscardEnable = false,
		polygonMode             = .FILL,
		lineWidth               = 1.0,
		cullMode                = {.BACK},
		frontFace               = .CLOCKWISE,
		depthBiasEnable         = false,
	}

	multisample_state := vk.PipelineMultisampleStateCreateInfo {
		sType                 = .PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		sampleShadingEnable   = false,
		rasterizationSamples  = {._1},
		minSampleShading      = 1.0,
		pSampleMask           = nil,
		alphaToCoverageEnable = false,
		alphaToOneEnable      = false,
	}

	ATTACHMENT_BLEND_STATE :: vk.PipelineColorBlendAttachmentState {
		colorWriteMask = {.R, .G, .B, .A},
		blendEnable    = false,
	}

	attachment_blend_states := make([]vk.PipelineColorBlendAttachmentState, color_attachment_count, context.temp_allocator)

	for i in 0..<color_attachment_count {
		attachment_blend_states[i] = ATTACHMENT_BLEND_STATE
	}

	color_blend_state := vk.PipelineColorBlendStateCreateInfo {
		sType           = .PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		logicOpEnable   = false,
		attachmentCount = cast(u32)len(attachment_blend_states),
		pAttachments    = raw_data(attachment_blend_states),
	}

	depth_stencil := vk.PipelineDepthStencilStateCreateInfo {
		sType                 = .PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		depthTestEnable       = false,
		depthWriteEnable      = false,
		depthCompareOp        = .ALWAYS,
		depthBoundsTestEnable = false,
		stencilTestEnable     = false,
	}

	dynamic_states := []vk.DynamicState{.VIEWPORT, .SCISSOR}

	dynamic_state := vk.PipelineDynamicStateCreateInfo {
		sType             = .PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		dynamicStateCount = cast(u32)len(dynamic_states),
		pDynamicStates    = raw_data(dynamic_states),
	}

	push_constant_count := 1 if push_constant != nil else 0

	vk_assert(vk.CreatePipelineLayout(vk_ctx.device, &vk.PipelineLayoutCreateInfo {
		sType                  = .PIPELINE_LAYOUT_CREATE_INFO,
		pSetLayouts            = raw_data(in_descriptor_set_layouts),
		setLayoutCount         = cast(u32)len(in_descriptor_set_layouts),
		pPushConstantRanges    = push_constant,
		pushConstantRangeCount = cast(u32)push_constant_count,
	}, nil, &pipeline_layout))

	pipeline_info := vk.GraphicsPipelineCreateInfo {
		sType               = .GRAPHICS_PIPELINE_CREATE_INFO,
		stageCount          = 2,
		pStages             = raw_data(shader_stages),
		pVertexInputState   = &vertex_input_info,
		pInputAssemblyState = &input_assembly_state, // static?
		pViewportState      = &viewport_state, // static
		pRasterizationState = &rasterization_state, // static
		pMultisampleState   = &multisample_state, // static?
		pDepthStencilState  = &depth_stencil, // static
		pColorBlendState    = &color_blend_state, // static?
		pDynamicState       = &dynamic_state, // static?
		layout              = pipeline_layout,
		renderPass          = 0, // Dynamic rendering
	}

	rt_descriptor := rt_descriptor
	dynamic_rendering_info := render_target_get_pipeline_rendering_info(&rt_descriptor)

	pipeline_info.pNext = auto_cast &dynamic_rendering_info

	vk_assert(vk.CreateGraphicsPipelines(vk_ctx.device, g_pipeline_cache, 1, &pipeline_info, nil, &pipeline_handle))

	return graphics_pipeline
}
