package nt2

import vk "vendor:vulkan"

// TODO[TS]: samples: vk.SampleCountFlags for MSAA not included yet!
// How to handle this in practice? Isn't it only for render targets?
// TODO[TS]: When creating the image, we pass memory properties, but these
// aren't stored anywhere.
// Presumably we'll be able to query this from a vma.Allocation eventually,
// so we don't store this for now.
Image :: struct {
	memory:    vk.DeviceMemory, // OR memory: vma.Allocation,
	image:     vk.Image,
	full_view: vk.ImageView, // Defaulted ImageView which refers to full resource

	type:         vk.ImageType,
	format:       vk.Format,
	extent:       vk.Extent3D,
	mip_levels:   u32,
	array_layers: u32,
	// tiling: assumed .OPTIMAL
	usage: vk.ImageUsageFlags,
	// sharing_mode: assumed .EXCLUSIVE
	// queue family is constant
	layout: vk.ImageLayout,
}

image_create :: proc(
	type: vk.ImageType,
	format: vk.Format,
	extent: vk.Extent3D,
	mip_levels: u32,
	array_layers: u32,
	usage: vk.ImageUsageFlags,
	memory_properties: vk.MemoryPropertyFlags,
	loc := #caller_location) -> Image {

	image := Image {
		type         = type,
		format       = format,
		extent       = extent,
		mip_levels   = mip_levels,
		array_layers = array_layers,
		usage        = usage,
		layout       = .UNDEFINED,
	}

	vk_assert(vk.CreateImage(vk_ctx.device, &vk.ImageCreateInfo {
		sType                 = .IMAGE_CREATE_INFO,
		pNext                 = nil,
		flags                 = {},
		imageType             = type,
		format                = format,
		extent                = extent,
		mipLevels             = mip_levels,
		arrayLayers           = array_layers,
		samples               = {._1},
		tiling                = .OPTIMAL,
		usage                 = usage,
		sharingMode           = .EXCLUSIVE,
		queueFamilyIndexCount = 0,   // ignored: sharingMode = .EXCLUSIVE
		pQueueFamilyIndices   = nil, // ignored: sharingMode = .EXCLUSIVE
		initialLayout         = .UNDEFINED,
	}, nil, &image.image))

	image.memory = memory_allocate_image_backing_memory(image.image, memory_properties)
	image.full_view = image_view_create_full_view(image)

	vk_name_object(image.image, "Image VkImage created at {}", loc)
	vk_name_object(image.memory, "Image VkDeviceMemory created at {}", loc)
	vk_name_object(image.full_view, "Image default VkImageView create at {}", loc)

	return image
}

image_destroy :: proc(image: Image) {
	vk.DestroyImageView(vk_ctx.device, image.full_view, nil)
	vk.DestroyImage(vk_ctx.device, image.image, nil)
	vk.FreeMemory(vk_ctx.device, image.memory, nil)
}

// Shorthand for creating the most common type of image
image_create_2d :: proc(
	format: vk.Format,
	w, h: u32,
	usage: vk.ImageUsageFlags,
	memory_properties: vk.MemoryPropertyFlags,
	mip_levels := u32(1),
	loc := #caller_location) -> Image {

	return image_create(
		type              = .D2,
		format            = format,
		extent            = vk.Extent3D { width = w, height = h, depth = 1 },
		mip_levels        = mip_levels,
		array_layers      = 1,
		usage             = usage,
		memory_properties = memory_properties,
		loc               = loc)
}

// A helper for the common case of uploading data to a newly created image
_image_transition_from_undefined_to_transfer_dst :: proc(
	command_buffer: vk.CommandBuffer,
	image: ^Image) {

	image_transition_layout(
		command_buffer = command_buffer,
		image          = image,
		old_layout     = .UNDEFINED, // We just created it in undefined
		new_layout     = .TRANSFER_DST_OPTIMAL,
		// We have no prior dependency, so use earliest possible stage, with no access dependencies
		src_stages     = {.TOP_OF_PIPE},
		src_access     = {},
		// Our next operation will be uploading to the texture, so transition before we hit the transfer stage
		dst_stages     = {.TRANSFER},
		dst_access     = {.TRANSFER_WRITE})
}

// A helper for the common case of transition back after uploading to a texture
_image_transition_from_transfer_dst_to_shader_read_only :: proc(
	command_buffer: vk.CommandBuffer,
	image: ^Image) {

	image_transition_layout(
		command_buffer = command_buffer,
		image          = image,
		old_layout     = .TRANSFER_DST_OPTIMAL,
		new_layout     = .SHADER_READ_ONLY_OPTIMAL,
		// We need to complete our transfer operation before we can transition
		src_stages     = {.TRANSFER},
		src_access     = {.TRANSFER_WRITE},
		// We will next probably use this for sampling
		dst_stages     = {.ALL_GRAPHICS}, // TODO[TS]: Seems overkill?
		dst_access     = {.SHADER_READ})
}

image_create_from_bytes :: proc(width, height: u32, format: vk.Format, pixels: []u8, loc := #caller_location) -> Image {
	image := image_create_2d(format, width, height, {.TRANSFER_DST, .SAMPLED}, {.DEVICE_LOCAL}, 1, loc)

	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	_image_transition_from_undefined_to_transfer_dst(command_buffer, &image)
	staging_buffer_copy_to_image_single_mip_slice(command_buffer, pixels, image, 0, 0)
	_image_transition_from_transfer_dst_to_shader_read_only(command_buffer, &image)

	return image
}

image_get_full_subresource :: proc(image: Image, aspect: vk.ImageAspectFlags) -> vk.ImageSubresourceRange {
	return vk.ImageSubresourceRange {
		aspectMask     = aspect,
		baseMipLevel   = 0,
		levelCount     = image.mip_levels,
		baseArrayLayer = 0,
		layerCount     = image.array_layers,
	}
}
