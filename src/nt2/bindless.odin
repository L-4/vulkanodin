package nt2

// import vk "vendor:vulkan"

// // set 0 = sampled images
// // set 1 = samplers
// // set 2 = ssbo

// Bindless_Descriptor_State :: struct {
// 	allocated: bool,
// 	// TODO[TS]: last_used_in_frame, when I understand how to do it properly

// 	resource: struct #raw_union {
// 		buffer: vk.Buffer,
// 		sampler: vk.Sampler,
// 	},
// }

// Bindless_Descriptor_Set :: struct {
// 	count: u32,
// 	layout: vk.DescriptorSetLayout,
// 	pool: vk.DescriptorPool,
// 	set: vk.DescriptorSet,
// }

// _bindless_create_descriptor_set :: proc(type: vk.DescriptorType, initial_descriptor_count: u32) -> Bindless_Descriptor_Set {
// 	set: Bindless_Descriptor_Set
// 	set.count = initial_descriptor_count

// 	set.layout = descriptor_set_layout_create([]Descriptor_Set_Layout_Info{
// 		{ type = type, count = 0, stage_flags = {.VERTEX,.FRAGMENT,.COMPUTE}, flags = {.PARTIALLY_BOUND, .VARIABLE_DESCRIPTOR_COUNT}},
// 	})

// 	// TODO[TS]: What's the benefit of having a pool per frame in flight?
// 	// Updating the set is synchronous, so it would only maybe be useful if you update a resource mid-frame?
// 	set.pool = descriptor_pool_create([]vk.DescriptorPoolSize{
// 		{ type = type, descriptorCount = set.count },
// 	}, 1)

// 	set.set = descriptor_set_allocate(set.pool, set.layout)

// 	return set
// }

// _bindless_destroy_descriptor_set :: proc(set: ^Bindless_Descriptor_Set) {
// 	vk.DestroyDescriptorSetLayout(vk_ctx.device, set.layout, nil)
// }

// Bindless_Data :: struct {
// 	bindless_images: Bindless_Descriptor_Set,
// 	bindless_samplers: Bindless_Descriptor_Set,
// 	bindless_buffers: Bindless_Descriptor_Set,
// }

// bindless_initialize :: proc(system_data: rawptr) {
// 	system_data := cast(^Bindless_Data)system_data

// 	system_data.bindless_images = _bindless_create_descriptor_set(.SAMPLED_IMAGE, 1000)
// 	system_data.bindless_samplers = _bindless_create_descriptor_set(.SAMPLER, 1000)
// 	system_data.bindless_buffers = _bindless_create_descriptor_set(.STORAGE_BUFFER, 1000)
// }

// bindless_deinitialize :: proc(system_data: rawptr) {
// 	system_data := cast(^Bindless_Data)system_data

// 	defer _bindless_destroy_descriptor_set(&system_data.bindless_images)
// 	defer _bindless_destroy_descriptor_set(&system_data.bindless_samplers)
// 	defer _bindless_destroy_descriptor_set(&system_data.bindless_buffers)
// }
