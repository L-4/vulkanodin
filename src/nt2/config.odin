package nt2

// Contains valid flags with defaults. This makes so that typos aren't possible.

VK_USE_DEVSIM               :: #config(VK_USE_DEVSIM, false)
VK_USE_EXT_DEBUG_UTILS      :: #config(VK_USE_EXT_DEBUG_UTILS, false)
VK_USE_VALIDATION           :: #config(VK_USE_VALIDATION, false)
VK_USE_EXTENDED_VALIDATION  :: #config(VK_USE_EXTENDED_VALIDATION, false)
VK_API_DUMP                 :: #config(VK_API_DUMP, false)
ENABLE_IMGUI                :: #config(ENABLE_IMGUI, false)
