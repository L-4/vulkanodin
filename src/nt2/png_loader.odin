package nt2

import vk "vendor:vulkan"

import "core:image/png"
import "core:bytes"
import "core:log"

Png_Load_Error :: union {
	png.Error,
	enum {
		Dimensions_Zero,
		Invalid_Channel_Count,
	},
}

png_load_from_file :: proc(file_name: string) -> (image: Image, err: Png_Load_Error) {
	png_image := png.load_from_file(file_name, {.alpha_add_if_missing}) or_return
	defer png.destroy(png_image)

	image_size := cast(vk.DeviceSize)(png_image.width * png_image.height * png_image.channels)

	if image_size == 0 {
		log.errorf("Loaded texture '%s', but image size was zero!\n", file_name)
		err = .Dimensions_Zero
		return
	}

	if png_image.channels != 4 {
		err = .Invalid_Channel_Count
		return
	}

	pixels := bytes.buffer_to_bytes(&png_image.pixels)

	image = image_create_2d(
		.R8G8B8A8_UNORM,
		u32(png_image.width),
		u32(png_image.height),
		{.TRANSFER_DST, .SAMPLED},
		{.DEVICE_LOCAL})

	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	_image_transition_from_undefined_to_transfer_dst(command_buffer, &image)
	staging_buffer_copy_to_image_single_mip_slice(command_buffer, pixels, image, 0, 0)
	_image_transition_from_transfer_dst_to_shader_read_only(command_buffer, &image)

	vk_name_object(image.image, "Image '%s'", file_name)
	vk_name_object(image.memory, "Image Memory '%s'", file_name)

	return
}
