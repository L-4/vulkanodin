package nt2

import vk "vendor:vulkan"

@(private="file")
default_command_pools: [FIF_COUNT]vk.CommandPool

command_buffer_initialize_default_pool :: proc() {
	pool_info := vk.CommandPoolCreateInfo {
		sType            = .COMMAND_POOL_CREATE_INFO,
		queueFamilyIndex = vk_ctx.combined_queue.family,
	}

	for fif_idx in 0..<FIF_COUNT {
		vk.CreateCommandPool(vk_ctx.device, &pool_info, nil, &default_command_pools[fif_idx])
		vk_name_object(default_command_pools[fif_idx], "Default command pool for frame {}", fif_idx)
	}
}

command_buffer_deinitialize_default_pool :: proc() {
	for fif_idx in 0..<FIF_COUNT {
		vk.DestroyCommandPool(vk_ctx.device, default_command_pools[fif_idx], nil)
	}
}

command_buffer_reset_default_pool_for_frame :: proc() {
	vk.ResetCommandPool(vk_ctx.device, default_command_pools[fif_get_current_index()], {})
}

command_buffer_allocate :: proc() -> vk.CommandBuffer {
	command_buffer_info := vk.CommandBufferAllocateInfo {
		sType              = .COMMAND_BUFFER_ALLOCATE_INFO,
		commandPool        = default_command_pools[fif_get_current_index()],
		level              = .PRIMARY,
		commandBufferCount = 1,
	}

	command_buffer: vk.CommandBuffer
	vk_assert(vk.AllocateCommandBuffers(vk_ctx.device, &command_buffer_info, &command_buffer))

	return command_buffer
}

command_buffer_begin_single_use :: proc(loc := #caller_location) -> vk.CommandBuffer {
	command_buffer := command_buffer_allocate()
	vk_name_object(command_buffer, "Single use command buffer: %v", loc)

	begin_info := vk.CommandBufferBeginInfo {
		sType = .COMMAND_BUFFER_BEGIN_INFO,
		flags = {.ONE_TIME_SUBMIT},
	}

	vk.BeginCommandBuffer(command_buffer, &begin_info)

	return command_buffer
}

command_buffer_end_single_use :: proc(command_buffer: vk.CommandBuffer) {
	command_buffer := command_buffer

	vk_assert(vk.EndCommandBuffer(command_buffer))

	submit_info := vk.SubmitInfo {
		sType              = .SUBMIT_INFO,
		commandBufferCount = 1,
		pCommandBuffers    = &command_buffer,
	}

	vk.QueueSubmit(vk_ctx.combined_queue.queue, 1, &submit_info, 0)
	vk.QueueWaitIdle(vk_ctx.combined_queue.queue)
}
