package nt2

import vk "vendor:vulkan"

import "core:log"

vk_assert :: proc(result: vk.Result) {
	if result == .SUCCESS do return

	log.panicf("VkResult != Success! Was %v", result);
}

size_of_slice :: proc(s: $S/[]$T) -> int {
	return size_of(T) * len(s)
}
