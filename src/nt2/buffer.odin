package nt2

import vk "vendor:vulkan"

import "core:slice"

buffer_create_multiple :: proc(
	size_per_buffer: vk.DeviceSize,
	usage: vk.BufferUsageFlags,
	properties: vk.MemoryPropertyFlags,
	buffers_out: []vk.Buffer,
	loc := #caller_location,
) -> vk.DeviceMemory {
	assert(len(buffers_out) > 0)

	buffer_info := vk.BufferCreateInfo {
		sType = .BUFFER_CREATE_INFO,
		size = size_per_buffer,
		usage = usage,
		sharingMode = .EXCLUSIVE,
	}

	for buffer_idx in 0..<len(buffers_out) {
		buffers_out[buffer_idx] = vk_create_buffer(&buffer_info, loc)
	}

	return memory_allocate_buffer_backing_memory(buffers_out, properties)
}

buffer_create_single :: proc(
	size: vk.DeviceSize,
	usage: vk.BufferUsageFlags,
	properties: vk.MemoryPropertyFlags,
	loc := #caller_location,
) -> (buffer: vk.Buffer, memory: vk.DeviceMemory) {

	buffer_array := []vk.Buffer{buffer}

	memory = buffer_create_multiple(size, usage, properties, buffer_array, loc)

	return buffer_array[0], memory
}

// Creates a device local buffer with given usage.
buffer_create_static :: proc(
	buffer_data: $T/[]$E,
	usage := vk.BufferUsageFlags {},
) -> (vk.Buffer, vk.DeviceMemory) {
	staging_buffer, offset, size := staging_buffer_upload_data(to_bytes(buffer_data))

	// Create our target buffer + memory
	buffer, memory := buffer_create_single(size, usage | {.TRANSFER_DST}, {.DEVICE_LOCAL})

	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	vk.CmdCopyBuffer(command_buffer, staging_buffer, buffer, 1, &vk.BufferCopy {
		srcOffset = offset,
		dstOffset = 0,
		size      = size,
	})

	return buffer, memory
}
