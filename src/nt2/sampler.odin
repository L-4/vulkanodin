package nt2

import vk "vendor:vulkan"

sampler_create :: proc() -> vk.Sampler {
	sampler_info := vk.SamplerCreateInfo {
		sType = .SAMPLER_CREATE_INFO,
		magFilter = .LINEAR,
		minFilter = .LINEAR,
		addressModeU = .REPEAT,
		addressModeV = .REPEAT,
		addressModeW = .REPEAT,
		anisotropyEnable = true,
		maxAnisotropy = vk_ctx.physical_device_properties.limits.maxSamplerAnisotropy,
		borderColor = .INT_OPAQUE_BLACK,
		unnormalizedCoordinates = false,
		compareEnable = false,
		compareOp = .ALWAYS,
		mipmapMode = .LINEAR,
		mipLodBias = 0,
		minLod = 0,
		maxLod = 0,
	}

	sampler: vk.Sampler
	vk.CreateSampler(vk_ctx.device, &sampler_info, nil, &sampler)

	return sampler
}

sampler_default_linear: vk.Sampler

sampler_create_default_samplers :: proc() {
	sampler_default_linear = sampler_create()
}
sampler_destroy_default_samplers :: proc() {
	vk.DestroySampler(vk_ctx.device, sampler_default_linear, nil)
}

