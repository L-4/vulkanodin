package nt2

import vk "vendor:vulkan"

RENDER_TARGET_MAX_ATTACHMENTS :: 8

Render_Target_Descriptor :: struct {
	size: vk.Extent2D,
	color_formats: Small_Array(RENDER_TARGET_MAX_ATTACHMENTS, vk.Format),

	has_depth:   bool,
	has_stencil: bool,
}

Render_Target :: struct {
	using descriptor:  Render_Target_Descriptor,
	color_attachments: Small_Array(RENDER_TARGET_MAX_ATTACHMENTS, Image),
	depth_stencil:     Image,
}

render_target_create :: proc(descriptor: Render_Target_Descriptor) -> Render_Target {
	render_target := Render_Target {
		descriptor = descriptor,
	}

	render_target.color_attachments.len = descriptor.color_formats.len

	for attachment_format, attachment_idx in _slice(&render_target.color_formats) {
		attachment := image_create_2d(
			attachment_format,
			render_target.size.width,
			render_target.size.width,
			{.COLOR_ATTACHMENT},
			{.DEVICE_LOCAL})
		render_target.color_attachments.data[attachment_idx] = attachment

		vk_name_object(attachment.image, "Render target color attachment image #{} ({})", attachment_idx, attachment_format)
		vk_name_object(attachment.full_view, "Render target color attachment view #{} ({})", attachment_idx, attachment_format)
		vk_name_object(attachment.memory, "Render target color attachment memory #{}", attachment_idx)
	}

	// If we have either/depth, stencil, we always  create the same combined format.
	depth_stencil_format := vk.Format.D24_UNORM_S8_UINT if render_target.has_depth || render_target.has_stencil else .UNDEFINED

	if depth_stencil_format != .UNDEFINED {
		render_target.depth_stencil = image_create_2d(
			depth_stencil_format,
			render_target.size.width,
			render_target.size.height,
			{.DEPTH_STENCIL_ATTACHMENT},
			{.DEVICE_LOCAL})

		vk_name_object(render_target.depth_stencil.image, "Render target depth/stencil attachment ({})", depth_stencil_format)
		vk_name_object(render_target.depth_stencil.full_view, "Render target depth/stencil attachment ({})", depth_stencil_format)
		vk_name_object(render_target.depth_stencil.memory, "Render target depth/stencil attachment memory")
	}

	return render_target
}

render_target_destroy :: proc(render_target: ^Render_Target) {
	if render_target.has_depth || render_target.has_stencil {
		image_destroy(render_target.depth_stencil)
	}

	for attachment in _slice(&render_target.color_attachments) {
		image_destroy(attachment)
	}
}

render_target_get_pipeline_rendering_info :: proc(
	rt_descriptor: ^Render_Target_Descriptor,
) -> vk.PipelineRenderingCreateInfo {
	depth_stencil_format := vk.Format.D24_UNORM_S8_UINT if rt_descriptor.has_depth || rt_descriptor.has_stencil else .UNDEFINED

	return vk.PipelineRenderingCreateInfo {
		sType                   = .PIPELINE_RENDERING_CREATE_INFO,
		pNext                   = nil,
		viewMask                = 0,
		colorAttachmentCount    = u32(_len(rt_descriptor.color_formats)),
		pColorAttachmentFormats = _raw_data(&rt_descriptor.color_formats),
		depthAttachmentFormat   = depth_stencil_format if rt_descriptor.has_depth else .UNDEFINED,
		stencilAttachmentFormat = depth_stencil_format if rt_descriptor.has_stencil else .UNDEFINED,
	}
}

// TODO[TS]: No way to provide load/store ops, clear values
render_target_bind :: proc(
	render_target: ^Render_Target,
	command_buffer: vk.CommandBuffer,
) {
	color_attachments := make(
		[]Attachment_Rendering_Info,
		_len(render_target.color_attachments),
		context.temp_allocator)

	for attachment, i in _slice(&render_target.color_attachments) {
		color_attachments[i] = Attachment_Rendering_Info {
			view        = attachment.full_view,
			load_op     = .CLEAR,
			store_op    = .STORE,
			clear_value = vk.ClearValue { color = { float32 = { 0, 0, 0, 0 } } },
		}
	}

	depth_attachment := Attachment_Rendering_Info {
		view        = render_target.depth_stencil.full_view,
		load_op     = .CLEAR,
		store_op    = .STORE,
		clear_value = vk.ClearValue { depthStencil = { depth = 0, stencil = 0 } },
	}

	stencil_attachment := Attachment_Rendering_Info {
		view        = render_target.depth_stencil.full_view,
		load_op     = .CLEAR,
		store_op    = .STORE,
		clear_value = vk.ClearValue { depthStencil = { depth = 0, stencil = 0 } },
	}

	vk_begin_rendering(
		command_buffer,
		vk.Rect2D { offset = { 0, 0 }, extent = render_target.size },
		color_attachments,
		depth_attachment if render_target.has_depth else nil,
		stencil_attachment if render_target.has_stencil else nil)
}
