package nt2

import vk "vendor:vulkan"

_image_determine_view_type :: proc(
	image_type: vk.ImageType,
	array_layers: u32,
	as_cubemap: bool) -> vk.ImageViewType {

	if as_cubemap {
		if array_layers < 6 {
			panic("Cubemap with < 6 faces!")
		}
		if array_layers == 6 {
			return .CUBE
		} else {
			if (array_layers %% 6) != 0 {
				panic("Cubmap array: array_layers %% 6 != 0!")
			}
			return .CUBE_ARRAY
		}

	} else {
		if array_layers == 1 {
			switch image_type {
				case .D1: return .D1
				case .D2: return .D2
				case .D3: return .D3
			}
		} else {
			switch image_type {
				case .D1: return .D1_ARRAY
				case .D2: return .D2_ARRAY
				// This doesn't exist in vk.ImageViewType!
				case .D3: panic("type == .D3 && array_layers > 1")
			}
		}
	}

	panic("Huh?")
}

// This creates a "simple" view encompassing the entire resource.
// But it might not be the view which the user intended. For instance, you could
// create a 1x10x1 image, intended as a
image_view_create_full_view :: proc(image: Image, as_cubemap := false) -> vk.ImageView {
	view: vk.ImageView
	vk_assert(vk.CreateImageView(vk_ctx.device, &vk.ImageViewCreateInfo {
		sType = .IMAGE_VIEW_CREATE_INFO,
		pNext = nil,
		flags = {},
		image = image.image,
		viewType = _image_determine_view_type(image.type, image.array_layers, as_cubemap),
		format = image.format,
		components = { r = .R, g = .G, b = .B, a = .A },
		subresourceRange = image_get_full_subresource(image, {.COLOR}), // TODO[TS]: TODO-1
	}, nil, &view))

	return view
}
