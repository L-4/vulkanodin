package nt2

import vk "vendor:vulkan"

import "core:log"
import "core:mem"

forward_align :: #force_inline proc(size, alignment: vk.DeviceSize) -> vk.DeviceSize {
	return cast(vk.DeviceSize)mem.align_forward_uintptr( cast(uintptr)size, cast(uintptr)alignment)
}

forward_align_size_to_coherent_atom :: #force_inline proc(size: vk.DeviceSize) -> vk.DeviceSize {
	return forward_align(size, vk_ctx.physical_device_properties.limits.nonCoherentAtomSize)
}

// supported_memory_indices is a bitmask of types supported 1 << i where i is
// the index into vkPhysicalDeviceMemoryProperties.memoryTypes[] array
memory_find_type :: proc(supported_memory_indices: u32, properties: vk.MemoryPropertyFlags) -> u32 {
	memory_properties: vk.PhysicalDeviceMemoryProperties
	vk.GetPhysicalDeviceMemoryProperties(vk_ctx.physical_device, &memory_properties)

	for i in 0..<memory_properties.memoryTypeCount {
		if (supported_memory_indices & (1 << i)) == 0 do continue

		if memory_properties.memoryTypes[i].propertyFlags >= properties {return i}
	}

	// This could be handled, but when would that ever be useful?
	log.panicf("Couldn't choose memory type!")
}

memory_allocate_image_backing_memory :: proc(image: vk.Image, properties: vk.MemoryPropertyFlags) -> vk.DeviceMemory {
	memory_requirements: vk.MemoryRequirements
	vk.GetImageMemoryRequirements(vk_ctx.device, image, &memory_requirements)

	memory := memory_allocate_memory(memory_requirements.size, memory_requirements, properties)
	vk_assert(vk.BindImageMemory(vk_ctx.device, image, memory, 0))

	return memory
}

memory_allocate_buffer_backing_memory :: proc(buffers: []vk.Buffer, properties: vk.MemoryPropertyFlags) -> vk.DeviceMemory {
	assert(len(buffers) > 0)

	first_buffer := buffers[0]

	memory_requirements: vk.MemoryRequirements
	vk.GetBufferMemoryRequirements(vk_ctx.device, first_buffer, &memory_requirements)

	offsets := make([]vk.DeviceSize, len(buffers), context.temp_allocator)

	// Head is the start location of the allocation in the next iteration of the loop.
	// On the last iteration it is instead the total size of the allocation.
	head := vk.DeviceSize(0)

	for buffer_idx in 0..<len(offsets) {
		offsets[buffer_idx] = head

		head += memory_requirements.size

		// If we have another allocation, then we need to align the start
		if buffer_idx + 1 < len(offsets) {
			head = forward_align(head, memory_requirements.alignment)
		}
	}

	memory := memory_allocate_memory(head, memory_requirements, properties)

	for offset, buffer_idx in offsets {
		vk_assert(vk.BindBufferMemory(vk_ctx.device, buffers[buffer_idx], memory, offset))
	}

	return memory
}

@(private="file")
memory_allocate_memory :: proc(
	size: vk.DeviceSize,
	memory_requirements: vk.MemoryRequirements,
	properties: vk.MemoryPropertyFlags,
) -> vk.DeviceMemory {
	memory_info := vk.MemoryAllocateInfo {
		sType           = .MEMORY_ALLOCATE_INFO,
		allocationSize  = size,
		memoryTypeIndex = memory_find_type(memory_requirements.memoryTypeBits, properties),
	}

	memory: vk.DeviceMemory
	vk_assert(vk.AllocateMemory(vk_ctx.device, &memory_info, nil, &memory))

	return memory
}
