package nt2

import sdl "vendor:sdl2"

import "core:log"

Window :: struct {
	sdl_window: ^sdl.Window,
}
g_window: Window

window_initialize :: proc() {
	sdl.InitSubSystem(sdl.INIT_VIDEO)

	bounds: sdl.Rect
	sdl.GetDisplayBounds(0, &bounds)

	g_window.sdl_window = sdl.CreateWindow("nt2_2dg_vk",
	                              sdl.WINDOWPOS_CENTERED,
	                              sdl.WINDOWPOS_CENTERED,
	                              bounds.w,
	                              bounds.h,
	                              {.SHOWN, sdl.WindowFlags.VULKAN})

	if g_window.sdl_window == nil {
		log.panic("Could not create SDL window!")
	}
}

window_deinitialize :: proc() {
	assert(g_window.sdl_window != nil)

	sdl.DestroyWindow(g_window.sdl_window)
}

// TODO[TS], p4: This is super temporary and will definitely break!
window_get_size :: proc() -> (w, h: int) {
	bounds: sdl.Rect
	sdl.GetDisplayBounds(0, &bounds)

	return int(bounds.w), int(bounds.h)
}
