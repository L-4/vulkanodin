package nt2

import vk "vendor:vulkan"

import "core:log"
import "core:os"

PIPELINE_CACHE_FILE :: "build/pipeline.cache"
g_pipeline_cache: vk.PipelineCache

pipeline_cache_load_or_create :: proc() {
    assert(g_pipeline_cache == 0)

    data, ok := os.read_entire_file_from_filename(PIPELINE_CACHE_FILE)
    defer if ok { delete(data) }

    if ok {
        log.infof("Pipeline cache loaded: was %d bytes.\n", len(data))
    } else {
        log.info("New pipeline cache created.")
    }

	pipeline_cache_info := vk.PipelineCacheCreateInfo {
		sType           = .PIPELINE_CACHE_CREATE_INFO,
		initialDataSize = len(data),
		pInitialData    = raw_data(data),
	}

	vk.CreatePipelineCache(vk_ctx.device, &pipeline_cache_info, nil, &g_pipeline_cache)
}

pipeline_cache_save_and_destroy :: proc() {
    assert(g_pipeline_cache != 0)

    new_pipeline_cache_size: int
    vk_assert(vk.GetPipelineCacheData(vk_ctx.device, g_pipeline_cache, &new_pipeline_cache_size, nil))

    if new_pipeline_cache_size == 0 {
        log.warn("New pipeline cache size was zero.")

        return
    }

    pipeline_cache_data := make([]byte, new_pipeline_cache_size)
    defer delete(pipeline_cache_data)

    vk_assert(vk.GetPipelineCacheData(vk_ctx.device, g_pipeline_cache, &new_pipeline_cache_size, raw_data(pipeline_cache_data)))

    ok := os.write_entire_file(PIPELINE_CACHE_FILE, pipeline_cache_data)

    if ok {
        log.infof("New pipeline cache written: was %d bytes.\n", new_pipeline_cache_size)
    } else {
        log.error("Could not save pipeline cache!")
    }

    vk.DestroyPipelineCache(vk_ctx.device, g_pipeline_cache, nil)
}
