package nt2

import vk "vendor:vulkan"

import "core:os"
import "core:mem"

Dds_Pixelformat_Flags :: bit_set[Dds_Pixelformat_Flag; u32]
Dds_Pixelformat_Flag :: enum u32 {
	Alphapixels = 0,
	Alpha       = 1,
	Fourcc      = 2, // Texture contains compressed RGB data; dwFourCC contains valid data.
	Rgb         = 6,
	Yuv         = 9,
	Luminance   = 17,
}

Dds_Pixelformat :: struct {
	size:          u32,
	flags:         Dds_Pixelformat_Flags,
	fourcc:        Dds_Pixelformat_Fourcc,
	Rgb_Bit_Count: u32,
	r_bitmask:     u32,
	g_bitmask:     u32,
	b_bitmask:     u32,
	a_bitmask:     u32,
};

Dds_Pixelformat_Fourcc :: enum u32 {
	Dxt1 = 'D' << 0 | 'X' << 8 | 'T' << 16 | '1' << 24,
	Dxt2 = 'D' << 0 | 'X' << 8 | 'T' << 16 | '2' << 24,
	Dxt3 = 'D' << 0 | 'X' << 8 | 'T' << 16 | '3' << 24,
	Dxt4 = 'D' << 0 | 'X' << 8 | 'T' << 16 | '4' << 24,
	Dxt5 = 'D' << 0 | 'X' << 8 | 'T' << 16 | '5' << 24,

	Dx10 = 'D' << 0 | 'X' << 8 | '1' << 16 | '0' << 24,
}

Dds_Header_Flags :: bit_set[Dds_Header_Flag; u32]
Dds_Header_Flag :: enum u32 {
	Caps        = 0,
	Height      = 1,
	Width       = 2,
	Pitch       = 3,
	Pixelformat = 12,
	Mipmapcount = 17,
	Linearsize  = 19,
	Depth       = 23,
}

Dds_Header_Caps1_Flags :: bit_set[Dds_Header_Caps1_Flag; u32]
Dds_Header_Caps1_Flag :: enum u32 {
	Complex = 3,
	Mipmap  = 22,
	Texture = 12,
}

Dds_Header_Caps2_Flags :: bit_set[Dds_Header_Caps1_Flag; u32]
Dds_Header_Caps2_Flag :: enum u32 {
	Cubemap = 9,
	Cubemap_Positive_X = 10,
	Cubemap_Negative_X = 11,
	Cubemap_Positive_Y = 12,
	Cubemap_Negative_Y = 13,
	Cubemap_Positive_Z = 14,
	Cubemap_Negative_Z = 15,
	Volume = 21,
}

Dds_Header_Caps3_Flags :: bit_set[Dds_Header_Caps1_Flag; u32]
Dds_Header_Caps3_Flag :: enum u32 {
}

Dds_Header_Caps4_Flags :: bit_set[Dds_Header_Caps1_Flag; u32]
Dds_Header_Caps4_Flag :: enum u32 {
}

Dds_Header :: struct {
	size:                 u32,
	flags:                Dds_Header_Flags,
	height:               u32,
	width:                u32,
	pitch_or_linear_size: u32,
	depth:                u32,
	mipmap_count:         u32,
	reserved1:            [11]u32,
	pixelformat:          Dds_Pixelformat,
	caps1:                Dds_Header_Caps1_Flags,
	caps2:                Dds_Header_Caps2_Flags,
	caps3:                Dds_Header_Caps3_Flags,
	caps4:                Dds_Header_Caps4_Flags,
	reserved2:            u32,
}

D3d_Resource_Dimension :: enum u32 {
	Unknown,
	Buffer,
	Texture1D,
	Texture2D,
	Texture3D,
}

Dxgi_Format :: enum u32 {
	UNKNOWN,
	R32G32B32A32_TYPELESS,
	R32G32B32A32_FLOAT,
	R32G32B32A32_UINT,
	R32G32B32A32_SINT,
	R32G32B32_TYPELESS,
	R32G32B32_FLOAT,
	R32G32B32_UINT,
	R32G32B32_SINT,
	R16G16B16A16_TYPELESS,
	R16G16B16A16_FLOAT,
	R16G16B16A16_UNORM,
	R16G16B16A16_UINT,
	R16G16B16A16_SNORM,
	R16G16B16A16_SINT,
	R32G32_TYPELESS,
	R32G32_FLOAT,
	R32G32_UINT,
	R32G32_SINT,
	R32G8X24_TYPELESS,
	D32_FLOAT_S8X24_UINT,
	R32_FLOAT_X8X24_TYPELESS,
	X32_TYPELESS_G8X24_UINT,
	R10G10B10A2_TYPELESS,
	R10G10B10A2_UNORM,
	R10G10B10A2_UINT,
	R11G11B10_FLOAT,
	R8G8B8A8_TYPELESS,
	R8G8B8A8_UNORM,
	R8G8B8A8_UNORM_SRGB,
	R8G8B8A8_UINT,
	R8G8B8A8_SNORM,
	R8G8B8A8_SINT,
	R16G16_TYPELESS,
	R16G16_FLOAT,
	R16G16_UNORM,
	R16G16_UINT,
	R16G16_SNORM,
	R16G16_SINT,
	R32_TYPELESS,
	D32_FLOAT,
	R32_FLOAT,
	R32_UINT,
	R32_SINT,
	R24G8_TYPELESS,
	D24_UNORM_S8_UINT,
	R24_UNORM_X8_TYPELESS,
	X24_TYPELESS_G8_UINT,
	R8G8_TYPELESS,
	R8G8_UNORM,
	R8G8_UINT,
	R8G8_SNORM,
	R8G8_SINT,
	R16_TYPELESS,
	R16_FLOAT,
	D16_UNORM,
	R16_UNORM,
	R16_UINT,
	R16_SNORM,
	R16_SINT,
	R8_TYPELESS,
	R8_UNORM,
	R8_UINT,
	R8_SNORM,
	R8_SINT,
	A8_UNORM,
	R1_UNORM,
	R9G9B9E5_SHAREDEXP,
	R8G8_B8G8_UNORM,
	G8R8_G8B8_UNORM,
	BC1_TYPELESS,
	BC1_UNORM,
	BC1_UNORM_SRGB,
	BC2_TYPELESS,
	BC2_UNORM,
	BC2_UNORM_SRGB,
	BC3_TYPELESS,
	BC3_UNORM,
	BC3_UNORM_SRGB,
	BC4_TYPELESS,
	BC4_UNORM,
	BC4_SNORM,
	BC5_TYPELESS,
	BC5_UNORM,
	BC5_SNORM,
	B5G6R5_UNORM,
	B5G5R5A1_UNORM,
	B8G8R8A8_UNORM,
	B8G8R8X8_UNORM,
	R10G10B10_XR_BIAS_A2_UNORM,
	B8G8R8A8_TYPELESS,
	B8G8R8A8_UNORM_SRGB,
	B8G8R8X8_TYPELESS,
	B8G8R8X8_UNORM_SRGB,
	BC6H_TYPELESS,
	BC6H_UF16,
	BC6H_SF16,
	BC7_TYPELESS,
	BC7_UNORM,
	BC7_UNORM_SRGB,
	AYUV,
	Y410,
	Y416,
	NV12,
	P010,
	P016,
	_420_OPAQUE,
	YUY2,
	Y210,
	Y216,
	NV11,
	AI44,
	IA44,
	P8,
	A8P8,
	B4G4R4A4_UNORM,
	P208,
	V208,
	V408,
	SAMPLER_FEEDBACK_MIN_MIP_OPAQUE,
	SAMPLER_FEEDBACK_MIP_REGION_USED_OPAQUE,
	FORCE_UINT,
}

Dx10_Header :: struct {
	dxgi_format: Dxgi_Format,
	resource_dimension: D3d_Resource_Dimension,
	misc_flag: u32,
	array_size: u32,
	misc_flags2: u32,
}

DDS_FOURCC : u32 : 'D' << 0 | 'D' << 8 | 'S' << 16 | ' ' << 24

Dds_Texture :: struct {
	pixel_data: []byte,
	format: Dxgi_Format,
}

Dds_Expected_Header :: struct {
	fourcc: u32,
	dds_header: Dds_Header,
	dx10_header: Dx10_Header,
}

Dds_Load_Error :: enum {
	None,
	Io_Error,
	File_Too_Small,
	File_Invalid,
	Unknown_Format,
	No_Format_Info,
	Invalid_Dimensions,
}

@(private="file")
dds_get_format :: proc(dds_header: ^Dds_Header, dx10_header: ^Dx10_Header) -> Dxgi_Format {

	if .Rgb in dds_header.pixelformat.flags {
		panic("Not implemented!")
	} else if .Luminance in dds_header.pixelformat.flags {
		panic("Not implemented!")
	} else if .Alpha in dds_header.pixelformat.flags {
		panic("Not implemented!")
	} else if .Fourcc in dds_header.pixelformat.flags {
		switch dds_header.pixelformat.fourcc {
		case .Dxt1: return .BC1_UNORM
		case .Dxt2: return .BC2_UNORM
		case .Dxt3: return .BC2_UNORM
		case .Dxt4: return .BC3_UNORM
		case .Dxt5: return .BC3_UNORM
		case .Dx10: return dx10_header.dxgi_format
		}
	}

	return .UNKNOWN
}

Dxgi_Format_Info :: struct {
	bits_per_pixel: int,
	is_compressed: bool,
	block_size: int, // if is_compressed
}

@(private="file")
dds_get_format_info :: proc(format: Dxgi_Format) -> (info: Dxgi_Format_Info, found: bool) {
	info = Dxgi_Format_Info {
		bits_per_pixel = -1,
		is_compressed = false,
		block_size = -1,
	}
	found = true

	#partial switch format {
	case .R32G32B32A32_TYPELESS: fallthrough
	case .R32G32B32A32_FLOAT: fallthrough
	case .R32G32B32A32_UINT: fallthrough
	case .R32G32B32A32_SINT:
		info.bits_per_pixel = 128

	case .R32G32B32_TYPELESS: fallthrough
	case .R32G32B32_FLOAT: fallthrough
	case .R32G32B32_UINT: fallthrough
	case .R32G32B32_SINT:
		info.bits_per_pixel = 96

	case .R16G16B16A16_TYPELESS: fallthrough
	case .R16G16B16A16_FLOAT: fallthrough
	case .R16G16B16A16_UNORM: fallthrough
	case .R16G16B16A16_UINT: fallthrough
	case .R16G16B16A16_SNORM: fallthrough
	case .R16G16B16A16_SINT: fallthrough
	case .R32G32_TYPELESS: fallthrough
	case .R32G32_FLOAT: fallthrough
	case .R32G32_UINT: fallthrough
	case .R32G32_SINT: fallthrough
	case .R32G8X24_TYPELESS: fallthrough
	case .D32_FLOAT_S8X24_UINT: fallthrough
	case .R32_FLOAT_X8X24_TYPELESS: fallthrough
	case .X32_TYPELESS_G8X24_UINT:
		info.bits_per_pixel = 64

	case .R10G10B10A2_TYPELESS: fallthrough
	case .R10G10B10A2_UNORM: fallthrough
	case .R10G10B10A2_UINT: fallthrough
	case .R11G11B10_FLOAT: fallthrough
	case .R8G8B8A8_TYPELESS: fallthrough
	case .R8G8B8A8_UNORM: fallthrough
	case .R8G8B8A8_UNORM_SRGB: fallthrough
	case .R8G8B8A8_UINT: fallthrough
	case .R8G8B8A8_SNORM: fallthrough
	case .R8G8B8A8_SINT: fallthrough
	case .R16G16_TYPELESS: fallthrough
	case .R16G16_FLOAT: fallthrough
	case .R16G16_UNORM: fallthrough
	case .R16G16_UINT: fallthrough
	case .R16G16_SNORM: fallthrough
	case .R16G16_SINT: fallthrough
	case .R32_TYPELESS: fallthrough
	case .D32_FLOAT: fallthrough
	case .R32_FLOAT: fallthrough
	case .R32_UINT: fallthrough
	case .R32_SINT: fallthrough
	case .R24G8_TYPELESS: fallthrough
	case .D24_UNORM_S8_UINT: fallthrough
	case .R24_UNORM_X8_TYPELESS: fallthrough
	case .X24_TYPELESS_G8_UINT: fallthrough
	case .R9G9B9E5_SHAREDEXP: fallthrough
	case .R8G8_B8G8_UNORM: fallthrough
	case .G8R8_G8B8_UNORM: fallthrough
	case .B8G8R8A8_UNORM: fallthrough
	case .B8G8R8X8_UNORM: fallthrough
	case .R10G10B10_XR_BIAS_A2_UNORM: fallthrough
	case .B8G8R8A8_TYPELESS: fallthrough
	case .B8G8R8A8_UNORM_SRGB: fallthrough
	case .B8G8R8X8_TYPELESS: fallthrough
	case .B8G8R8X8_UNORM_SRGB:
		info.bits_per_pixel = 32

	case .R8G8_TYPELESS: fallthrough
	case .R8G8_UNORM: fallthrough
	case .R8G8_UINT: fallthrough
	case .R8G8_SNORM: fallthrough
	case .R8G8_SINT: fallthrough
	case .R16_TYPELESS: fallthrough
	case .R16_FLOAT: fallthrough
	case .D16_UNORM: fallthrough
	case .R16_UNORM: fallthrough
	case .R16_UINT: fallthrough
	case .R16_SNORM: fallthrough
	case .R16_SINT: fallthrough
	case .B5G6R5_UNORM: fallthrough
	case .B5G5R5A1_UNORM: fallthrough
	case .B4G4R4A4_UNORM:
		info.bits_per_pixel = 16

	case .R8_TYPELESS: fallthrough
	case .R8_UNORM: fallthrough
	case .R8_UINT: fallthrough
	case .R8_SNORM: fallthrough
	case .R8_SINT: fallthrough
	case .A8_UNORM:
		info.bits_per_pixel = 8

	case .R1_UNORM:
		info.bits_per_pixel = 1

	case .BC1_TYPELESS: fallthrough
	case .BC1_UNORM: fallthrough
	case .BC1_UNORM_SRGB: fallthrough
	case .BC4_TYPELESS: fallthrough
	case .BC4_UNORM: fallthrough
	case .BC4_SNORM:
		info.bits_per_pixel = 4
		info.is_compressed = true
		info.block_size = 8

	case .BC2_TYPELESS: fallthrough
	case .BC2_UNORM: fallthrough
	case .BC2_UNORM_SRGB: fallthrough
	case .BC3_TYPELESS: fallthrough
	case .BC3_UNORM: fallthrough
	case .BC3_UNORM_SRGB: fallthrough
	case .BC5_TYPELESS: fallthrough
	case .BC5_UNORM: fallthrough
	case .BC5_SNORM: fallthrough
	case .BC6H_TYPELESS: fallthrough
	case .BC6H_UF16: fallthrough
	case .BC6H_SF16: fallthrough
	case .BC7_TYPELESS: fallthrough
	case .BC7_UNORM: fallthrough
	case .BC7_UNORM_SRGB:
		info.bits_per_pixel = 8
		info.is_compressed = true
		info.block_size = 16

	case: found = false
	}

	// TODO[TS]: Deal with `packed` in reference code.

	return
}

@(private="file")
dxgi_format_to_vulkan_format :: proc(format: Dxgi_Format) -> vk.Format {
	#partial switch format {
	case .BC1_UNORM: return .BC1_RGB_UNORM_BLOCK
	}

	panic("Format not implemented")
}

@(private="file")
Sub_Resource :: struct {
	bytes_per_unit: int,
	width, height: int, // Given in above unit
	size: int,
}

@(private="file")
dds_get_sub_resource :: proc(width, height: int, format_info: Dxgi_Format_Info) -> Sub_Resource {
	width, height := width, height

	if format_info.is_compressed {
		width = mem.align_forward_int(width, 4) / 4
		height = mem.align_forward_int(height, 4) / 4
	}

	bits_per_unit := format_info.bits_per_pixel
	if format_info.is_compressed {
		bits_per_unit *= 16
	}

	bytes_per_unit := mem.align_forward_int(bits_per_unit, 8) / 8

	return Sub_Resource {
		width = width,
		height = height,
		bytes_per_unit = bytes_per_unit,
		size = width * height * bytes_per_unit,
	}
}

dds_load_from_file :: proc(filename: string) -> (image: Image, error: Dds_Load_Error) {
	data, success := os.read_entire_file(filename)
	defer delete(data)

	if !success {
		error = .Io_Error

		return
	}

	// TODO[TS]: This is wrong! The dx10 header is not required.
	if len(data) < size_of(Dds_Expected_Header) {
		// You must be this tall to ride
		error = .File_Too_Small

		return
	}

	using header := cast(^Dds_Expected_Header)raw_data(data)

	if header.fourcc != DDS_FOURCC {
		error = .File_Invalid

		return
	}

	format := dds_get_format(&dds_header, &dx10_header)

	if format == .UNKNOWN {
		error = .Unknown_Format

		return
	}

	format_info, found := dds_get_format_info(format)
	if !found {
		error = .No_Format_Info

		return
	}

	base_width := cast(int)dds_header.width
	base_height := cast(int)dds_header.height
	// Note: it seems that num_mipmaps is counting the total number of mips, including level 0

	// TODO[TS]: Use flags to check whether mips are present
	num_mipmaps := 1
	if .Mipmapcount in dds_header.flags {
		num_mipmaps = cast(int)dds_header.mipmap_count
	}

	has_dx10 := false
	if .Fourcc in dds_header.pixelformat.flags {
		// We have a valid dds_header.pixelformat.fourcc
		has_dx10 = dds_header.pixelformat.fourcc == .Dx10
	}

	pixel_data := data[size_of(DDS_FOURCC) + size_of(Dds_Header):]
	if has_dx10 {
		pixel_data = pixel_data[size_of(Dx10_Header):]
	}

	if format_info.is_compressed {
		if (base_width % 4) != 0 || (base_height % 4) != 0 {
			error = .Invalid_Dimensions

			return
		}
	}

	width, height := base_width, base_height

	copy_regions := make([]vk.BufferImageCopy, num_mipmaps)
	defer delete(copy_regions)

	data_start := vk.DeviceSize(0)

	staging_buffer, staging_offset, _ := staging_buffer_upload_data(pixel_data)
	for mip_level in 0..<num_mipmaps {
		sub_resource := dds_get_sub_resource(width, height, format_info)

		copy_regions[mip_level] = vk.BufferImageCopy {
			bufferOffset = cast(vk.DeviceSize)data_start + staging_offset,
			bufferRowLength = 0,
			bufferImageHeight = 0,
			imageSubresource = {
				aspectMask = {.COLOR},
				mipLevel = cast(u32)mip_level,
				baseArrayLayer = 0,
				layerCount = 1,
			},
			imageOffset = {0, 0, 0},
			imageExtent = {cast(u32)width, cast(u32)height, 1},
		}

		data_start += cast(vk.DeviceSize)sub_resource.size

		if mip_level == (num_mipmaps - 1) {
			assert(width == 1 && height == 1)
		}

		width >>= 1
		height >>= 1
	}

	image = image_create_2d(
		dxgi_format_to_vulkan_format(format),
		u32(base_width),
		u32(base_height),
		{.TRANSFER_DST, .SAMPLED},
		{.DEVICE_LOCAL},
		u32(num_mipmaps))

	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	_image_transition_from_undefined_to_transfer_dst(command_buffer, &image)

	vk.CmdCopyBufferToImage(
		command_buffer,
		staging_buffer,
		image.image,
		.TRANSFER_DST_OPTIMAL,
		cast(u32)len(copy_regions),
		raw_data(copy_regions))

	_image_transition_from_transfer_dst_to_shader_read_only(command_buffer, &image)

	return
}

