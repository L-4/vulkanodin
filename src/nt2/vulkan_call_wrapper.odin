package nt2

import vk "vendor:vulkan"

// TODO[TS], p2: This file should probably be generated
// TODO[TS], p1: Non-debug versions should also be generated, which don't use loc

vk_create_buffer :: proc(create_info: ^vk.BufferCreateInfo, loc := #caller_location) -> (buffer: vk.Buffer, ok: bool) #optional_ok {
	result := vk.CreateBuffer(vk_ctx.device, create_info, nil, &buffer)
	ok = result == .SUCCESS
	vk_assert(result)
	if ok {
		vk_name_object(buffer, "Unnamed VkBuffer created at {}", loc)
	}

	return
}
