package nt2

import sdl "vendor:sdl2"
import vk "vendor:vulkan"

import "lib:imgui"
import "lib:imgui/imgui_impl_sdl2"
import "lib:imgui/imgui_impl_vulkan"
import "lib:imgui/imgui_impl_opengl3"

_imgui_impl_vk_descriptor_pool: vk.DescriptorPool

@(disabled=!ENABLE_IMGUI)
imgui_initialize_general :: proc() {
	imgui.CHECKVERSION()
	imgui.CreateContext(nil)

	io := imgui.GetIO()
	io.ConfigFlags |= {
		.NavEnableKeyboard,
		.NavEnableGamepad,
		.DockingEnable,
		.ViewportsEnable,
	}

	imgui.StyleColorsDark(nil)

	if .ViewportsEnable in io.ConfigFlags {
		style := imgui.GetStyle()
		style.WindowRounding = 0.0
		style.Colors[imgui.Col.WindowBg].a = 1
	}
}

@(disabled=!ENABLE_IMGUI)
imgui_initialize_gl :: proc(window: ^sdl.Window, gl_context: sdl.GLContext) {
	imgui_initialize_general()

	imgui_impl_sdl2.InitForOpenGL(window, gl_context)
	imgui_impl_opengl3.Init(nil)
}

// TODO[TS]: ImGui supports dynamic rendering now!
@(disabled=!ENABLE_IMGUI)
imgui_initialize_vk :: proc(window: ^sdl.Window, render_pass: vk.RenderPass, swapchain_image_count: u32) {
	imgui_initialize_general()

	imgui_impl_vulkan.LoadFunctions(proc "c" (function_name: cstring, user_data: rawptr) -> vk.ProcVoidFunction {
		return vk.GetInstanceProcAddr(auto_cast user_data, function_name)
	}, auto_cast vk_ctx.instance)

	pool_sizes := []vk.DescriptorPoolSize {
		{ .SAMPLER,                1000 },
		{ .COMBINED_IMAGE_SAMPLER, 1000 },
		{ .SAMPLED_IMAGE,          1000 },
		{ .STORAGE_IMAGE,          1000 },
		{ .UNIFORM_TEXEL_BUFFER,   1000 },
		{ .STORAGE_TEXEL_BUFFER,   1000 },
		{ .UNIFORM_BUFFER,         1000 },
		{ .STORAGE_BUFFER,         1000 },
		{ .UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ .STORAGE_BUFFER_DYNAMIC, 1000 },
		{ .INPUT_ATTACHMENT,       1000 },
	}

	assert(_imgui_impl_vk_descriptor_pool == {})
	vk_assert(vk.CreateDescriptorPool(vk_ctx.device, &vk.DescriptorPoolCreateInfo {
		sType         = .DESCRIPTOR_POOL_CREATE_INFO,
		flags         = {.FREE_DESCRIPTOR_SET},
		maxSets       = auto_cast len(pool_sizes) * 1000,
		poolSizeCount = auto_cast len(pool_sizes),
		pPoolSizes    = raw_data(pool_sizes),
	}, nil, &_imgui_impl_vk_descriptor_pool))
	vk_name_object(_imgui_impl_vk_descriptor_pool, "ImGui Init_Info descriptor pool")

	imgui_impl_sdl2.InitForVulkan(window)
	imgui_impl_vulkan.Init(&imgui_impl_vulkan.InitInfo {
		Instance       = vk_ctx.instance,
		PhysicalDevice = vk_ctx.physical_device,
		Device         = vk_ctx.device,
		QueueFamily    = vk_ctx.combined_queue.family,
		Queue          = vk_ctx.combined_queue.queue,
		PipelineCache  = g_pipeline_cache,
		DescriptorPool = _imgui_impl_vk_descriptor_pool,
		MinImageCount  = swapchain_image_count,
		ImageCount     = swapchain_image_count,
		MSAASamples    = {._1},
		ColorAttachmentFormat = .B8G8R8A8_SRGB,
	}, render_pass)

	command_buffer := command_buffer_begin_single_use()
	assert(imgui_impl_vulkan.CreateFontsTexture(command_buffer))
	command_buffer_end_single_use(command_buffer)
}

@(disabled=!ENABLE_IMGUI)
imgui_deinitialize :: proc() {
	imgui_impl_vulkan.DestroyFontUploadObjects()
	imgui_impl_vulkan.Shutdown()
	vk.DestroyDescriptorPool(vk_ctx.device, _imgui_impl_vk_descriptor_pool, nil)
	imgui.DestroyContext(nil)
}

// First in frame
imgui_handle_event :: proc(e: ^sdl.Event) -> Handled {
	when ENABLE_IMGUI {
		imgui_impl_sdl2.ProcessEvent(e)
		io := imgui.GetIO()
		#partial switch e.type {
		case .KEYDOWN, .KEYUP:
			return Handled(io.WantCaptureKeyboard)
		case .MOUSEBUTTONDOWN, .MOUSEBUTTONUP, .MOUSEMOTION, .MOUSEWHEEL:
			return Handled(io.WantCaptureMouse)
		}

		return Handled(false)
	} else {
		return Not_Handled
	}
}

// Second in frame
@(disabled=!ENABLE_IMGUI)
imgui_newframe :: proc() {
	// imgui_impl_opengl3.NewFrame()
	imgui_impl_vulkan.NewFrame()
	imgui_impl_sdl2.NewFrame()
	imgui.NewFrame()
	imgui.DockSpaceOverViewportEx(nil, {.PassthruCentralNode, .NoDockingInCentralNode}, nil)
	imgui_do_all_windows()
}

// Third in frame
@(disabled=!ENABLE_IMGUI)
imgui_render_gl :: proc() {
	when ENABLE_IMGUI {
		imgui.Render()
		imgui_impl_opengl3.RenderDrawData(imgui.GetDrawData())

		// Update and render additional windows
		io := imgui.GetIO()
		if .ViewportsEnable in io.ConfigFlags {
			backup_current_window := sdl.GL_GetCurrentWindow()
			backup_current_context := sdl.GL_GetCurrentContext()
			imgui.UpdatePlatformWindows()
			imgui.RenderPlatformWindowsDefault()
			sdl.GL_MakeCurrent(backup_current_window, backup_current_context)
		}
	}
}

@(disabled=!ENABLE_IMGUI)
imgui_render_vk :: proc(command_buffer: vk.CommandBuffer) {
	when ENABLE_IMGUI {
		imgui.Render()

		imgui_impl_vulkan.RenderDrawData(imgui.GetDrawData(), command_buffer)

		// Update and render additional windows
		io := imgui.GetIO()
		if .ViewportsEnable in io.ConfigFlags {
			imgui.UpdatePlatformWindows()
			imgui.RenderPlatformWindowsDefault()
		}
	}
}

when ENABLE_IMGUI {
	ImGui_Window :: struct {
		title:     cstring,
		open:      bool,
		show_proc: proc(title: cstring, open: ^bool),
	}

	imgui_windows := []ImGui_Window {
		{ "Demo window",   false, imgui_do_demo_window },
	}
}

@(disabled=!ENABLE_IMGUI)
imgui_do_demo_window :: proc(title: cstring, open: ^bool) {
	imgui.ShowDemoWindow(open)
}

@(disabled=!ENABLE_IMGUI)
imgui_do_all_windows :: proc() {
	when ENABLE_IMGUI {
		if imgui.BeginMainMenuBar() {
			defer imgui.EndMainMenuBar()
			if imgui.BeginMenu("Windows") {
				defer imgui.EndMenu()
				for window in &imgui_windows {
					if imgui.MenuItemEx(window.title, nil, window.open, true) {
						window.open = !window.open
					}
				}
			}
		}

		for window in &imgui_windows {
			if !window.open {
				continue
			}

			window.show_proc(window.title, &window.open)
		}
	}
}

imgui_get_game_viewport :: proc() -> (tl, br: [2]int) {
	viewport := imgui.GetMainViewport()

	tl = {cast(int)viewport.WorkPos.x, cast(int)viewport.WorkPos.y}
	br = tl + [2]int { cast(int)viewport.WorkSize.x, cast(int)viewport.WorkSize.y}

	return
}
