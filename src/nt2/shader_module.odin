package nt2

import vk "vendor:vulkan"

import "core:os"

// TODO[TS]: Handle errors
shader_module_create_from_file :: proc(filename: string) -> vk.ShaderModule {
	data, success := os.read_entire_file(filename)
	defer delete(data)
	assert(success, "Could not open shader file")

	// Vulkan requires that the length of the shader data is four byte aligned.
	// While there may not be any such requirement for spirv files, it seems to
	// be the case for glslc.
	assert(len(data) % 4 == 0)

	shader_module_info := vk.ShaderModuleCreateInfo {
		sType    = .SHADER_MODULE_CREATE_INFO,
		codeSize = len(data),
		pCode    = cast(^u32)raw_data(data),
	}

	shader_module: vk.ShaderModule
	vk_assert(vk.CreateShaderModule(vk_ctx.device, &shader_module_info, nil, &shader_module))

	vk_name_object(shader_module, "shader_%s", filename)

	return shader_module
}

shader_stage_info_create_default :: proc(vertex_module, fragment_module: vk.ShaderModule) -> [2]vk.PipelineShaderStageCreateInfo {
	vertex_shader_stage_info := vk.PipelineShaderStageCreateInfo {
		sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
		stage = {.VERTEX},
		module = vertex_module,
		pName = "main",
	}

	fragment_shader_stage_info := vk.PipelineShaderStageCreateInfo {
		sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
		stage = {.FRAGMENT},
		module = fragment_module,
		pName = "main",
	}

	return {vertex_shader_stage_info, fragment_shader_stage_info}
}
