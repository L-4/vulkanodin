package nt2

import vk "vendor:vulkan"
import sdl "vendor:sdl2"

select_optimal_swapchain_extent :: proc() -> vk.Extent2D {
	if vk_ctx.surface_capabilities.currentExtent.width != max(u32) {
		return vk_ctx.surface_capabilities.currentExtent
	} else {
		w, h: i32
		sdl.Vulkan_GetDrawableSize(g_window.sdl_window, &w, &h)

		return vk.Extent2D {
			width = clamp(cast(u32)w, vk_ctx.surface_capabilities.minImageExtent.width, vk_ctx.surface_capabilities.maxImageExtent.width),
			height = clamp(cast(u32)h, vk_ctx.surface_capabilities.minImageExtent.height, vk_ctx.surface_capabilities.maxImageExtent.height),
		}
	}
}

// Totally arbitrary number, but kept low ish as this amount of per-image
// structs are statically allocated
SWAPCHAIN_MAX_IMAGES :: 4

Swapchain_Per_Image :: struct {
	image:       vk.Image,
	view:        vk.ImageView,
	framebuffer: vk.Framebuffer,
}

Swapchain_Per_Frame :: struct {
	// Signaled when the image requested is available. Waited for in QueueSubmit to the backbuffer
	image_available_semaphore:      vk.Semaphore,
	render_finished_semaphore:      vk.Semaphore,
	backbuffer_draw_command_buffer: vk.CommandBuffer,

	// Index used when submitting to backbuffer
	image_index: u32,
}

Swapchain :: struct {
	extent:     vk.Extent2D,
	handle:     vk.SwapchainKHR,
	renderpass: vk.RenderPass,

	// TODO[TS]: Small array?
	frame_data:  [FIF_COUNT]Swapchain_Per_Frame,
	image_count: u32,
	image_data:  [SWAPCHAIN_MAX_IMAGES]Swapchain_Per_Image,
}

// frames_in_flight = 3
// images = 2

// fif_idx = 0
// loop:
//		// 1. Begin swapchain
//		// We have one command pool for each frame, used for all command buffers in said frame.
//		// This resets said command pool, which currently has allocated command buffers from the last
// 		// time that we were at our current fif_idx.
//		// These command buffers should have finished executing by definition because (why?)
//		reset_command_buffer_pool(fif_idx)
//		// image_idx represents an index into our swapchain images, that is the next image which we will be able to use.
//		// This image might not be available yet however. We also provide a semaphore which will be signaled when it is.
//		image_idx = vkAcquireNextImageKHR(sem_signaled_when_image_available)
//
//		// 2. User draws
//		// Now any arbitrary commands can happen
//
//		// 3. Backbuffer draw
//		// Here we begin a new command buffer (couldn't we reuse the one from the work done during this frame?) and begin our renderpass
//		// to draw to the backbuffer.
//		// Here we draw into the framebuffer referred to by image_idx
//		// When we've recorded our draws into this command buffer, we submit.
//		// The submit waits for the semaphore 'sem_signaled_when_image_available', as we cannot render into
//		// our image until it is available.
//		// We also make sure that the semaphore wait mask in the submit is .COLOR_ATTACHMENT_OUTPUT, as we
//		// are free to perform all previous backbuffer render steps without waiting. The only part of the command
// 		// which has to wait for the image is the part where we write to the image.
//		//
//		// In the same submit, we also tell vulkan to signal a semaphore 'sem_signaled_when_rendering_complete'.
//		// We also give a fence 'fence_rendering_complete' to the submit which is signaled when the work is complete.
//
//		// 4. Presenting
//		// We now call vkQueuePresentKHR. We give it the image index that we have rendered to, as well as tell it to
//		// wait with presenting until `sem_signaled_when_rendering_complete` finishes, as until that point we havent
//		// finished rendering to the backbuffer.
//
//		// Finally we increment the fif_idx, wrapping it to the number of frames in flight we support.
//		// Note that we don't personally increment the image_idx - this is done by vulkan. It might even be the
//		// case that image indices don't increment as expected. Vulkan only gives us the index of the image which will
//		// be available next - this might not be the previous index + 1.

swapchain_create :: proc() -> Swapchain {
	swapchain := Swapchain {}

	color_attachment := vk.AttachmentDescription {
		format         = vk_ctx.surface_format,
		samples        = {._1},
		loadOp         = .CLEAR,
		storeOp        = .STORE,
		stencilLoadOp  = .DONT_CARE,
		stencilStoreOp = .DONT_CARE,
		initialLayout  = .UNDEFINED,
		finalLayout    = .PRESENT_SRC_KHR,
	}

	color_attachment_ref := vk.AttachmentReference {
		attachment = 0,
		layout     = .COLOR_ATTACHMENT_OPTIMAL,
	}

	subpass := vk.SubpassDescription {
		pipelineBindPoint    = .GRAPHICS,
		colorAttachmentCount = 1,
		pColorAttachments    = &color_attachment_ref,
	}

	subpass_dependency := vk.SubpassDependency {
		srcSubpass    = vk.SUBPASS_EXTERNAL,
		dstSubpass    = 0,
		srcStageMask  = {.COLOR_ATTACHMENT_OUTPUT, .EARLY_FRAGMENT_TESTS},
		srcAccessMask = {},
		dstStageMask  = {.COLOR_ATTACHMENT_OUTPUT, .EARLY_FRAGMENT_TESTS},
		dstAccessMask = {.COLOR_ATTACHMENT_WRITE},
	}

	render_pass_info := vk.RenderPassCreateInfo {
		sType           = .RENDER_PASS_CREATE_INFO,
		attachmentCount = 1,
		pAttachments    = &color_attachment,
		subpassCount    = 1,
		pSubpasses      = &subpass,
		dependencyCount = 1,
		pDependencies   = &subpass_dependency,
	}

	vk_assert(vk.CreateRenderPass(vk_ctx.device, &render_pass_info, nil, &swapchain.renderpass))

	swapchain.extent = select_optimal_swapchain_extent()

	swapchain_create_info := vk.SwapchainCreateInfoKHR {
		sType            = .SWAPCHAIN_CREATE_INFO_KHR,
		surface          = vk_ctx.surface,
		minImageCount    = 3,
		imageFormat      = vk_ctx.surface_format,
		imageColorSpace  = vk_ctx.surface_color_space,
		imageExtent      = swapchain.extent,
		imageArrayLayers = 1,
		imageUsage       = {.COLOR_ATTACHMENT},
		imageSharingMode = .EXCLUSIVE,
		preTransform     = vk_ctx.surface_capabilities.currentTransform,
		compositeAlpha   = {.OPAQUE},
		presentMode      = vk_ctx.surface_present_mode,
		clipped          = true,
	}

	vk_assert(vk.CreateSwapchainKHR(vk_ctx.device, &swapchain_create_info, nil, &swapchain.handle))

	// Swapchain images
	{
		vk.GetSwapchainImagesKHR(vk_ctx.device, swapchain.handle, &swapchain.image_count, nil)
		assert(swapchain.image_count <= SWAPCHAIN_MAX_IMAGES)

		// Transfer swapchain image handles into frame data...
		swapchain_images := make([]vk.Image, swapchain.image_count, context.temp_allocator)

		vk.GetSwapchainImagesKHR(vk_ctx.device, swapchain.handle, &swapchain.image_count, raw_data(swapchain_images))

		for i in 0..<swapchain.image_count {
			swapchain.image_data[i].image = swapchain_images[i]
		}
	}

	// Create per-fif semaphores
	for swapchain_frame_idx in 0..<FIF_COUNT {
		frame_data := &swapchain.frame_data[swapchain_frame_idx]

		// Semaphore and fence
		semaphore_info := vk.SemaphoreCreateInfo {
			sType = .SEMAPHORE_CREATE_INFO,
		}

		vk_assert(vk.CreateSemaphore(vk_ctx.device, &semaphore_info, nil, &frame_data.image_available_semaphore))
		vk_assert(vk.CreateSemaphore(vk_ctx.device, &semaphore_info, nil, &frame_data.render_finished_semaphore))
		vk_name_object(frame_data.image_available_semaphore, "Swapchain f#{} image available", swapchain_frame_idx)
		vk_name_object(frame_data.render_finished_semaphore, "Swapchain f#{} render finished", swapchain_frame_idx)
	}

	// Create backbuffer iamge views and framebuffers
	for swapchain_image_idx in 0..<swapchain.image_count {
		image_data := &swapchain.image_data[swapchain_image_idx]

		// Image view
		vk_assert(vk.CreateImageView(vk_ctx.device, &vk.ImageViewCreateInfo {
			sType            = .IMAGE_VIEW_CREATE_INFO,
			pNext            = nil,
			flags            = {},
			image            = image_data.image,
			viewType         = .D2,
			format           = vk_ctx.surface_format,
			components       = { r = .IDENTITY, g = .IDENTITY, b = .IDENTITY, a = .IDENTITY },
			subresourceRange = {
				aspectMask     = {.COLOR},
				baseMipLevel   = 0,
				levelCount     = 1,
				baseArrayLayer = 0,
				layerCount     = 1,
			},
		}, nil, &image_data.view))

		// Framebuffer
		vk_assert(vk.CreateFramebuffer(vk_ctx.device, &vk.FramebufferCreateInfo {
			sType           = .FRAMEBUFFER_CREATE_INFO,
			renderPass      = swapchain.renderpass,
			attachmentCount = 1,
			pAttachments    = &image_data.view,
			width           = swapchain.extent.width,
			height          = swapchain.extent.height,
			layers          = 1,
		}, nil, &image_data.framebuffer))
	}

	return swapchain
}

swapchain_destroy :: proc(swapchain: ^Swapchain) {
	for swapchain_frame_idx in 0..<FIF_COUNT {
		frame_data := &swapchain.frame_data[swapchain_frame_idx]

		vk.DestroySemaphore(vk_ctx.device, frame_data.image_available_semaphore, nil)
		vk.DestroySemaphore(vk_ctx.device, frame_data.render_finished_semaphore, nil)
	}

	for swapchain_image_idx in 0..<swapchain.image_count {
		image_data := &swapchain.image_data[swapchain_image_idx]

		vk.DestroyImageView(vk_ctx.device, image_data.view, nil)
		vk.DestroyFramebuffer(vk_ctx.device, image_data.framebuffer, nil)
	}

	vk.DestroySwapchainKHR(vk_ctx.device, swapchain.handle, nil)
	vk.DestroyRenderPass(vk_ctx.device, swapchain.renderpass, nil)
}

// TODO[TS], p3: Doesn't belong in swapchain
// Blocks until frame info is available. Acquires backbuffer image target, and frame data.
swapchain_begin_frame :: proc(swapchain: ^Swapchain) {
	// Waits for the fence which should have been signaled by the previous time fif_index was what it is now.
	fif_begin_frame()
	// Note that we're collecting any available query results for this frame,
	// which was last used num_fifs - 1 frames ago.
	command_buffer_reset_default_pool_for_frame()
	frame_data := &swapchain.frame_data[fif_get_current_index()]
	staging_buffer_begin_frame()

	// TODO[TS], p3: Can this be moved up to the point where we actually begin drawing to the backbuffer?
	// Get next image index, and setup semaphore to be signaled when image is available
	vk_assert(vk.AcquireNextImageKHR(vk_ctx.device, swapchain.handle, max(u64), frame_data.image_available_semaphore, 0, &frame_data.image_index))
}

swapchain_begin_backbuffer_draw :: proc(swapchain: ^Swapchain) -> vk.CommandBuffer {
	frame_data := &swapchain.frame_data[fif_get_current_index()]
	image_data := &swapchain.image_data[frame_data.image_index]

	frame_data.backbuffer_draw_command_buffer = command_buffer_allocate()
	vk_name_object(frame_data.backbuffer_draw_command_buffer, "Backbuffer draw command buffer")
	command_buffer := frame_data.backbuffer_draw_command_buffer
	vk_queue_begin_debug_label(vk_ctx.combined_queue.queue, { 0, 1, 0, 1 }, "Backbuffer draw")
	vk.BeginCommandBuffer(command_buffer, &vk.CommandBufferBeginInfo { sType = .COMMAND_BUFFER_BEGIN_INFO })

	clear_values := []vk.ClearValue{ { color = { float32 = { 0, 0, 0, 0 } } } }

	render_pass_info := vk.RenderPassBeginInfo {
		sType       = .RENDER_PASS_BEGIN_INFO,
		renderPass  = swapchain.renderpass,
		framebuffer = image_data.framebuffer,
		renderArea  = {
			offset = {0, 0},
			extent = swapchain.extent,
		},
		clearValueCount = cast(u32)len(clear_values),
		pClearValues    = raw_data(clear_values),
	}

	vk.CmdBeginRenderPass(command_buffer, &render_pass_info, .INLINE)

	return command_buffer
}

swapchain_end_backbuffer_draw :: proc(swapchain: ^Swapchain) {
	frame_data := &swapchain.frame_data[fif_get_current_index()]

	command_buffer := frame_data.backbuffer_draw_command_buffer

	vk.CmdEndRenderPass(command_buffer)

	vk.EndCommandBuffer(command_buffer)

	// Submit frame
	wait_stages := []vk.PipelineStageFlags{{.COLOR_ATTACHMENT_OUTPUT}}
	submit_info := vk.SubmitInfo {
		sType                = .SUBMIT_INFO,
		waitSemaphoreCount   = 1,
		pWaitSemaphores      = &frame_data.image_available_semaphore,
		pWaitDstStageMask    = raw_data(wait_stages),
		commandBufferCount   = 1,
		pCommandBuffers      = &command_buffer,
		signalSemaphoreCount = 1,
		pSignalSemaphores    = &frame_data.render_finished_semaphore,
	}

	fif_fence := fif_get_fence()

	vk_assert(vk.QueueSubmit(vk_ctx.combined_queue.queue, 1, &submit_info, fif_fence))

	vk_queue_end_debug_label(vk_ctx.combined_queue.queue)
}

swapchain_present_frame :: proc(swapchain: ^Swapchain) {
	frame_data := &swapchain.frame_data[fif_get_current_index()]

	present_info := vk.PresentInfoKHR {
		sType              = .PRESENT_INFO_KHR,
		waitSemaphoreCount = 1,
		pWaitSemaphores    = &frame_data.render_finished_semaphore,
		swapchainCount     = 1,
		pSwapchains        = &swapchain.handle,
		pImageIndices      = &frame_data.image_index,
	}

	vk.QueuePresentKHR(vk_ctx.combined_queue.queue, &present_info)
	fif_end_frame()
}
