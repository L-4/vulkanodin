package nt2

// Some functions which are nice to have everywhere
import "core:intrinsics"
import "core:slice"
import "core:fmt"

trap     :: intrinsics.debug_trap
to_bytes :: slice.to_bytes
print    :: fmt.println
printf   :: fmt.printf

// Event handling
import sdl "vendor:sdl2"

// Helpers to keep track of who handles which event
Handled     :: distinct bool
Not_Handled :: Handled(false)
handle_event :: proc(e: ^sdl.Event) -> Handled {
	// Do some logic here to find events which you want to see who Handled!
	// #partial switch e.type {
	// case .KEYUP: trap()
	// }
	return Handled(true)
}

// Math types
import "core:math/linalg"

M4 :: linalg.Matrix4x4f32

V2 :: linalg.Vector2f32
V3 :: linalg.Vector3f32
V4 :: linalg.Vector4f32

Q :: linalg.Quaternionf32

// Small array
import "core:container/small_array"

Small_Array :: small_array.Small_Array

_len             :: small_array.len
_cap             :: small_array.cap
_slice           :: small_array.slice
_push_back_elems :: small_array.push_back_elems
_resize          :: small_array.resize

_raw_data :: proc "contextless" (a: ^$A/Small_Array($N, $T)) -> ^T {
	return small_array.get_ptr(a, 0)
}

