package nt2

import vk "vendor:vulkan"
import sdl "vendor:sdl2"

import "core:log"

Vulkan_Surface :: struct {
	surface:                    vk.SurfaceKHR,

	surface_capabilities:       vk.SurfaceCapabilitiesKHR,
	surface_format:             vk.Format,
	surface_color_space:        vk.ColorSpaceKHR,
	surface_present_mode:       vk.PresentModeKHR,
}

vulkan_surface_create :: proc(using vulkan_device: ^Vulkan_Device) -> Vulkan_Surface {
    using vulkan_surface := Vulkan_Surface {}

	assert(vulkan_device.supported_window_for_present != nil)

	assert(cast(bool)sdl.Vulkan_CreateSurface(vulkan_device.supported_window_for_present, vk_ctx.instance, &surface))

    vk_assert(vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &surface_capabilities))

	format_count: u32
	vk.GetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, nil)
	formats := make([]vk.SurfaceFormatKHR, format_count)
	defer delete(formats)

	vk.GetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &format_count, raw_data(formats))

	assert(len(formats) > 0)

	surface_format = formats[0].format
	surface_color_space = formats[0].colorSpace
	got_nice_format := false

	for format in formats {
		if format.format == .B8G8R8A8_SRGB && format.colorSpace == .SRGB_NONLINEAR {
		// if format.format == .B8G8R8A8_UNORM && format.colorSpace == .SRGB_NONLINEAR {
			surface_format = format.format
			surface_color_space = format.colorSpace
			got_nice_format = true
			break
		}
	}

	if !got_nice_format && ODIN_DEBUG {
		log.warn("Nice format could not be chosen!")
	}

	// https://www.intel.com/content/www/us/en/developer/articles/training/api-without-secrets-introduction-to-vulkan-part-2.html?language=en#inpage-nav-4-5
	// https://www.khronos.org/registry/vulkan/specs/1.3-extensions/man/html/VkPresentModeKHR.html

	// Immediate: When present is called, the new image is presented directly. No internal queues,
	// no reason to have any more than two images

	// Mailbox: Has a single slot for presentable image (which may be empty). Waits for vblank, and
	// presents image in slot, if present.
	// When present is called, image is put into slot. If there was already an image in the slot,
	// said image will be returned to the pool of image that can be aqcuired.
	// Two images means that application will be starved of an image to aqcuire after rendering a frame,
	// more than three images is redundant as there can always only be one image presenting, one image
	// in slot, and one image being rendered to.

	// Fifo: Has a queue for images waiting to be presented. Calling present adds image to queue.
	// One image will be removed from queue per vblank.
	// More images means more frames to smooth out performance, but higher input latency and
	// resource usage by images.

	// fifo_relaxed: Like fifo, but immediately presents image if a vblank has passed without an image
	// to present.

	// TODO[TS], p1: Look into how many images min/max are supported generally.
	present_mode_counts: u32
	vk.GetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_counts, nil)
	present_modes := make([]vk.PresentModeKHR, present_mode_counts)
	defer delete(present_modes)

	vk.GetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_mode_counts, raw_data(present_modes))

	surface_present_mode = .FIFO
	got_nice_present_mode := false

	for mode in present_modes {
		if mode == .MAILBOX {
			surface_present_mode = mode
			got_nice_present_mode = true
			break
		}
	}

	if !got_nice_present_mode && ODIN_DEBUG {
		log.warn("Nice present mode could not be chosen!")
	}

	return vulkan_surface
}
