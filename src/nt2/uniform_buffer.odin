package nt2

import vk "vendor:vulkan"

import "core:mem"

// TODO[TS], p1: Rethink UBOs entire when bindless is in
Uniform_Buffer :: struct($T: typeid) {
	// We might want several UBOs to point to a single struct when we use UBOs
	// in the swapchain frame data.
	// @todo: Are there enough use cases to warrant this being a part of this struct?
	data:   union {T, ^T},
	buffer: vk.Buffer,
	memory: vk.DeviceMemory,
}

uniform_buffer_create :: proc($T: typeid) -> (uniform_buffer: Uniform_Buffer(T)) {
	uniform_buffer.buffer, uniform_buffer.memory = buffer_create_single(
		size_of(T),
		{.UNIFORM_BUFFER},
		{.HOST_VISIBLE, .HOST_COHERENT})

	return uniform_buffer
}

uniform_buffer_destroy :: proc(uniform_buffer: ^Uniform_Buffer($T)) {
	vk.DestroyBuffer(vk_ctx.device, uniform_buffer.buffer, nil)
	vk.FreeMemory(vk_ctx.device, uniform_buffer.memory, nil)
}

uniform_buffer_upload :: proc(uniform_buffer: ^Uniform_Buffer($T)) {
	data: rawptr
	vk.MapMemory(vk_ctx.device, uniform_buffer.memory, 0, size_of(T), {}, &data)

	if data_ptr, ok := uniform_buffer.data.(^T); ok {
		mem.copy(data, data_ptr, cast(int)size_of(T))
	} else {
		mem.copy(data, &uniform_buffer.data, cast(int)size_of(T))
	}

	vk.UnmapMemory(vk_ctx.device, uniform_buffer.memory)
}

uniform_buffer_size_of_data :: proc(uniform_buffer: ^Uniform_Buffer($T)) -> int {return size_of(T)}

uniform_buffer_get_descriptor :: proc(uniform_buffer: ^Uniform_Buffer($T), offset: vk.DeviceSize = 0) -> vk.DescriptorBufferInfo {
	return vk.DescriptorBufferInfo {
		buffer = uniform_buffer.buffer,
		offset = offset,
		range = size_of(T),
	}
}
