package nt2

import "core:log"
import "core:os"

Loggers :: struct {
	console_logger: log.Logger,
	file_logger:    log.Logger,
	log_file:       os.Handle,
	shared_logger:  log.Logger,
}

loggers_initialize :: proc(using loggers: ^Loggers) {
	lowest :: log.Level.Debug

	console_logger_options := log.Options {
		.Level,
		// .Date,
		// .Time,
		// .Short_File_Path,
		// .Long_File_Path,
		// .Line,
		// .Procedure,
		.Terminal_Color,
	}

	file_logger_options := log.Options {
		.Level,
		// .Date,
		.Time,
		// .Short_File_Path,
		.Long_File_Path,
		.Line,
		.Procedure,
		// .Terminal_Color,
	}

	console_logger = log.create_console_logger(lowest, console_logger_options)

	err: os.Errno
	log_file, err = os.open("log.txt", os.O_WRONLY | os.O_CREATE | os.O_TRUNC)

	file_logger = log.create_file_logger(log_file, lowest, file_logger_options)

	shared_logger = log.create_multi_logger(console_logger, file_logger)
}

loggers_deinitialize :: proc(using loggers: ^Loggers) {
	log.destroy_multi_logger(&shared_logger)
	log.destroy_console_logger(console_logger)
	log.destroy_file_logger(&file_logger)
}
