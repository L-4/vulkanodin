package nt2

import vk "vendor:vulkan"

import "core:log"

// Loads extensions

vk_get_instance_proc_addr_or_panic :: proc(instance: vk.Instance, name: cstring) -> vk.ProcVoidFunction {
	func := vk.GetInstanceProcAddr(instance, name)

	if func == nil {
		log.panicf("Could not load Vulkan extension proc '%s'!\n", name)
	}

	return func
}

vk_load_EXT_debug_utils :: proc(instance: vk.Instance) {
	vk.CmdBeginDebugUtilsLabelEXT    = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkCmdBeginDebugUtilsLabelEXT")
	vk.CmdEndDebugUtilsLabelEXT      = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkCmdEndDebugUtilsLabelEXT")
	vk.CmdInsertDebugUtilsLabelEXT   = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkCmdInsertDebugUtilsLabelEXT")
	vk.CreateDebugUtilsMessengerEXT  = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkCreateDebugUtilsMessengerEXT")
	vk.DestroyDebugUtilsMessengerEXT = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkDestroyDebugUtilsMessengerEXT")
	vk.QueueBeginDebugUtilsLabelEXT  = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkQueueBeginDebugUtilsLabelEXT")
	vk.QueueEndDebugUtilsLabelEXT    = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkQueueEndDebugUtilsLabelEXT")
	vk.QueueInsertDebugUtilsLabelEXT = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkQueueInsertDebugUtilsLabelEXT")
	vk.SetDebugUtilsObjectNameEXT    = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkSetDebugUtilsObjectNameEXT")
	vk.SetDebugUtilsObjectTagEXT     = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkSetDebugUtilsObjectTagEXT")
	vk.SubmitDebugUtilsMessageEXT    = auto_cast vk_get_instance_proc_addr_or_panic(instance, "vkSubmitDebugUtilsMessageEXT")
}
