package nt2

g_renderer_system := System {
	name              = "Renderer",
	dependencies      = {},
	system_data_type  = struct{},
	initialize_proc   = renderer_initialize,
	deinitialize_proc = renderer_deinitialize,
}

renderer_initialize :: proc(_: rawptr) {
	window_initialize()
	initialize_context()
	fif_initialize()
	renderer_initialize_static_resources()
	command_buffer_initialize_default_pool()
	staging_buffer_initialize()
	pipeline_cache_load_or_create()
}

renderer_deinitialize :: proc(_: rawptr) {
	pipeline_cache_save_and_destroy()
	staging_buffer_deinitialize()
	command_buffer_deinitialize_default_pool()
	renderer_deinitialize_static_resources()
	fif_deinitialize()
	deinitialize_context()
	window_deinitialize()
}

renderer_initialize_static_resources :: proc() {
	sampler_create_default_samplers()
}

renderer_deinitialize_static_resources :: proc() {
	sampler_destroy_default_samplers()
}
