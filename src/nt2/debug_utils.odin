package nt2

import vk "vendor:vulkan"

import "core:strings"
import "core:fmt"

// This file exists to wrap EXT_debug_utils

// Debug procs to name Vulkan objects.
// @note: Everything has to be #force_inline for it to be optimized away at compile time!

@(private)
vk_name_object_by_handle :: #force_inline proc(object_handle: u64, object_type: vk.ObjectType, fmt_str: string, args: ..any) {
	when VK_USE_EXT_DEBUG_UTILS {
		name_fmt := fmt.tprintf(fmt_str, ..args)
		name_cstring := strings.clone_to_cstring(name_fmt, context.temp_allocator)

		object_name_info := vk.DebugUtilsObjectNameInfoEXT {
			sType        = .DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
			objectType   = object_type,
			objectHandle = object_handle,
			pObjectName  = name_cstring,
		}

		vk.SetDebugUtilsObjectNameEXT(vk_ctx.device, &object_name_info)
	}
}

// A whole buncha' overloads to name stuff.
// Handles
@(private) vk_name_instance :: proc(instance: vk.Instance, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)cast(uintptr)instance, .INSTANCE, fmt_str, ..args)}
@(private) vk_name_physical_device :: proc(physical_device: vk.PhysicalDevice, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)cast(uintptr)physical_device, .PHYSICAL_DEVICE, fmt_str, ..args)}
@(private) vk_name_device :: proc(device: vk.Device, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)cast(uintptr)device, .DEVICE, fmt_str, ..args)}
@(private) vk_name_queue :: proc(queue: vk.Queue, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)cast(uintptr)queue, .QUEUE, fmt_str, ..args)}
@(private) vk_name_command_buffer :: proc(command_buffer: vk.CommandBuffer, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)cast(uintptr)command_buffer, .COMMAND_BUFFER, fmt_str, ..args)}

// Non dispatchable handles
@(private) vk_name_buffer :: proc(buffer: vk.Buffer, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)buffer, .BUFFER, fmt_str, ..args)}
@(private) vk_name_image :: proc(image: vk.Image, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)image, .IMAGE, fmt_str, ..args)}
@(private) vk_name_semaphore :: proc(semaphore: vk.Semaphore, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)semaphore, .SEMAPHORE, fmt_str, ..args)}
@(private) vk_name_fence :: proc(fence: vk.Fence, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)fence, .FENCE, fmt_str, ..args)}
@(private) vk_name_device_memory :: proc(device_memory: vk.DeviceMemory, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)device_memory, .DEVICE_MEMORY, fmt_str, ..args)}
@(private) vk_name_event :: proc(event: vk.Event, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)event, .EVENT, fmt_str, ..args)}
@(private) vk_name_query_pool :: proc(query_pool: vk.QueryPool, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)query_pool, .QUERY_POOL, fmt_str, ..args)}
@(private) vk_name_buffer_view :: proc(buffer_view: vk.BufferView, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)buffer_view, .BUFFER_VIEW, fmt_str, ..args)}
@(private) vk_name_image_view :: proc(image_view: vk.ImageView, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)image_view, .IMAGE_VIEW, fmt_str, ..args)}
@(private) vk_name_shader_module :: proc(shader_module: vk.ShaderModule, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)shader_module, .SHADER_MODULE, fmt_str, ..args)}
@(private) vk_name_pipeline_cache :: proc(pipeline_cache: vk.PipelineCache, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)pipeline_cache, .PIPELINE_CACHE, fmt_str, ..args)}
@(private) vk_name_pipeline_layout :: proc(pipeline_layout: vk.PipelineLayout, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)pipeline_layout, .PIPELINE_LAYOUT, fmt_str, ..args)}
@(private) vk_name_pipeline :: proc(pipeline: vk.Pipeline, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)pipeline, .PIPELINE, fmt_str, ..args)}
@(private) vk_name_render_pass :: proc(render_pass: vk.RenderPass, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)render_pass, .RENDER_PASS, fmt_str, ..args)}
@(private) vk_name_descriptor_set_layout :: proc(descriptor_set_layout: vk.DescriptorSetLayout, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)descriptor_set_layout, .DESCRIPTOR_SET_LAYOUT, fmt_str, ..args)}
@(private) vk_name_sampler :: proc(sampler: vk.Sampler, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)sampler, .SAMPLER, fmt_str, ..args)}
@(private) vk_name_descriptor_set :: proc(descriptor_set: vk.DescriptorSet, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)descriptor_set, .DESCRIPTOR_SET, fmt_str, ..args)}
@(private) vk_name_descriptor_pool :: proc(descriptor_pool: vk.DescriptorPool, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)descriptor_pool, .DESCRIPTOR_POOL, fmt_str, ..args)}
@(private) vk_name_framebuffer :: proc(framebuffer: vk.Framebuffer, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)framebuffer, .FRAMEBUFFER, fmt_str, ..args)}
@(private) vk_name_command_pool :: proc(command_pool: vk.CommandPool, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)command_pool, .COMMAND_POOL, fmt_str, ..args)}
@(private) vk_name_sampler_ycbcr_conversion :: proc(sampler_ycbcr_conversion: vk.SamplerYcbcrConversion, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)sampler_ycbcr_conversion, .SAMPLER_YCBCR_CONVERSION, fmt_str, ..args)}
@(private) vk_name_descriptor_update_template :: proc(descriptor_update_template: vk.DescriptorUpdateTemplate, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)descriptor_update_template, .DESCRIPTOR_UPDATE_TEMPLATE, fmt_str, ..args)}
@(private) vk_name_surface_khr :: proc(surface_khr: vk.SurfaceKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)surface_khr, .SURFACE_KHR, fmt_str, ..args)}
@(private) vk_name_swapchain_khr :: proc(swapchain_khr: vk.SwapchainKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)swapchain_khr, .SWAPCHAIN_KHR, fmt_str, ..args)}
@(private) vk_name_display_khr :: proc(display_khr: vk.DisplayKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)display_khr, .DISPLAY_KHR, fmt_str, ..args)}
@(private) vk_name_display_mode_khr :: proc(display_mode_khr: vk.DisplayModeKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)display_mode_khr, .DISPLAY_MODE_KHR, fmt_str, ..args)}
@(private) vk_name_deferred_operation_khr :: proc(deferred_operation_khr: vk.DeferredOperationKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)deferred_operation_khr, .DEFERRED_OPERATION_KHR, fmt_str, ..args)}
@(private) vk_name_debug_report_callback_ext :: proc(debug_report_callback_ext: vk.DebugReportCallbackEXT, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)debug_report_callback_ext, .DEBUG_REPORT_CALLBACK_EXT, fmt_str, ..args)}
@(private) vk_name_debug_utils_messenger_ext :: proc(debug_utils_messenger_ext: vk.DebugUtilsMessengerEXT, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)debug_utils_messenger_ext, .DEBUG_UTILS_MESSENGER_EXT, fmt_str, ..args)}
@(private) vk_name_validation_cache_ext :: proc(validation_cache_ext: vk.ValidationCacheEXT, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)validation_cache_ext, .VALIDATION_CACHE_EXT, fmt_str, ..args)}
@(private) vk_name_acceleration_structure_nv :: proc(acceleration_structure_nv: vk.AccelerationStructureNV, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)acceleration_structure_nv, .ACCELERATION_STRUCTURE_NV, fmt_str, ..args)}
@(private) vk_name_performance_configuration_intel :: proc(performance_configuration_intel: vk.PerformanceConfigurationINTEL, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)performance_configuration_intel, .PERFORMANCE_CONFIGURATION_INTEL, fmt_str, ..args)}
@(private) vk_name_indirect_commands_layout_nv :: proc(indirect_commands_layout_nv: vk.IndirectCommandsLayoutNV, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)indirect_commands_layout_nv, .INDIRECT_COMMANDS_LAYOUT_NV, fmt_str, ..args)}
@(private) vk_name_private_data_slot_ext :: proc(private_data_slot_ext: vk.PrivateDataSlotEXT, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)private_data_slot_ext, .PRIVATE_DATA_SLOT_EXT, fmt_str, ..args)}
@(private) vk_name_acceleration_structure_khr :: proc(acceleration_structure_khr: vk.AccelerationStructureKHR, fmt_str: string, args: ..any) {vk_name_object_by_handle(cast(u64)acceleration_structure_khr, .ACCELERATION_STRUCTURE_KHR, fmt_str, ..args)}

vk_name_object :: proc {
	// Handles
	vk_name_instance,
	vk_name_physical_device,
	vk_name_device,
	vk_name_queue,
	vk_name_command_buffer,

	// Non dispatchable handles
	vk_name_buffer,
	vk_name_image,
	vk_name_semaphore,
	vk_name_fence,
	vk_name_device_memory,
	vk_name_event,
	vk_name_query_pool,
	vk_name_buffer_view,
	vk_name_image_view,
	vk_name_shader_module,
	vk_name_pipeline_cache,
	vk_name_pipeline_layout,
	vk_name_pipeline,
	vk_name_render_pass,
	vk_name_descriptor_set_layout,
	vk_name_sampler,
	vk_name_descriptor_set,
	vk_name_descriptor_pool,
	vk_name_framebuffer,
	vk_name_command_pool,
	vk_name_sampler_ycbcr_conversion,
	vk_name_descriptor_update_template,
	vk_name_surface_khr,
	vk_name_swapchain_khr,
	vk_name_display_khr,
	vk_name_display_mode_khr,
	vk_name_deferred_operation_khr,
	vk_name_debug_report_callback_ext,
	vk_name_debug_utils_messenger_ext,
	vk_name_validation_cache_ext,
	vk_name_acceleration_structure_nv,
	vk_name_performance_configuration_intel,
	vk_name_indirect_commands_layout_nv,
	vk_name_private_data_slot_ext,
	vk_name_acceleration_structure_khr,
}

@(private="file")
get_debug_label_info :: proc(color: [4]f32, name_cstring: cstring) -> vk.DebugUtilsLabelEXT {
	return vk.DebugUtilsLabelEXT {
		sType      = .DEBUG_UTILS_LABEL_EXT,
		pLabelName = name_cstring,
		color      = color,
	}
}

vk_cmd_begin_debug_label :: #force_inline proc(command_buffer: vk.CommandBuffer, color: [4]f32, fmt_str: string, args: ..any) {
	when VK_USE_EXT_DEBUG_UTILS {
		name_fmt := fmt.tprintf(fmt_str, ..args)
		name_cstring := strings.clone_to_cstring(name_fmt, context.temp_allocator)

		debug_label_info := get_debug_label_info(color, name_cstring)
		vk.CmdBeginDebugUtilsLabelEXT(command_buffer, &debug_label_info)
	}
}

vk_cmd_end_debug_label :: #force_inline proc(command_buffer: vk.CommandBuffer) {
	when VK_USE_EXT_DEBUG_UTILS {
		vk.CmdEndDebugUtilsLabelEXT(command_buffer)
	}
}
vk_cmd_insert_debug_label :: #force_inline proc(command_buffer: vk.CommandBuffer, color: [4]f32, fmt_str: string, args: ..any) {
	when VK_USE_EXT_DEBUG_UTILS {
		name_fmt := fmt.tprintf(fmt_str, ..args)
		name_cstring := strings.clone_to_cstring(name_fmt, context.temp_allocator)

		debug_label_info := get_debug_label_info(color, name_cstring)
		vk.CmdInsertDebugUtilsLabelEXT(command_buffer, &debug_label_info)
	}
}

vk_queue_begin_debug_label :: #force_inline proc(queue: vk.Queue, color: [4]f32, fmt_str: string, args: ..any) {
	when VK_USE_EXT_DEBUG_UTILS {
		name_fmt := fmt.tprintf(fmt_str, ..args)
		name_cstring := strings.clone_to_cstring(name_fmt, context.temp_allocator)

		debug_label_info := get_debug_label_info(color, name_cstring)
		vk.QueueBeginDebugUtilsLabelEXT(queue, &debug_label_info)
	}
}

vk_queue_end_debug_label :: #force_inline proc(queue: vk.Queue) {
	when VK_USE_EXT_DEBUG_UTILS {
		vk.QueueEndDebugUtilsLabelEXT(queue)
	}
}

vk_queue_insert_debug_label :: #force_inline proc(queue: vk.Queue, color: [4]f32, fmt_str: string, args: ..any) {
	when VK_USE_EXT_DEBUG_UTILS {
		name_fmt := fmt.tprintf(fmt_str, ..args)
		name_cstring := strings.clone_to_cstring(name_fmt, context.temp_allocator)

		debug_label_info := get_debug_label_info(color, name_cstring)
		vk.QueueInsertDebugUtilsLabelEXT(queue, &debug_label_info)
	}
}
