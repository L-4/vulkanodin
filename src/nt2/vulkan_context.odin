package nt2

import vk "vendor:vulkan"
import sdl "vendor:sdl2"

import "core:log"

// These have to be declared here as vk.MAKE_VERSION can't be constexpr'd
API_VERSION_1_0 :: (1<<22) | (0<<12) | (0)
API_VERSION_1_1 :: (1<<22) | (1<<12) | (0)
API_VERSION_1_2 :: (1<<22) | (2<<12) | (0)
API_VERSION_1_3 :: (1<<22) | (3<<12) | (0)

VK_API_VERSION_VARIANT :: proc(version: u32) -> u32 { return (version >> 29) }
VK_API_VERSION_MAJOR   :: proc(version: u32) -> u32 { return (version >> 22) & 0x7F }
VK_API_VERSION_MINOR   :: proc(version: u32) -> u32 { return (version >> 12) & 0x3FF }
VK_API_VERSION_PATCH   :: proc(version: u32) -> u32 { return (version & 0xFFF) }

// Vulkan application API version. Features at or below this version come from
// core, feature above this version come from extensions.
USED_VK_API_VERSION :: API_VERSION_1_3

// TODO[TS], p4: Rethink context. More things should go here - swapchain
Vulkan_Context :: struct {
	// Fully static handles.
	instance:             vk.Instance,
	instance_api_version: u32,

	using vulkan_surface: Vulkan_Surface,
	using vulkan_device:  Vulkan_Device,

	// May be 0
	debug_messenger: vk.DebugUtilsMessengerEXT,
}

vk_ctx: ^Vulkan_Context

initialize_context :: proc() {
	assert(vk_ctx == nil)
	vk_ctx = new(Vulkan_Context)

	vk.GetInstanceProcAddr = cast(vk.ProcGetInstanceProcAddr)sdl.Vulkan_GetVkGetInstanceProcAddr()

	vk.load_proc_addresses_global(cast(rawptr)vk.GetInstanceProcAddr)

	vk.EnumerateInstanceVersion(&vk_ctx.instance_api_version)
	assert(vk_ctx.instance_api_version >= USED_VK_API_VERSION)

	// Instance version comes from loader
	log.infof("Vulkan instance version %d.%d.%d",
		VK_API_VERSION_MAJOR(vk_ctx.instance_api_version),
		VK_API_VERSION_MINOR(vk_ctx.instance_api_version),
		VK_API_VERSION_PATCH(vk_ctx.instance_api_version))

	instance_create_info := vk.InstanceCreateInfo {
		sType            = .INSTANCE_CREATE_INFO,
		pApplicationInfo = &vk.ApplicationInfo {
			sType              = .APPLICATION_INFO,
			pApplicationName   = "nt2_2dg_vk",
			applicationVersion = vk.MAKE_VERSION(1, 0, 0),
			pEngineName        = "nt2_vk",
			engineVersion      = vk.MAKE_VERSION(1, 0, 0),
			apiVersion         = USED_VK_API_VERSION,
		},
	}

	instance_create_info_next := cast(^vk.BaseInStructure)&instance_create_info

	// Get required instance_extensions from SDL
	required_extension_count: u32
	sdl.Vulkan_GetInstanceExtensions(g_window.sdl_window, &required_extension_count, nil)

	// Note that this becomes len() == count && cap() == count, which is fine since we're about
	// to fill it without incrementing len().
	instance_extensions := make([dynamic]cstring, required_extension_count)
	defer delete(instance_extensions)

	sdl.Vulkan_GetInstanceExtensions(g_window.sdl_window, &required_extension_count, raw_data(instance_extensions))

	instance_layers := make([dynamic]cstring)
	defer delete(instance_layers)

	// TODO[TS], p3: If we don't enable debug utils, then we need to make sure to never use object
	// naming, messenger creation, debug label cmds! Right now we just do it anyway.
	when VK_USE_EXT_DEBUG_UTILS do append(&instance_extensions, vk.EXT_DEBUG_UTILS_EXTENSION_NAME)

	when VK_USE_VALIDATION do append(&instance_layers, "VK_LAYER_KHRONOS_validation")
	when VK_USE_DEVSIM do append(&instance_layers, "VK_LAYER_LUNARG_device_simulation")
	when VK_API_DUMP do append(&instance_layers, "VK_LAYER_LUNARG_api_dump")

	when VK_USE_EXTENDED_VALIDATION {
		append(&instance_extensions, vk.EXT_VALIDATION_FEATURES_EXTENSION_NAME)

		enabled_features := []vk.ValidationFeatureEnableEXT {
			.GPU_ASSISTED,
			.GPU_ASSISTED_RESERVE_BINDING_SLOT,
			.BEST_PRACTICES,
			// It seems that we never want to enable this. Doing so disallows use of gpu assisted
			// validation, and is also not required to see the outputs of the printfs.
			// .DEBUG_PRINTF,
			.SYNCHRONIZATION_VALIDATION,
		}

		disabled_features := []vk.ValidationFeatureDisableEXT {}

		validation_features := vk.ValidationFeaturesEXT {
			sType                          = .VALIDATION_FEATURES_EXT,
			enabledValidationFeatureCount  = cast(u32)len(enabled_features),
			pEnabledValidationFeatures     = raw_data(enabled_features),
			disabledValidationFeatureCount = cast(u32)len(disabled_features),
			pDisabledValidationFeatures    = raw_data(disabled_features),
		}

		// TODO[TS] p4: Find extensible way of doing pNext chains
		instance_create_info_next.pNext = cast(^vk.BaseInStructure)&validation_features
		instance_create_info_next = instance_create_info_next.pNext
	}

	when VK_USE_VALIDATION {
		debug_messenger_info_copy := global_debug_messenger_info

		instance_create_info_next.pNext = cast(^vk.BaseInStructure)&debug_messenger_info_copy
		instance_create_info_next = instance_create_info_next.pNext

		set_debug_messenger_context()
	}

	instance_create_info.ppEnabledExtensionNames = raw_data(instance_extensions)
	instance_create_info.enabledExtensionCount = cast(u32)len(instance_extensions)

	instance_create_info.ppEnabledLayerNames = raw_data(instance_layers)
	instance_create_info.enabledLayerCount = cast(u32)len(instance_layers)

	vk_assert(vk.CreateInstance(&instance_create_info, nil, &vk_ctx.instance))

	when VK_USE_EXT_DEBUG_UTILS {
		// @TODO[TS], p3: Not required?
		vk_load_EXT_debug_utils(vk_ctx.instance)
	}

	vk.load_proc_addresses_instance(vk_ctx.instance)

	when VK_USE_VALIDATION {
		debug_messenger_initialize()
	}

	vk_ctx.vulkan_device = vulkan_device_create(with_present_support_for = g_window.sdl_window)
	vk_ctx.vulkan_surface = vulkan_surface_create(&vk_ctx.vulkan_device)

	// Only now is the Vulkan context fully initialized.
}

deinitialize_context :: proc() {
	assert(vk_ctx != nil)

	vulkan_device_destroy(&vk_ctx.vulkan_device)

	vk.DestroySurfaceKHR(vk_ctx.instance, vk_ctx.surface, nil)

	when VK_USE_VALIDATION {
		debug_messenger_deinitialize()
	}

	vk.DestroyInstance(vk_ctx.instance, nil)
	free(vk_ctx)
}
