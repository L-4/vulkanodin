package nt2

import vk "vendor:vulkan"
import sdl "vendor:sdl2"

import "core:log"

Queue :: struct {
	queue:  vk.Queue,
	family: u32,
}

vulkan_device_get_queue_family :: proc(
	vulkan_device: ^Vulkan_Device,
	required_flags: vk.QueueFlags,
	with_present_support_for: ^sdl.Window = nil,
) -> (
	queue_family: u32,
	ok: bool,
) {
	using vulkan_device

	queue_family_count: u32
	vk.GetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, nil)

	requires_surface := with_present_support_for != nil
	surface: vk.SurfaceKHR

	// If we want present support, create a dummy surface to test against. This isn't strictly the
	// correcy way to do this, but it makes the code a lot cleaner as a surface doesn't have to be
	// created before we create the phyiscal and logical device.
	if requires_surface {
		assert(cast(bool)sdl.Vulkan_CreateSurface(with_present_support_for, vk_ctx.instance, &surface))
	}
	defer if requires_surface {
		vk.DestroySurfaceKHR(vk_ctx.instance, surface, nil);
	}

	for family_properties, family_index in _slice(&queue_family_properties) {
		if !(family_properties.queueFlags >= required_flags) do continue

		if requires_surface {
			has_present_support: b32
			vk.GetPhysicalDeviceSurfaceSupportKHR(
				physical_device,
				cast(u32)family_index,
				surface,
				&has_present_support,
			)

			if !has_present_support do continue
		}

		queue_family = cast(u32)family_index
		ok = true

		return
	}

	ok = false

	return
}

Vulkan_Device :: struct {
	physical_device:            vk.PhysicalDevice,
	device_api_version:         u32,
	physical_device_properties: vk.PhysicalDeviceProperties,
	physical_device_features:   vk.PhysicalDeviceFeatures,
	queue_family_properties:    Small_Array(8, vk.QueueFamilyProperties),
	device:                     vk.Device,

	combined_queue: Queue,

	// If non nil, indicates that device was created to support presenting to this window.
	supported_window_for_present: ^sdl.Window,
}

// If a SDL window is passed, device is set up to be able to present to it.
vulkan_device_create :: proc(with_present_support_for: ^sdl.Window = nil) -> Vulkan_Device {
	vulkan_device := Vulkan_Device {}

	vulkan_device.supported_window_for_present = with_present_support_for

	vulkan_device.physical_device = vulkan_device_select_physical_device()

	vk.GetPhysicalDeviceProperties(vulkan_device.physical_device, &vulkan_device.physical_device_properties)
	vk.GetPhysicalDeviceFeatures(vulkan_device.physical_device, &vulkan_device.physical_device_features)
	queue_family_count: u32
	vk.GetPhysicalDeviceQueueFamilyProperties(vulkan_device.physical_device, &queue_family_count, nil)
	assert(queue_family_count <= u32(_cap(vulkan_device.queue_family_properties)))
	_resize(&vulkan_device.queue_family_properties, int(queue_family_count))
	vk.GetPhysicalDeviceQueueFamilyProperties(vulkan_device.physical_device, &queue_family_count, _raw_data(&vulkan_device.queue_family_properties))

	// Device version comes from driver
	vulkan_device.device_api_version = vulkan_device.physical_device_properties.apiVersion
	log.infof("Vulkan device version %d.%d.%d",
		VK_API_VERSION_MAJOR(vulkan_device.device_api_version),
		VK_API_VERSION_MINOR(vulkan_device.device_api_version),
		VK_API_VERSION_PATCH(vulkan_device.device_api_version))

	device_features := vk.PhysicalDeviceFeatures2 {
		sType = .PHYSICAL_DEVICE_FEATURES_2,
		features = vk.PhysicalDeviceFeatures {
			samplerAnisotropy = true,
		},
	}

	vulkan_13_features := vk.PhysicalDeviceVulkan13Features {
		sType            = .PHYSICAL_DEVICE_VULKAN_1_3_FEATURES,
		dynamicRendering = true,
	}

	device_features.pNext = auto_cast &vulkan_13_features

	device_extensions := make([dynamic]cstring)
	defer delete(device_extensions)

	// KHR_SWAPCHAIN
	append(&device_extensions, vk.KHR_SWAPCHAIN_EXTENSION_NAME)

	// Create queue
	ok: bool
	vulkan_device.combined_queue.family, ok = vulkan_device_get_queue_family(
		&vulkan_device,
		{.GRAPHICS, .TRANSFER},
		with_present_support_for)

	if !ok {
		log.panic("Could not find combined queue family!")
	}

	DEFAULT_QUEUE_PRIORITY: f32 = 1.0
	queue_create_infos := []vk.DeviceQueueCreateInfo{
		{
			sType            = .DEVICE_QUEUE_CREATE_INFO,
			queueFamilyIndex = vulkan_device.combined_queue.family,
			queueCount       = 1,
			pQueuePriorities = &DEFAULT_QUEUE_PRIORITY,
		},
	}

	// Create device
	device_create_info := vk.DeviceCreateInfo {
		sType                   = .DEVICE_CREATE_INFO,
		pNext                   = &device_features,
		pQueueCreateInfos       = raw_data(queue_create_infos),
		queueCreateInfoCount    = cast(u32)len(queue_create_infos),
		pEnabledFeatures        = nil, // As we're using PhysicalDeviceFeatures2
		ppEnabledExtensionNames = raw_data(device_extensions),
		enabledExtensionCount   = cast(u32)len(device_extensions),
	}

	vk_assert(vk.CreateDevice(vulkan_device.physical_device, &device_create_info, nil, &vulkan_device.device))

	vk.load_proc_addresses_device(vulkan_device.device)

	// Only once we've created the device can we also retrieve the queue.
	vk.GetDeviceQueue(vulkan_device.device, vulkan_device.combined_queue.family, 0, &vulkan_device.combined_queue.queue)

	return vulkan_device
}

vulkan_device_destroy :: proc(vulkan_device: ^Vulkan_Device) {
	vk_assert(vk.QueueWaitIdle(vulkan_device.combined_queue.queue))
	vk_assert(vk.DeviceWaitIdle(vulkan_device.device))

	vk.DestroyDevice(vulkan_device.device, nil)
}

// Queries the physical device to check whether given format is valid as a vertex attribute.
physical_device_is_vertex_format_supported :: proc(format: vk.Format) -> bool {
	fmt_props: vk.FormatProperties
	vk.GetPhysicalDeviceFormatProperties(vk_ctx.physical_device, format, &fmt_props)

	return .VERTEX_BUFFER in fmt_props.bufferFeatures
}

vulkan_device_select_physical_device :: proc() -> vk.PhysicalDevice {
	device_count: u32
	vk.EnumeratePhysicalDevices(vk_ctx.instance, &device_count, nil)

	devices := make([]vk.PhysicalDevice, device_count)
	defer delete(devices)
	vk.EnumeratePhysicalDevices(vk_ctx.instance, &device_count, raw_data(devices))

	if device_count == 0 {
		log.panic("No physical devices available!")
	}

	device_properties: vk.PhysicalDeviceProperties

	selected_device_idx := u32(0)

	// Try to find discrete gpu, or use whatever is at index 0
	for device, device_idx in devices {
		vk.GetPhysicalDeviceProperties(device, &device_properties)

		if device_properties.deviceType == .DISCRETE_GPU {
			selected_device_idx = cast(u32)device_idx
		}
	}

	return devices[selected_device_idx]
}
