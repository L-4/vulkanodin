package nt2

Vertex_3D :: struct #packed {
	position: [3]f32,                                // R32G32B32_SFLOAT
	normal:   u32     `pack32_type:"normal_packed"`, // A2B10G10R10_SNORM_PACK32
	tangent:  u32     `pack32_type:"normal_packed"`, // A2B10G10R10_SNORM_PACK32
	texcoord: [2]u16  `int_type:"norm"`,             // R16G16_UNORM
	color:    [4]u8   `int_type:"norm"`,             // R8G8B8A8_UNORM
}

Vertex_2D :: struct #packed {
	position: [2]f32,                     // R32G32_SFLOAT
	texcoord: [2]u16  `int_type:"norm"`,  // R16G16_UNORM
	color:    [4]u8   `int_type:"norm"`,  // R8G8B8A8_UNORM
}
