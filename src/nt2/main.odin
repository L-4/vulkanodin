package nt2

import "core:log"
import "core:mem"

import sdl "vendor:sdl2"
import vk "vendor:vulkan"

TRACK_ALLOCATIONS :: #config(TRACK_ALLOCATIONS, false)
g_running := true
handle_quit_events :: proc(e: ^sdl.Event) -> Handled {
	#partial switch e.type {
	case .QUIT: g_running = false; return handle_event(e)
	case .KEYDOWN: {
		if e.key.keysym.sym == .ESCAPE {
			g_running = false
			return handle_event(e)
		}
	}
	}

	return Not_Handled
}

main :: proc() {

	loggers := Loggers{}
	loggers_initialize(&loggers)
	defer loggers_deinitialize(&loggers)

	context.logger = loggers.shared_logger

	when TRACK_ALLOCATIONS {
		allocator := mem.Tracking_Allocator{}
		mem.tracking_allocator_init(&allocator, context.allocator)
		context.allocator = mem.tracking_allocator(&allocator)
	}

	{
		system_manager_register(&g_renderer_system)

		system_manager_init_all()
		defer system_manager_deinit_all()

		swapchain := swapchain_create()
		defer swapchain_destroy(&swapchain)

		imgui_initialize_vk(g_window.sdl_window, swapchain.renderpass, swapchain.image_count)
		defer imgui_deinitialize()

		w, h := window_get_size()
		rt_descriptor := Render_Target_Descriptor {
			size        = {u32(w), u32(h)},
			has_depth   = false,
			has_stencil = false,
		}
		_push_back_elems(&rt_descriptor.color_formats, vk.Format.B10G11R11_UFLOAT_PACK32)
		render_target := render_target_create(rt_descriptor)
		defer render_target_destroy(&render_target)

		triangle_test_initialize(rt_descriptor)
		defer triangle_test_deinitialize()

		vk_assert(vk.QueueWaitIdle(vk_ctx.combined_queue.queue))

		for g_running {
			// Handle events
			e: sdl.Event
			for sdl.PollEvent(&e) {
				// Important: imgui needs to be first as to deny events if necessary
				if imgui_handle_event(&e) do continue
				if handle_quit_events(&e) do continue
			}

			imgui_newframe()

			// Render
			// 1. Request frame from swapchain
			swapchain_begin_frame(&swapchain)

			// TODO[TS]: Custom resolve shader is required here
			// buf := command_buffer_begin_single_use()

			// render_target_bind(&render_target, buf)
			// triangle_test_render(buf)
			// vk.CmdEndRendering(buf)

			// command_buffer_end_single_use(buf)

			// 2. Do drawing (not to backbuffer)

			// 3. Do required drawing to backbuffer.
			command_buffer := swapchain_begin_backbuffer_draw(&swapchain)

			// vk.CmdResolveImage(
			// 	command_buffer,
			// 	render_target.color_attachments.data[0].image,
			// 	.TRANSFER_SRC_OPTIMAL,
			// 	swapchain.image_data[swapchain.frame_data[fif_get_current_index()].image_index].image,
			// 	)

			imgui_render_vk(command_buffer)

			// 5. Draw your stuff to backbuffer.
			swapchain_end_backbuffer_draw(&swapchain)

			// 4. Submit frame
			swapchain_present_frame(&swapchain)

			// Free temp allocations from this frame
			free_all(context.temp_allocator)
		}

		vk_assert(vk.QueueWaitIdle(vk_ctx.combined_queue.queue))
	}

	when TRACK_ALLOCATIONS {
		if len(allocator.allocation_map) > 0 {
			log.error("===== LEAKS =====")

			for _, allocation in allocator.allocation_map {
				log.errorf("%d bytes not freed! Allocated at %s", allocation.size, allocation.location)
			}
		}

		mem.tracking_allocator_destroy(&allocator)
	}
}
