package nt2

import vk "vendor:vulkan"

// AccessFlag = specific operation (read from depth bufer, write to attachment etc)
// PipelineStageFlag = point in pipeline. Logically expanded in execution barriers.
// (But not in memory barriers, because the only thing we're interested in when
// transitioning images is the single point in time where the transition happens,
// and it will never happen later than the dstStage, or earlier than the srcStage.
// Hence we don't care about expanding the stages.)

// Slightly less verbose single image layout transition
image_transition_layout :: proc(
	command_buffer: vk.CommandBuffer,
	image: ^Image,
	old_layout, new_layout: vk.ImageLayout,
	src_stages: vk.PipelineStageFlags,
	src_access: vk.AccessFlags,
	dst_stages: vk.PipelineStageFlags,
	dst_access: vk.AccessFlags) {

	assert(old_layout == image.layout)

	barrier := vk.ImageMemoryBarrier {
		sType               = .IMAGE_MEMORY_BARRIER,
		pNext               = nil,
		srcAccessMask       = src_access,
		dstAccessMask       = dst_access,
		oldLayout           = old_layout,
		newLayout           = new_layout,
		srcQueueFamilyIndex = vk.QUEUE_FAMILY_IGNORED,
		dstQueueFamilyIndex = vk.QUEUE_FAMILY_IGNORED,
		image               = image.image,
		subresourceRange    = image_get_full_subresource(image^, {.COLOR}), // TODO[TS]: TODO-1
	}

	vk.CmdPipelineBarrier(
		command_buffer,
		src_stages,
		dst_stages,
		{},
		0, nil,
		0, nil,
		1, &barrier)

	image.layout = new_layout
}
