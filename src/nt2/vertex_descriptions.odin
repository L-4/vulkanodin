package nt2

import vk "vendor:vulkan"

import "core:reflect"
import "core:log"
import "core:runtime"

// Valid int_types: norm, scaled, int (as in (us)norm, (us)scaled, (us)int)
// Sign is gathered from the passed int type.
// Valid float_types: (nothing)
// Valid pack32_types: normal_packed (=A2B10G10R10_SNORM_PACK32)
// Important: struct must have exactly the memory layout of the generated descriptor!

// Example:
// Vertex_3D :: struct #packed {
// 	position: [3]f32,                                // R32G32B32_SFLOAT
// 	normal:   u32     `pack32_type:"normal_packed"`, // A2B10G10R10_SNORM_PACK32
// 	tangent:  u32     `pack32_type:"normal_packed"`, // A2B10G10R10_SNORM_PACK32
// 	texcoord: [2]u16  `int_type:"norm"`,             // R16G16_UNORM
// 	color:    [4]u8   `int_type:"norm"`,             // R8G8B8A8_UNORM
// }

Vertex_Format_Descriptor :: struct {
	binding_description:   vk.VertexInputBindingDescription,
	attribute_descriptors: []vk.VertexInputAttributeDescription,
}

// TODO[TS], p1: Handle failure
// TODO[TS], p1: Deal with multiple input bindings
fill_vertex_binding_attribute_descriptions :: proc(
	$V: typeid,
	binding_description: ^vk.VertexInputBindingDescription,
	attribute_descriptions: ^[]vk.VertexInputAttributeDescription) {

	when ODIN_DEBUG {
		if (size_of(V) & 0b11) != 0 {
			log.warnf("Vertex struct {} is not four byte aligned! (is {} bytes)", type_info_of(V), size_of(V))
		}
	}

	binding_description^ = vk.VertexInputBindingDescription {
		binding = 0,
		stride = size_of(V),
		inputRate = .VERTEX,
	}

	types              := reflect.struct_field_types(V)
	struct_field_count := len(types)

	if struct_field_count > len(attribute_descriptions) {
		log.fatalf("More vertex attributes present than room was allocated! (change the static array to be larger than [%d])\n",
			len(attribute_descriptions))
	}

	attribute_descriptions^ = attribute_descriptions[:struct_field_count]

	using runtime

	for type_idx in 0..<struct_field_count {
		field := reflect.struct_field_at(V, type_idx)
		type  := types[type_idx]

		// Initialize constant parts of descriptor
		descriptor := &attribute_descriptions[type_idx]
		descriptor.binding = 0
		descriptor.location = cast(u32)type_idx
		descriptor.offset = cast(u32)field.offset
		descriptor.format = vk.Format.UNDEFINED // Explicitly initialize to undefined (even though this should be zero-filled)

		if array_type, ok := type.variant.(Type_Info_Array); ok {
			array_element_size  := array_type.elem_size
			array_element_count := array_type.count

			if array_int_type, ok := array_type.elem.variant.(Type_Info_Integer); ok {
				array_element_signed := array_int_type.signed
				descriptor.format = vertex_description_get_int_attribute_format(array_element_count, array_element_size, array_element_signed, field.tag)
			} else if array_float_type, ok := array_type.elem.variant.(Type_Info_Float); ok {
				descriptor.format = vertex_description_get_float_attribute_format(array_element_count, array_element_size, field.tag)
			} else {
				log.panic("Invalid attribute type!")
			}
		} else if int_type, ok := type.variant.(Type_Info_Integer); ok {
			descriptor.format = vertex_description_get_pack32_attribute_format(field.tag)
		} else {
			log.panic("Invalid attribute type!")
		}

		if descriptor.format == .UNDEFINED {
			log.panicf("Could not find vertex format for type: %#v", field)
		}
		assert(physical_device_is_vertex_format_supported(descriptor.format), "Given format is not valid for vertex buffers!")
	}
}

vertex_description_get_float_attribute_format :: proc(components, component_size: int,
                                                      tag: reflect.Struct_Tag) -> vk.Format {

	assert(component_size == 4, "Float bytes != 4 not supported.")

	switch components {
	case 1: return .R32_SFLOAT
	case 2: return .R32G32_SFLOAT
	case 3: return .R32G32B32_SFLOAT
	case 4: return .R32G32B32A32_SFLOAT
	}

	return .UNDEFINED
}

vertex_description_get_int_attribute_format :: proc(components, component_size: int,
                                                    signed: bool,
                                                    tag: reflect.Struct_Tag) -> vk.Format {

	switch reflect.struct_tag_get(tag, "int_type") {
	case "int": if signed {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_SINT
				case 2: return .R8G8_SINT
				case 3: return .R8G8B8_SINT
				case 4: return .R8G8B8A8_SINT
				}
			case 2: switch (components) {
				case 1: return .R16_SINT
				case 2: return .R16G16_SINT
				case 3: return .R16G16B16_SINT
				case 4: return .R16G16B16A16_SINT
				}
			case 4: switch (components) {
				case 1: return .R32_SINT
				case 2: return .R32G32_SINT
				case 3: return .R32G32B32_SINT
				case 4: return .R32G32B32A32_SINT
				}
			case 8: switch (components) {
				case 1: return .R64_SINT
				case 2: return .R64G64_SINT
				case 3: return .R64G64B64_SINT
				case 4: return .R64G64B64A64_SINT
				}
			}
		} else {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_UINT
				case 2: return .R8G8_UINT
				case 3: return .R8G8B8_UINT
				case 4: return .R8G8B8A8_UINT
				}
			case 2: switch (components) {
				case 1: return .R16_UINT
				case 2: return .R16G16_UINT
				case 3: return .R16G16B16_UINT
				case 4: return .R16G16B16A16_UINT
				}
			case 4: switch (components) {
				case 1: return .R32_UINT
				case 2: return .R32G32_UINT
				case 3: return .R32G32B32_UINT
				case 4: return .R32G32B32A32_UINT
				}
			case 8: switch (components) {
				case 1: return .R64_UINT
				case 2: return .R64G64_UINT
				case 3: return .R64G64B64_UINT
				case 4: return .R64G64B64A64_UINT
				}
			}
		}
	case "norm": if signed {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_SNORM
				case 2: return .R8G8_SNORM
				case 3: return .R8G8B8_SNORM
				case 4: return .R8G8B8A8_SNORM
				}
			case 2: switch (components) {
				case 1: return .R16_SNORM
				case 2: return .R16G16_SNORM
				case 3: return .R16G16B16_SNORM
				case 4: return .R16G16B16A16_SNORM
				}
			case 4:fallthrough
			case 8: { log.panic("Normalized int attributes don't exist for sizes larger than two bytes!") }
			}
		} else {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_UNORM
				case 2: return .R8G8_UNORM
				case 3: return .R8G8B8_UNORM
				case 4: return .R8G8B8A8_UNORM
				}
			case 2: switch (components) {
				case 1: return .R16_UNORM
				case 2: return .R16G16_UNORM
				case 3: return .R16G16B16_UNORM
				case 4: return .R16G16B16A16_UNORM
				}
			case 4:fallthrough
			case 8: { log.panic("Normalized int attributes don't exist for sizes larger than two bytes!") }
			}
		}
	case "scaled": if signed {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_SSCALED
				case 2: return .R8G8_SSCALED
				case 3: return .R8G8B8_SSCALED
				case 4: return .R8G8B8A8_SSCALED
				}
			case 2: switch (components) {
				case 1: return .R16_SSCALED
				case 2: return .R16G16_SSCALED
				case 3: return .R16G16B16_SSCALED
				case 4: return .R16G16B16A16_SSCALED
				}
			case 4:fallthrough
			case 8: { log.panic("Scaled int attributes don't exist for sizes larger than two bytes!") }
			}
		} else {
			switch component_size {
			case 1: switch (components) {
				case 1: return .R8_USCALED
				case 2: return .R8G8_USCALED
				case 3: return .R8G8B8_USCALED
				case 4: return .R8G8B8A8_USCALED
				}
			case 2: switch (components) {
				case 1: return .R16_USCALED
				case 2: return .R16G16_USCALED
				case 3: return .R16G16B16_USCALED
				case 4: return .R16G16B16A16_USCALED
				}
			case 4:fallthrough
			case 8: { log.panic("Scaled int attributes don't exist for sizes larger than two bytes!") }
			}
		}
	}

	return .UNDEFINED
}

// @see: 33.1.4. Representation and Texel Block Size
vertex_description_get_pack32_attribute_format :: proc(tag: reflect.Struct_Tag) -> vk.Format {
	switch reflect.struct_tag_get(tag, "pack32_type") {
	case "normal_packed": return .A2B10G10R10_SNORM_PACK32
	}

	return .UNDEFINED
}
