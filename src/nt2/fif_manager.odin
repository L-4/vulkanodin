package nt2

import vk "vendor:vulkan"

// TODO[TS]: frame_manager?
// Manages the current and total fif count, as well as semaphores required,
// and making sure that only one person signals the frame end per frame.

// Currently the number of frames in flight is constant
FIF_COUNT :: 2

Fif_Data :: struct {
	fif_complete_fences:  [FIF_COUNT]vk.Fence,
	current_frame_index:  int,
	frame_status:         enum { In_Progress, Submitted, Complete },
}

fif_data: Fif_Data

fif_initialize :: proc() {
	fif_data.current_frame_index  = 0
	fif_data.frame_status         = .Complete

	fence_info := vk.FenceCreateInfo {
		sType = .FENCE_CREATE_INFO,
		flags = {.SIGNALED},
	}

	for fif_index in 0..<FIF_COUNT {
		vk_assert(vk.CreateFence(vk_ctx.device, &fence_info, nil, &fif_data.fif_complete_fences[fif_index]))
	}
}

fif_deinitialize :: proc() {
	for fif_index in 0..<FIF_COUNT {
		vk.DestroyFence(vk_ctx.device, fif_data.fif_complete_fences[fif_index], nil)
	}
}

fif_begin_frame :: proc() {
	assert(fif_data.frame_status == .Complete)
	vk_assert(vk.WaitForFences(vk_ctx.device, 1, &fif_data.fif_complete_fences[fif_data.current_frame_index], true, max(u64)))

	fif_data.frame_status = .In_Progress
}

// Whatever happens to be the last thing to happen in a frame, should signal
// this fence once complete.
fif_get_fence :: proc() -> vk.Fence {
	assert(fif_data.frame_status == .In_Progress)
	fif_data.frame_status = .Submitted

	fence := fif_data.fif_complete_fences[fif_data.current_frame_index]
	vk_assert(vk.ResetFences(vk_ctx.device, 1, &fence))

	return fence
}

// Increments the fif index.
fif_end_frame :: proc() {
	assert(fif_data.frame_status == .Submitted)
	fif_data.frame_status = .Complete

	fif_data.current_frame_index = (fif_data.current_frame_index + 1) % FIF_COUNT
}

fif_get_current_index :: proc() -> int {return fif_data.current_frame_index}
