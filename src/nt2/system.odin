package nt2

import "core:log"
import "core:mem"
import "core:builtin"

// TODO[TS], p2: Ensure no duplicate dependencies. Acyclic checks are done later
System :: struct {
	name: string,
	dependencies: []^System,

	system_data_type: typeid,
	// System manager will allocate data for systems, and pointer to this type will be passed to init/deinit
	initialize_proc: proc(system_data: rawptr),
	deinitialize_proc: proc(system_data: rawptr),

	initialized: bool,
	system_data: rawptr,
}

_system_manager_systems: [dynamic]^System
_system_manager_system_initialization_order: [dynamic]^System
_system_manager_system_datas: []u8
_system_manager_all_initialized := false

system_manager_register :: proc(system: ^System) {
	assert(!_system_manager_all_initialized)

	for other_system in _system_manager_systems {
		if other_system == system {
			log.panicf("Duplicate system {} registered", system)
		}
	}

	append(&_system_manager_systems, system)
}

// Gets size of allocation required for all systems.
// Note that order matters, as alignment is respected.
_system_manger_get_allocation_size :: proc() -> int {
	size := 0

	for system in _system_manager_systems {
		ti := type_info_of(system.system_data_type)
		size = mem.align_forward_int(size, ti.align)
		size += ti.size
	}

	return size
}

system_manager_init_all :: proc() {
	assert(!_system_manager_all_initialized)

	// Allocate contiguous data for systems

	// Make sure that this has a good alignment, as we care about the internal alignment as well
	_system_manager_system_datas, _ = mem.make_aligned([]u8, _system_manger_get_allocation_size(), 16)

	allocation_start := 0
	for system in _system_manager_systems {
		ti := type_info_of(system.system_data_type)
		allocation_start := mem.align_forward_int(allocation_start, ti.align)
		system.system_data = cast(rawptr)raw_data(_system_manager_system_datas[allocation_start:allocation_start+ti.size])
		allocation_start += ti.size
	}
	assert(allocation_start == len(_system_manager_system_datas))

	// Super stupid initializatino logic for now
	{
		for {
			did_initialize_any := false
			any_not_initialized := false

			for system in _system_manager_systems {
				if system.initialized do continue

				any_unsatisfied_dependency := false
				for system_dep in system.dependencies {
					if !system_dep.initialized {
						any_unsatisfied_dependency = true
						break
					}
				}

				if any_unsatisfied_dependency {
					any_not_initialized = true
				} else {
					log.infof("Initializing system '{}'", system.name)
					system.initialize_proc(system.system_data)
					system.initialized = true
					did_initialize_any = true
					append(&_system_manager_system_initialization_order, system)
				}
			}

			if !any_not_initialized {
				break // Done!
			}

			if !did_initialize_any {
				panic("Circular dependency!")
			}
		}
	}

	// System_Node :: struct {
	// 	system:         ^System,
	// 	// Can't use system.dependencies, as this only holds while initializing
	// 	dependencies:   []^System,
	// 	remaining_deps: int,
	// 	state:          enum {Deinitialized, Initializing, Initialized, Deinitializing},
	// }

	// system_nodes := make([dynamic]System_Node, len(_system_manager_systems))
	// // remaining_systems_count := len(_system_manager_systems)
	// for system, i in _system_manager_systems {
	// 	system_nodes[i] = System_Node {
	// 		system                 = system,
	// 		dependencies           = system.dependencies,
	// 		remaining_dependencies = len(system.dependencies),
	// 		state                  = .Deinitialized,
	// 	}
	// }

	// leaf_nodes, pending_nodes, internal_nodes: [dynamic]^System_Node

	// for node in &system_nodes {
	// 	append(&leaf_nodes if node.remaining_dependencies == 0 else &internal_nodes, &node)
	// }

	// for {
	// 	// Start initing leaf nodes
	// 	for node_idx := len(leaf_nodes) - 1; node_idx >= 0; node_idx -= 1 {
	// 		leaf_node := leaf_nodes[node_idx]
	// 		unordered_remove(&leaf_nodes, node_idx)
	// 		append(&pending_nodes, leaf_node)
	// 		append(&_system_manager_system_initialization_order, leaf_node)
	// 		// async
	// 		leaf_node.system.initialize_proc(leaf_node.system.system_data)
	// 		leaf_node.state = .Initialized
	// 		// /async
	// 	}

	// 	// Resolve completed nodes
	// 	for node_idx := len(pending_nodes) - 1; node_idx >= 0; node_idx -= 1 {
	// 		pending_node := pending_nodes[node_idx]
	// 		if pending_node.state == .Initialized {
	// 			unordered_remove(&pending_nodes, node_idx)
	// 			for other_node_idx := len(internal_nodes) - 1; other_node_idx >= 0; other_node_idx -= 1 {
	// 				internal_node := internal_nodes[other_node_idx]
	// 				for dependency in internal_node.dependencies {
	// 					if dependency == pending_node {
	// 						internal_node.remaining_deps -= 1
	// 						break
	// 					}
	// 				}

	// 				if internal_node.remaining_deps == 0 {
	// 					unordered_remove(&internal_nodes, other_node_idx)
	// 					append(&leaf_nodes, internal_node)
	// 				}
	// 			}
	// 		}
	// 	}

	// 	if len(pending_nodes) == 0 && len(leaf_nodes) == 0 {
	// 		log.panic("Circular dependency!")
	// 	}
	// }

	_system_manager_all_initialized = true
}

system_manager_deinit_all :: proc() {
	assert(_system_manager_all_initialized)

	// Deinitializing should be as simple as traversing the graph in the same way as during
	// initialization, but with the dependencies reversed. But for now we just deinitialize in
	// reverse order, which is unoptimal, but always correct.

	for system_idx := len(_system_manager_system_initialization_order) - 1; system_idx >= 0; system_idx -= 1 {
		system := _system_manager_system_initialization_order[system_idx]
		log.infof("Deinitializing system '{}'", system.name)
		system.deinitialize_proc(system)
		system.initialized = false
	}

	delete(_system_manager_system_initialization_order)

	_system_manager_all_initialized = false
}
