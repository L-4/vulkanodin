package nt2

import vk "vendor:vulkan"

// Note that a descriptorCount of zero within pool_sizes will be changed to a one
descriptor_pool_create :: proc(pool_sizes: []vk.DescriptorPoolSize, max_sets: int, flags := vk.DescriptorPoolCreateFlags {}) -> (descriptor_pool: vk.DescriptorPool) {
	for pool_size, idx in pool_sizes {
		if pool_size.descriptorCount == 0 {
			pool_sizes[idx].descriptorCount = 1
		}
	}

	// Poolsizes is an array of { type = uniform_buffer/sampler, count = int }
	// It should describe the allocation required for an entire descriptor set.

	// maxSets is the max number of sets assumed to be allocated from this pool at any given time.

	descriptor_pool_info := vk.DescriptorPoolCreateInfo {
		sType         = .DESCRIPTOR_POOL_CREATE_INFO,
		flags         = flags,
		maxSets       = cast(u32)max_sets,
		poolSizeCount = cast(u32)len(pool_sizes),
		pPoolSizes    = raw_data(pool_sizes),
	}

	vk_assert(vk.CreateDescriptorPool(vk_ctx.device, &descriptor_pool_info, nil, &descriptor_pool))

	return descriptor_pool
}

descriptor_set_allocate :: proc(descriptor_pool: vk.DescriptorPool,
								descriptor_set_layout: vk.DescriptorSetLayout,
								loc := #caller_location) -> vk.DescriptorSet {
	descriptor_set_layout := descriptor_set_layout

	descriptor_set: vk.DescriptorSet
	vk_assert(vk.AllocateDescriptorSets(vk_ctx.device, &vk.DescriptorSetAllocateInfo {
		sType              = .DESCRIPTOR_SET_ALLOCATE_INFO,
		descriptorPool     = descriptor_pool,
		descriptorSetCount = 1,
		pSetLayouts        = &descriptor_set_layout,
	}, &descriptor_set))

	vk_name_object(descriptor_set, "Unamed VkDescriptorSet created at {}", loc)

	return descriptor_set
}

Descriptor_Set_Layout_Info :: struct {
	type:        vk.DescriptorType,
	count:       int,
	stage_flags: vk.ShaderStageFlags,
	flags:       vk.DescriptorBindingFlags,
}

// TODO[TS]: We very often create poolSizes from this - could we make the return value
// here a bit heavier, such that we can generate poolSizes from it?
descriptor_set_layout_create :: proc(bindings: []Descriptor_Set_Layout_Info) -> vk.DescriptorSetLayout {
	layout_bindings      := make([]vk.DescriptorSetLayoutBinding, len(bindings), context.temp_allocator)
	layout_binding_flags := make([]vk.DescriptorBindingFlags, len(bindings), context.temp_allocator)

	for binding_idx in 0..<len(bindings) {
		binding_info := bindings[binding_idx]

		layout_bindings[binding_idx] = vk.DescriptorSetLayoutBinding {
			binding         = cast(u32)binding_idx,
			descriptorType  = binding_info.type,
			descriptorCount = cast(u32)binding_info.count,
			stageFlags      = binding_info.stage_flags,
		}

		layout_binding_flags[binding_idx] = binding_info.flags
	}

	layout_binding_flags_info := vk.DescriptorSetLayoutBindingFlagsCreateInfo {
		sType         = .DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO,
		bindingCount  = cast(u32)len(layout_binding_flags),
		pBindingFlags = raw_data(layout_binding_flags),
	}

	layout_info := vk.DescriptorSetLayoutCreateInfo {
		sType        = .DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		pNext        = &layout_binding_flags_info,
		bindingCount = cast(u32)len(layout_bindings),
		pBindings    = raw_data(layout_bindings),
	}

	descriptor_layout: vk.DescriptorSetLayout
	vk_assert(vk.CreateDescriptorSetLayout(vk_ctx.device, &layout_info, nil, &descriptor_layout))

	return descriptor_layout
}
