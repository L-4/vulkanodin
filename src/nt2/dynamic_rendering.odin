package nt2

import vk "vendor:vulkan"

// TODO[TS]: Instead of taking []color_attachments, depth_attachment,
// stencil_attachment, maybe have enum Attachment_Type, then always just pass
// []attachments?

Attachment_Rendering_Info :: struct {
	view:        vk.ImageView,
	load_op:     vk.AttachmentLoadOp,
	store_op:    vk.AttachmentStoreOp,
	clear_value: vk.ClearValue,
}

vk_begin_rendering :: proc(
	command_buffer: vk.CommandBuffer,
	region: vk.Rect2D,
	color_attachments: []Attachment_Rendering_Info,
	depth_attachment: Maybe(Attachment_Rendering_Info) = nil,
	stencil_attachment: Maybe(Attachment_Rendering_Info) = nil,
) {
	color_attachment_infos := make([]vk.RenderingAttachmentInfo, len(color_attachments), context.temp_allocator)

	for color_attchment, i in color_attachments {
		color_attachment_infos[i] = vk.RenderingAttachmentInfo {
			sType       = .RENDERING_ATTACHMENT_INFO,
			pNext       = nil,
			imageView   = color_attchment.view,
			imageLayout = .ATTACHMENT_OPTIMAL, // Why would it be anything else...
			resolveMode = vk.ResolveModeFlags_NONE,
			loadOp      = color_attchment.load_op,
			storeOp     = color_attchment.store_op,
			clearValue  = color_attchment.clear_value,
		}
	}

	depth_info, stencil_info: Attachment_Rendering_Info
	has_depth, has_stencil := false, false

	if actual_depth_info, ok := depth_attachment.?; ok {
		has_depth = true
		depth_info = actual_depth_info
	}

	if actual_stencil_info, ok := stencil_attachment.?; ok {
		has_stencil = true
		stencil_info = actual_stencil_info
	}

	depth_attachment_info := vk.RenderingAttachmentInfo {
		sType       = .RENDERING_ATTACHMENT_INFO,
		pNext       = nil,
		imageView   = depth_info.view,
		imageLayout = .ATTACHMENT_OPTIMAL, // Why would it be anything else...
		resolveMode = vk.ResolveModeFlags_NONE,
		loadOp      = depth_info.load_op,
		storeOp     = depth_info.store_op,
		clearValue  = depth_info.clear_value,
	}

	stencil_attachment_info := vk.RenderingAttachmentInfo {
		sType       = .RENDERING_ATTACHMENT_INFO,
		pNext       = nil,
		imageView   = stencil_info.view,
		imageLayout = .ATTACHMENT_OPTIMAL, // Why would it be anything else...
		resolveMode = vk.ResolveModeFlags_NONE,
		loadOp      = stencil_info.load_op,
		storeOp     = stencil_info.store_op,
		clearValue  = stencil_info.clear_value,
	}

	vk.CmdBeginRendering(command_buffer, &vk.RenderingInfo {
		sType                = .RENDERING_INFO,
		pNext                = nil,
		flags                = {},
		renderArea           = region,
		layerCount           = 1,
		viewMask             = 0,
		colorAttachmentCount = u32(len(color_attachment_infos)),
		pColorAttachments    = raw_data(color_attachment_infos),
		pDepthAttachment     = &depth_attachment_info if has_depth else nil,
		pStencilAttachment   = &stencil_attachment_info if has_stencil else nil,
	})
}
