package nt2

// TODO[TS]: Can we flush regions when using vk.mapmemory, to help debuggers?

import vk "vendor:vulkan"

import "core:log"
import "core:mem"

staging_buffer_data: [FIF_COUNT]Staging_Buffer_Frame_Info
DEFAULT_STAGING_BUFFER_SIZE :: vk.DeviceSize(16 * 1024)
STAGING_BUFFER_RESIZE_PRIMARY :: false

Staging_Buffer_Frame_Info :: struct {
	// Primary buffer represents "expected" data to be uploaded to the gpu.
	// If all goes well, this is the only one required.
	primary_buffer: vk.Buffer,
	primary_memory: vk.DeviceMemory,

	// Represents the total size of the buffer, and the amount taken so far.
	primary_buffer_size: vk.DeviceSize,
	primary_buffer_contents_size: vk.DeviceSize,

	// List of allocations made that didn't fit into the primary buffer.
	secondary_buffers: [dynamic]Secondary_Staging_Buffer,
}

Secondary_Staging_Buffer :: struct {
	buffer: vk.Buffer,
	memory: vk.DeviceMemory,
	size: vk.DeviceSize,
}

staging_buffer_initialize :: proc() {
	for i in 0..<FIF_COUNT {
		using frame_data := &staging_buffer_data[i]

		primary_buffer, primary_memory = buffer_create_single(DEFAULT_STAGING_BUFFER_SIZE, { .TRANSFER_SRC }, { .HOST_VISIBLE, .HOST_COHERENT })
		primary_buffer_size = DEFAULT_STAGING_BUFFER_SIZE
		primary_buffer_contents_size = 0

		secondary_buffers = make([dynamic]Secondary_Staging_Buffer)
	}
}

staging_buffer_deinitialize :: proc() {
	for i in 0..<FIF_COUNT {
		using frame_data := &staging_buffer_data[i]

		vk.DestroyBuffer(vk_ctx.device, primary_buffer, nil)
		vk.FreeMemory(vk_ctx.device, primary_memory, nil)

		for secondary_buffer in secondary_buffers {
			vk.DestroyBuffer(vk_ctx.device, secondary_buffer.buffer, nil)
			vk.FreeMemory(vk_ctx.device, secondary_buffer.memory, nil)
		}

		delete(secondary_buffers)
	}
}

// Call when new frame begins. Resets and staging buffer state from the previous
// time this frame index was used.
staging_buffer_begin_frame :: proc() {
	using frame_data := &staging_buffer_data[fif_get_current_index()]

	primary_buffer_contents_size = 0

	total_secondary_size := vk.DeviceSize(0)

	for secondary_buffer in secondary_buffers {
		vk.DestroyBuffer(vk_ctx.device, secondary_buffer.buffer, nil)
		vk.FreeMemory(vk_ctx.device, secondary_buffer.memory, nil)
		total_secondary_size += secondary_buffer.size
	}

	when STAGING_BUFFER_RESIZE_PRIMARY {
		log.panic("Not implemented")
	} else {
		if total_secondary_size > 0 {
			log.warnf("Secondary staging buffers had to be created. %d buffer(s), totalling %d bytes\n", len(secondary_buffers), total_secondary_size)
		}
	}

	clear(&secondary_buffers)
}

// Finds (or creates) a region of coherent memory of a given size which won't be used again this frame.
staging_buffer_get_space :: proc(size: vk.DeviceSize) -> (vk.Buffer, vk.DeviceMemory, vk.DeviceSize) {
	using frame_data := &staging_buffer_data[fif_get_current_index()]

	primary_buffer_capacity := primary_buffer_size - primary_buffer_contents_size

	if primary_buffer_capacity >= size {
		offset := primary_buffer_contents_size
		primary_buffer_contents_size += size

		return primary_buffer, primary_memory, offset
	} else {
		new_buffer, new_memory := buffer_create_single(size, { .TRANSFER_SRC }, { .HOST_VISIBLE, .HOST_COHERENT })

		append(&secondary_buffers, Secondary_Staging_Buffer {
			buffer = new_buffer,
			memory = new_memory,
			size = size,
		})

		return new_buffer, new_memory, 0
	}
}

// Uploads data to a staging buffer, and returns the buffer and region of the data.
staging_buffer_upload_data :: proc(data: []u8) -> (buffer: vk.Buffer, offset: vk.DeviceSize, size: vk.DeviceSize) {
	memory: vk.DeviceMemory
	size = cast(vk.DeviceSize)size_of_slice(data)
	buffer, memory, offset = staging_buffer_get_space(size)

	memory_ptr: rawptr
	vk.MapMemory(vk_ctx.device, memory, offset, size, { }, &memory_ptr)
	mem.copy(memory_ptr, raw_data(data), cast(int)size)
	vk.UnmapMemory(vk_ctx.device, memory)

	return buffer, offset, size
}

// TODO[TS]: This doesn't actually have to reserve the region - we know that the memory can be reused instantly
staging_buffer_copy_to_buffer_sync :: proc(
	buffer_data: []u8,
	target: vk.Buffer,
	offset: vk.DeviceSize = 0,
) {
	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	staging_buffer_copy_to_buffer(command_buffer, buffer_data, target, offset)
}

staging_buffer_copy_to_buffer :: proc(
	command_buffer: vk.CommandBuffer,
	buffer_data: []u8,
	target: vk.Buffer,
	offset: vk.DeviceSize = 0,
) {
	staging_buffer, staging_offset, size := staging_buffer_upload_data(buffer_data)

	vk.CmdCopyBuffer(command_buffer, staging_buffer, target, 1, &vk.BufferCopy {
		srcOffset = staging_offset,
		dstOffset = offset,
		size      = size,
	})
}

@(private="file")
_calculate_mip_extents :: proc(mip0_extent: vk.Extent3D, mip: u32) -> vk.Extent3D {
	return vk.Extent3D {
		width  = max(mip0_extent.width  >> mip, 0),
		height = max(mip0_extent.height >> mip, 0),
		depth  = max(mip0_extent.depth  >> mip, 0),
	}
}

// Given the data for a single slice of a single mip, uploads the data to the texture
staging_buffer_copy_to_image_single_mip_slice :: proc(
	command_buffer: vk.CommandBuffer,
	data: []u8,
	image: Image,
	mip_level, array_slice: u32,
) {
	staging_buffer, staging_offset, _ := staging_buffer_upload_data(data)

	vk.CmdCopyBufferToImage(command_buffer, staging_buffer, image.image, image.layout, 1, &vk.BufferImageCopy {
		bufferOffset = staging_offset,
		// If these are zero, then image is assumed to be tightly packed.
		bufferRowLength = 0, bufferImageHeight = 0,
		imageSubresource = vk.ImageSubresourceLayers {
			aspectMask     = {.COLOR}, // TODO-1
			mipLevel       = mip_level,
			baseArrayLayer = array_slice,
			layerCount     = 1,
		},
		imageOffset = { 0, 0, 0 },
		imageExtent = _calculate_mip_extents(image.extent, mip_level),
	})
}
