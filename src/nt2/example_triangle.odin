package nt2

import vk "vendor:vulkan"
import "core:math/linalg"

// This file exists as an example of how to render a triangle, as a test to find API pain points

Triangle_Transform :: struct {
	projection: matrix[4, 4]f32,
}

Triangle_Test_Data :: struct {
	triangle_image:      Image,

	set_layout:        vk.DescriptorSetLayout,
	triangle_pipeline: Pipeline,
	descriptor_pool:   vk.DescriptorPool,
	descriptor_set:    vk.DescriptorSet,
	projection_ubo:    Uniform_Buffer(Triangle_Transform),

	vbo_buffer: vk.Buffer,
	vbo_memory: vk.DeviceMemory,
}
triangle_test_data: Triangle_Test_Data

Triangle_Vertex :: struct {
	position: [2]f32,
	color:    [4]f32,
}

triangle_test_initialize :: proc(rt_descriptor: Render_Target_Descriptor) {
	triangle_test_data.triangle_image = image_create_from_bytes(2, 2, .R8G8B8A8_UNORM, {
		0xff, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff,
		0xff, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff,
	})

	vert_shader := shader_module_create_from_file("engine_assets/shaders/triangle.vert.spv")
	frag_shader := shader_module_create_from_file("engine_assets/shaders/triangle.frag.spv")
	defer vk.DestroyShaderModule(vk_ctx.device, vert_shader, nil)
	defer vk.DestroyShaderModule(vk_ctx.device, frag_shader, nil)

	module_infos := shader_stage_info_create_default(vert_shader, frag_shader)

	// TODO[TS]: Ideally here, we'd omit the descriptor set layout, and in fact have one global pipeline layout.
	// Descriptors can be bindless, and we could always have a maximum size push constant (128b) accessible
	// by all relevant pipeline stages.

	// TODO[TS]: Given that we for now have to create a set layout, pool, then finally set,
	// we should have the return type here include enough data such that we can automatically
	// allocate the pool immediately.
	// This is also in the TODO for the following proc.
	triangle_test_data.set_layout = descriptor_set_layout_create([]Descriptor_Set_Layout_Info {
		{ type = .UNIFORM_BUFFER, count = 1, stage_flags = {.VERTEX} },
		{ type = .SAMPLER, count = 1,        stage_flags = {.FRAGMENT} },
		{ type = .SAMPLED_IMAGE, count = 1,  stage_flags = {.FRAGMENT} },
	})

	triangle_test_data.triangle_pipeline = graphics_pipeline_create(
		module_infos[:],
		{triangle_test_data.set_layout},
		Triangle_Vertex,
		nil, 1,
		rt_descriptor)

	// TODO[TS]: It really sucks here that we have to create a dedicated descriptor pool.
	// But we also can't use one per frame, as our allocations are static. Something better should be done.
	triangle_test_data.descriptor_pool = descriptor_pool_create({
		{ type = .UNIFORM_BUFFER, descriptorCount = 1 },
		{ type = .SAMPLER,        descriptorCount = 1 },
		{ type = .SAMPLED_IMAGE,  descriptorCount = 1 },
	}, 1)
	triangle_test_data.descriptor_set = descriptor_set_allocate(triangle_test_data.descriptor_pool, triangle_test_data.set_layout)

	triangle_test_data.projection_ubo = uniform_buffer_create(Triangle_Transform)
	triangle_test_data.projection_ubo.data = Triangle_Transform {}
	buffer_info := uniform_buffer_get_descriptor(&triangle_test_data.projection_ubo)
	descriptor_writes := []vk.WriteDescriptorSet {
		{
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = triangle_test_data.descriptor_set,
			dstBinding      = 0,
			dstArrayElement = 0,
			descriptorCount = 1,
			descriptorType  = .UNIFORM_BUFFER,
			pBufferInfo = &buffer_info,
		},
		{
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = triangle_test_data.descriptor_set,
			dstBinding      = 1,
			dstArrayElement = 0,
			descriptorCount = 1,
			descriptorType  = .SAMPLER,
			pImageInfo = &vk.DescriptorImageInfo {
				sampler = sampler_default_linear,
			},
		},
		{
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = triangle_test_data.descriptor_set,
			dstBinding      = 2,
			dstArrayElement = 0,
			descriptorCount = 1,
			descriptorType  = .SAMPLED_IMAGE,
			pImageInfo = &vk.DescriptorImageInfo {
				imageView   = triangle_test_data.triangle_image.full_view,
				imageLayout = .SHADER_READ_ONLY_OPTIMAL,
			},
		},
	}
	vk.UpdateDescriptorSets(vk_ctx.device, cast(u32)len(descriptor_writes), raw_data(descriptor_writes), 0, nil)

	triangle_test_data.vbo_buffer, triangle_test_data.vbo_memory = buffer_create_static([]Triangle_Vertex {
		{ { 200, 0 }, { 1, 0, 0, 1 } },
		{ { 0, 400 }, { 0, 1, 0, 1 } },
		{ { 600, 400 }, { 0, 0, 1, 1 } },
	}, {.VERTEX_BUFFER})
}

triangle_test_deinitialize :: proc() {
	vk.DestroyBuffer(vk_ctx.device, triangle_test_data.vbo_buffer, nil)
	vk.FreeMemory(vk_ctx.device, triangle_test_data.vbo_memory, nil)

	uniform_buffer_destroy(&triangle_test_data.projection_ubo)

	vk.DestroyDescriptorPool(vk_ctx.device, triangle_test_data.descriptor_pool, nil)

	vk.DestroyPipelineLayout(vk_ctx.device, triangle_test_data.triangle_pipeline.pipeline_layout, nil)
	vk.DestroyPipeline(vk_ctx.device, triangle_test_data.triangle_pipeline.pipeline_handle, nil)

	vk.DestroyDescriptorSetLayout(vk_ctx.device, triangle_test_data.set_layout, nil)

	image_destroy(triangle_test_data.triangle_image)
}

triangle_test_render :: proc(command_buffer: vk.CommandBuffer) {
	vk.CmdBindPipeline(command_buffer, .GRAPHICS, triangle_test_data.triangle_pipeline.pipeline_handle)

	w, h := window_get_size()

	mat := &triangle_test_data.projection_ubo.data.(Triangle_Transform)
	mat.projection = linalg.matrix_ortho3d_f32(0, f32(w), f32(h), 0, -1, 1, false)
	uniform_buffer_upload(&triangle_test_data.projection_ubo)


	vk.CmdBindDescriptorSets(command_buffer, .GRAPHICS, triangle_test_data.triangle_pipeline.pipeline_layout, 0, 1, &triangle_test_data.descriptor_set, 0, nil)
	offset := vk.DeviceSize(0)
	vk.CmdBindVertexBuffers(command_buffer, 0, 1, &triangle_test_data.vbo_buffer, &offset)
	vk.CmdSetScissor(command_buffer, 0, 1, &vk.Rect2D { offset = {}, extent = { u32(w), u32(h) } } )
	vk.CmdSetViewport(command_buffer, 0, 1, &vk.Viewport { x = 0, y = 0, width = f32(w), height = f32(h), minDepth = 0, maxDepth = 1})
	vk.CmdDraw(command_buffer, 3, 1, 0, 0)
}
