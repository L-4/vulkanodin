package nt2

import vk "vendor:vulkan"

import "core:log"
import "core:runtime"
import "core:strings"

ignored_warnings := map[cstring]struct{} {
	// These two are ignored for now as allocations are not batched.
	"UNASSIGNED-BestPractices-vkAllocateMemory-small-allocation" = {},
	"UNASSIGNED-BestPractices-vkBindMemory-small-dedicated-allocation" = {},

	// Yells at you for using the validation layers.
	// https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/2509
	"UNASSIGNED-BestPractices-vkCreateInstance-specialuse-extension-debugging" = {},

	// This cries when you use VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT. But I can't figure out
	// what you're actually meant to do instead.
	"UNASSIGNED-BestPractices-pipeline-stage-flags" = {},

	// This warns about resetting individual command buffers vs the whole pool.
	// However this is called from the vulkan imgui impl, so we ignore it.
	"UNASSIGNED-BestPractices-vkCreateCommandPool-command-buffer-reset" = {},
}

num_warnings_ignored := 0

@(private)
debug_messenger_context: runtime.Context

@(private)
debug_messenger_callback :: proc "system" (
	message_severity: vk.DebugUtilsMessageSeverityFlagsEXT,
	message_type: vk.DebugUtilsMessageTypeFlagsEXT,
	callback_data: ^vk.DebugUtilsMessengerCallbackDataEXT,
	user_data: rawptr,
) -> b32 {

	context = debug_messenger_context

	if callback_data.pMessageIdName in ignored_warnings {
		num_warnings_ignored += 1
		return false
	}

	message := string(callback_data.pMessage)

	// QUICK[TS]: Remove if not reproducible for a while
	when false {
		// We get an error about a missing json, with no good message id to filter it out.
		SOCIAL_CLUB_ERROR_END :: "SocialClubVulkanLayer.json"
		if len(message) > len(SOCIAL_CLUB_ERROR_END) &&
			message[len(message) - len(SOCIAL_CLUB_ERROR_END):] == SOCIAL_CLUB_ERROR_END {

			num_warnings_ignored += 1
			return false
		}
	}

	ss := strings.split(message, "|", context.temp_allocator)

	log_proc := log.infof

	if      message_severity == { .VERBOSE } do log_proc = log.debugf
	else if message_severity == { .INFO    } do log_proc = log.infof
	else if message_severity == { .WARNING } do log_proc = log.warnf
	else if message_severity == { .ERROR   } do log_proc = log.errorf
	else do log.panicf("Unhandled message_severity '%v'\n", message_severity)

	if num_warnings_ignored > 0 {
		log.infof("Ignored %d warnings", num_warnings_ignored)
		num_warnings_ignored = 0
	}

	log_proc("VK Validation: %s", strings.trim_space(ss[len(ss) - 1]))
	when true {
		log_proc("Message ID: %s", callback_data.pMessageIdName)
	}

	when true {
		trap()
	}

	return false
}

// Used for the debug messenger as well as on instance creation
global_debug_messenger_info := vk.DebugUtilsMessengerCreateInfoEXT {
	sType = .DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
	messageSeverity = { /*.VERBOSE, .INFO, */.WARNING, .ERROR},
	messageType = {.GENERAL, .VALIDATION, .PERFORMANCE},
	pfnUserCallback = debug_messenger_callback,
}

set_debug_messenger_context :: proc() {
	debug_messenger_context = context
}

debug_messenger_initialize :: proc() {
	vk.CreateDebugUtilsMessengerEXT(vk_ctx.instance, &global_debug_messenger_info, nil, &vk_ctx.debug_messenger)
}

debug_messenger_deinitialize :: proc() {
	vk.DestroyDebugUtilsMessengerEXT(vk_ctx.instance, vk_ctx.debug_messenger, nil)
}
