package nt2

import vk "vendor:vulkan"

import "core:mem"
import "core:math/linalg"
import "core:slice"
import "core:log"

V3 :: types.V3
Q  :: types.Q

// @todo: Long term: sometimes we allocate transforms which don't need to live on the gpu,
// maybe as a root for a bunch of meshes. Could these be separated out?

// @todo: Change invalid transform index from -1 to u32(max), and change the type of this
Transform_Index :: distinct i32
DEFAULT_TRANSFORM :: Transform { translation = V3 { 0, 0, 0 }, scale = 1, rotation = Q(1) }

Transform :: struct {
	translation: V3,
	rotation:    Q,
	scale:       f32,
}

// @todo: Better name
Complete_Transform :: struct {
	world, local: Transform,
	dirty, allocated: bool,
	matrix_dirty: bool,
	parent: Transform_Index,
}

Node :: struct {
	index: Transform_Index,
	children: []Node,
}

transform_system: struct {
	transforms:               []Complete_Transform,
	matrices:                 []M4,
	num_allocated_transforms: int,

	tree_roots:           []Node,
	tree_arena_data:      []byte,
	tree_arena:           mem.Arena,
	tree_allocator:       mem.Allocator,
	tree_needs_rebuild:   bool,

	// GPU stuff
	gpu_buffer_set_layout:      vk.DescriptorSetLayout,
	gpu_buffer_descriptor_pool: vk.DescriptorPool, // @todo: This should not be here.
	gpu_buffers_memory:         vk.DeviceMemory,

	gpu_buffers: [MAX_FRAMES_IN_FLIGHT]struct {
		min_dirty, max_dirty: Transform_Index,
		buffer:               vk.Buffer,
		descriptor_set:       vk.DescriptorSet,
	},
}

transform_system_initialize :: proc(size := 256) {
	using transform_system

	// @todo: Use one big allocation for these
	transforms = make([]Complete_Transform, size)
	matrices = make([]M4, size)

	arena_size := size_of(Node) * size + size_of(^Node) * 4
	tree_arena_data = make([]byte, arena_size)
	mem.init_arena(&transform_system.tree_arena, tree_arena_data)
	tree_allocator = mem.arena_allocator(&transform_system.tree_arena)

	// GPU stuff
	fif_count := FIF_COUNT

	gpu_buffer_set_layout = descriptor_set_layout_create({
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .VERTEX } },
	})

	gpu_buffer_descriptor_pool = descriptor_pool_create({
		vk.DescriptorPoolSize { type = .STORAGE_BUFFER, descriptorCount = 1 },
	}, fif_count)

	gpu_buffers_static: [MAX_FRAMES_IN_FLIGHT]vk.Buffer
	gpu_buffers_slice := gpu_buffers_static[:fif_count]

	gpu_buffer_size := size_of(M4) * size
	gpu_buffers_memory = buffer_create_multiple(
		cast(vk.DeviceSize)gpu_buffer_size,
		{ .STORAGE_BUFFER, .TRANSFER_DST },
		{ .DEVICE_LOCAL },
		gpu_buffers_slice)

	valid_gpu_buffers := gpu_buffers[:fif_count]

	for gpu_buffer_data, frame_idx in &valid_gpu_buffers {
		gpu_buffer_data.buffer = gpu_buffers_slice[frame_idx]
		gpu_buffer_data.min_dirty = -1
		gpu_buffer_data.max_dirty = -1
		gpu_buffer_data.descriptor_set = descriptor_sets_allocate_single(
			gpu_buffer_descriptor_pool,
			gpu_buffer_set_layout)

		descriptor_write := vk.WriteDescriptorSet {
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = gpu_buffer_data.descriptor_set,
			dstBinding      = 0,
			dstArrayElement = 0,
			descriptorType  = .STORAGE_BUFFER,
			descriptorCount = 1,
			pBufferInfo     = &vk.DescriptorBufferInfo {
				buffer = gpu_buffer_data.buffer,
				offset = 0,
				range = cast(vk.DeviceSize)gpu_buffer_size,
			},
		}

		vk.UpdateDescriptorSets(device, 1, &descriptor_write, 0, nil)
	}
}

transform_system_deinitialize :: proc() {
	using transform_system

	delete(transforms)
	delete(matrices)
	delete(tree_arena_data)

	// GPU stuff
	for gpu_buffer_data in &gpu_buffers {
		vk.DestroyBuffer(device, gpu_buffer_data.buffer, nil)
	}

	vk.FreeMemory(device, gpu_buffers_memory, nil)
	vk.DestroyDescriptorSetLayout(device, gpu_buffer_set_layout, nil)
	vk.DestroyDescriptorPool(device, gpu_buffer_descriptor_pool, nil)
}

@(private="file")
transform_system_find_free_index :: proc() -> Transform_Index {
	using transform_system

	for transform, transform_index in transforms {
		if !transform.allocated {
			return Transform_Index(transform_index)
		}
	}

	return Transform_Index(-1)
}

transform_system_allocate_transform :: proc() -> Transform_Index {
	using transform_system

	transform_index := transform_system_find_free_index()

	transform := &transforms[transform_index]
	transform.allocated = true
	transform.dirty = true

	transform.world = DEFAULT_TRANSFORM
	transform.local = DEFAULT_TRANSFORM
	transform.parent = Transform_Index(-1)

	num_allocated_transforms += 1
	// @todo: We can probably avoid this, it should not really have any effect on the tree.
	tree_needs_rebuild = true

	return transform_index
}

transform_system_release_transform :: proc(index: Transform_Index) {
	using transform_system

	// @todo: Make sure that we aren't a parent

	transform := &transforms[index]
	transform.allocated = false

	num_allocated_transforms -= 1
}

@(private="file")
transform_system_is_heirarchy_circular :: proc(start: Transform_Index) -> bool {
	using transform_system

	transform := transforms[start]

	for transform.parent != Transform_Index(-1) {
		if transform.parent == start {
			return true
		}

		transform = transforms[transform.parent]
	}

	return false
}

// Returns success
transform_system_parent_transform :: proc(parent, child: Transform_Index) -> bool {
	using transform_system

	child_transform := &transforms[child]
	old_parent := child_transform.parent

	if child_transform.parent == parent { return true }

	child_transform.parent = parent

	if transform_system_is_heirarchy_circular(child) {
		child_transform.parent = old_parent
		log.warn("Attempted to make circular transform hierarchy!")

		return false
	}

	tree_needs_rebuild = true

	return true
}

@(private="file")
transform_system_rebuild_tree :: proc() {
	using transform_system

	// @todo: This whole procedure is O(n^2)

	Transform_With_Index :: struct {
		index: Transform_Index,
		using transform: Complete_Transform,
	}

	allocated_transforms := make([]Transform_With_Index, num_allocated_transforms)
	defer delete(allocated_transforms)
	{
		idx := 0
		for transform, transform_idx in transforms {
			if transform.allocated {
				allocated_transforms[idx] = Transform_With_Index {
					index     = Transform_Index(transform_idx),
					transform = transform,
				}

				idx += 1
				if idx == num_allocated_transforms { break }
			}
		}
	}

	collect_children :: proc(allocated_transforms: []Transform_With_Index, temp_nodes: ^[dynamic]Node, parent_index: Transform_Index) {
		temp_nodes := temp_nodes
		clear(temp_nodes)

		for transform in allocated_transforms {
			if transform.parent == parent_index {
				append(temp_nodes, Node { index = transform.index })
			}
		}
	}

	temp_nodes := make([dynamic]Node)
	defer delete(temp_nodes)

	{
		context.allocator = tree_allocator
		free_all()

		collect_children(allocated_transforms, &temp_nodes, Transform_Index(-1))
		tree_roots = slice.clone(temp_nodes[:])

		collect_children_2 :: proc(parent_node: ^Node, allocated_transforms: []Transform_With_Index, temp_nodes: ^[dynamic]Node) {
			collect_children(allocated_transforms, temp_nodes, parent_node.index)
			parent_node.children = slice.clone(temp_nodes[:])

			for node in &parent_node.children {
				collect_children_2(&node, allocated_transforms, temp_nodes)
			}
		}

		for root_node in &tree_roots {
			collect_children_2(&root_node, allocated_transforms, &temp_nodes)
		}
	}

	tree_needs_rebuild = false
}

@(private="file")
transform_system_resolve_transforms :: proc() {
	using transform_system

	// @todo: transform_system.any_transform_dirty early out

	for node in tree_roots {
		transform := &transforms[node.index]

		if transform.dirty {
			transform.world = transform.local
			transform.dirty = false
			transform.matrix_dirty = true

			for child_node in node.children {
				transform_system_definitely_dirty(transform.world, child_node)
			}
		} else {
			for child_node in node.children {
				transform_system_maybe_dirty(transform.world, child_node)
			}
		}
	}
}

@(private="file")
transform_system_maybe_dirty :: proc(world_transform: Transform, node: Node) {
	using transform_system

	node_transform := &transforms[node.index]

	if node_transform.dirty {
		transform_system_definitely_dirty(world_transform, node)
	} else {
		for child_node in node.children {
			transform_system_maybe_dirty(node_transform.world, child_node)
		}
	}
}

@(private="file")
transform_system_definitely_dirty :: proc(world_transform: Transform, node: Node) {
	using transform_system

	node_transform := &transforms[node.index]

	node_transform.world = transform_system_calculate_transform(world_transform, node_transform.local)
	node_transform.dirty = false
	node_transform.matrix_dirty = true

	// @todo: Write min/max dirty index?

	for child_node in node.children {
		transform_system_definitely_dirty(node_transform.world, child_node)
	}
}

transform_system_calculate_transform :: proc(world, local: Transform) -> Transform {
	new_transform := Transform {}

	new_transform.scale       = world.scale * local.scale
	new_transform.rotation    = linalg.quaternion_mul_quaternion(world.rotation, local.rotation)
	new_transform.translation = linalg.quaternion_mul_vector3(world.rotation, local.translation) * world.scale + world.translation

	return new_transform
}

@(private="file")
transform_system_write_dirty_min_max :: proc(min_dirty, max_dirty: Transform_Index) {
	using transform_system
	// @todo: Feels like this could be cleaned up

	for frame_idx in 0..<FIF_COUNT {
		gpu_buffer_data := &gpu_buffers[frame_idx]

		if gpu_buffer_data.min_dirty == -1 {
			gpu_buffer_data.min_dirty = min_dirty
		} else {
			gpu_buffer_data.min_dirty = min(gpu_buffer_data.min_dirty, min_dirty)
		}

		if gpu_buffer_data.max_dirty == -1 {
			gpu_buffer_data.max_dirty = max_dirty
		} else {
			gpu_buffer_data.max_dirty = max(gpu_buffer_data.max_dirty, max_dirty)
		}
	}
}

@(private="file")
transform_system_write_matrices :: proc() {
	using transform_system

	first_dirty := Transform_Index(-1)
	last_dirty := Transform_Index(-1)

	for transform, transform_index in transforms {
		if transform.matrix_dirty {
			if first_dirty == -1 { first_dirty = Transform_Index(transform_index) }
			last_dirty = Transform_Index(transform_index)

			s := transform.world.scale

			// @todo: Scale is a float, we can optimize this procedure.
			matrices[transform_index] = linalg.matrix4_from_trs(
				transform.world.translation,
				transform.world.rotation,
				V3 { s, s, s })

			transforms[transform_index].matrix_dirty = false
		}
	}

	if first_dirty != -1 {
		transform_system_write_dirty_min_max(first_dirty, last_dirty)
	}
}

@(private="file")
transform_system_upload_gpu_data :: proc() {
	using transform_system

	gpu_buffer_data := &gpu_buffers[fif_get_current_index()]

	if gpu_buffer_data.min_dirty == -1 do return

	staging_buffer_copy_to_buffer_sync(
		matrices[gpu_buffer_data.min_dirty:gpu_buffer_data.max_dirty+1],
		gpu_buffer_data.buffer,
		cast(vk.DeviceSize)(gpu_buffer_data.min_dirty * size_of(M4)))

	gpu_buffer_data.min_dirty = -1
	gpu_buffer_data.max_dirty = -1
}

transform_system_flush_data :: proc() {
	using transform_system

	if tree_needs_rebuild { transform_system_rebuild_tree() }
	transform_system_resolve_transforms()
	transform_system_write_matrices()
	transform_system_upload_gpu_data()
}

transform_system_bind_descriptor_set :: proc(command_buffer: vk.CommandBuffer, pipeline_layout: vk.PipelineLayout) {
	using transform_system

	gpu_buffer_data := gpu_buffers[fif_get_current_index()]

	vk.CmdBindDescriptorSets(command_buffer, .GRAPHICS, pipeline_layout, 1, 1, &gpu_buffer_data.descriptor_set, 0, nil)
}

transform_system_set_transform :: proc(transform_index: Transform_Index, new_transform: Transform) {
	using transform_system

	transform := &transforms[transform_index]
	assert(transform.allocated)

	transform.local = new_transform
	transform.dirty = true
}
