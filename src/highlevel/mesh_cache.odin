package nt2
/*
import "core:log"

Mesh_Data :: struct {
	mesh_indices: []Mesh_Index,
}

// Resource containing refcount, backing data, and info to fetch from cache.
Mesh_Resource :: struct {
	// Ref count and cache info should maybe be using'd?
	ref_count: int,
	using data: Mesh_Data,
	cache_info: Mesh_Cache_Info, // Maybe should be pImpl to any cache
}

Mesh_Ref :: struct {
	using ref: ^Mesh_Resource,
}

// Data required for default loader
Mesh_File_Info :: struct {
	path: string,
}

// Union of all structs which describe cache info for a mesh
Mesh_Cache_Info :: union {
	Mesh_File_Info,
}

Mesh_Cache :: struct {
	cache: map[Mesh_Cache_Info]^Mesh_Resource,
}

Mesh_Cache_Error :: union {
	Cache_Error,
}

mesh_cache_initialize :: proc() {
	caches.mesh_cache.cache = make(map[Mesh_Cache_Info]^Mesh_Resource)
}

mesh_cache_deinitialize :: proc() {
	for cache_key, resource in caches.mesh_cache.cache {
		if resource.ref_count > 0 {
			log.warnf("Resource %v still had '%d' references when the cache was destroyed!", cache_key, resource.ref_count)
		}
	}

	delete(caches.mesh_cache.cache)
}

mesh_cache_get :: proc(info: Mesh_Cache_Info) -> (ref: Mesh_Ref, err: Mesh_Cache_Error) {
	using caches.mesh_cache

	if res, ok := cache[info]; ok {
		ref = mesh_cache_ref(res)

		log.debugf("Loading cached resource %v", info)

		return
	}

	#partial switch info_specfic in info {
		case Mesh_File_Info: {

			raw_meshes := raw_mesh_from_gltf(info_specfic.path)
			defer delete(raw_meshes)
			defer for raw_mesh in raw_meshes {
				raw_mesh_free(raw_mesh)
			}

			mesh_indices := make([]Mesh_Index, len(raw_meshes))
			for raw_mesh, raw_mesh_idx in raw_meshes {
				mesh_indices[raw_mesh_idx] = mesh_vertex_data_allocate(raw_mesh)
			}

			ref = mesh_cache_insert(info, Mesh_Data { mesh_indices = mesh_indices })

			return
		}

		case: {
			err = .Cannot_Load_With_Info_Type

			return
		}
	}
}

mesh_cache_insert :: proc(info: Mesh_Cache_Info, mesh: Mesh_Data) -> Mesh_Ref {
	using caches.mesh_cache

	assert(!(info in cache))

	log.debugf("Inserting new resource into cache %v", info)

	new_res := new_clone(Mesh_Resource {
		ref_count = 1,
		data = mesh,
		cache_info = info,
	})

	cache[info] = new_res
	return Mesh_Ref { ref = new_res }
}

// @todo: This should use Mesh_Ref
// @todo: Procedure group ref/unref
mesh_cache_ref :: #force_inline proc(res: ^Mesh_Resource) -> Mesh_Ref {
	res.ref_count += 1
	log.debugf("References++ -> %d refs: %v", res.ref_count, res.cache_info)
	return Mesh_Ref { ref = res }
}

mesh_cache_unref :: proc(ref: Mesh_Ref) {
	using caches.mesh_cache

	ref.ref_count -= 1

	log.debugf("References-- -> %d refs: %v", ref.ref_count, ref.cache_info)

	if ref.ref_count == 0 {
		log.debugf("Removed resource from cache %v", ref.cache_info)
		for mesh_index in ref.mesh_indices {
			mesh_vertex_data_free(mesh_index)
		}
		delete(ref.mesh_indices)
		delete_key(&cache, ref.cache_info)
		free(ref.ref)
	}
}
*/
