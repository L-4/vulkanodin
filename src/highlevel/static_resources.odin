package nt2

Static_Resources :: struct {
	checkerboard_image: Image_Ref,
	default_normal_image: Image_Ref,
	black_image: Image_Ref,
	white_image: Image_Ref,
	grey_image: Image_Ref,
}

@(private)
static_resources: ^Static_Resources

static_resources_initialize :: proc() {
	assert(static_resources == nil)
	static_resources = new(Static_Resources)

	// Checkerboard
	checkerboard_image := image_create_from_bytes(2, 2, .R8G8B8A8_UNORM, []u8 {
		0,   0,   0,   255, 255, 0,   255, 255,
		255, 0,   255, 255, 0,   0,   0,   255,
	})

	vk_name_object(checkerboard_image.image, "Debug checkerboard image")
	vk_name_object(checkerboard_image.memory, "Debug checkerboard image memory")

	checkerboard_image_index := Image_Data { image_index = object_data_buffer_insert_texture(checkerboard_image, .R8G8B8A8_UNORM) }
	static_resources.checkerboard_image = image_cache_insert(Image_Inline_Info {name = "Checkerboard image"}, checkerboard_image_index)

	// Default normal
	default_normal_image := image_create_from_bytes(1, 1, .R8G8B8A8_UNORM, []u8 {
		127, 127, 255, 255,
	})

	vk_name_object(default_normal_image.image, "Default normal image")
	vk_name_object(default_normal_image.memory, "Default normal image memory")

	default_normal_image_index := Image_Data { image_index = object_data_buffer_insert_texture(default_normal_image, .R8G8B8A8_UNORM) }
	static_resources.default_normal_image = image_cache_insert(Image_Inline_Info {name = "Default normal"}, default_normal_image_index)

	// Fully black image
	black_image := image_create_from_bytes(1, 1, .R8G8B8A8_UNORM, []u8 {
		0, 0, 0, 255,
	})

	vk_name_object(black_image.image, "Fully black image")
	vk_name_object(black_image.memory, "Fully black image memory")

	black_image_index := Image_Data { image_index = object_data_buffer_insert_texture(black_image, .R8G8B8A8_UNORM) }
	static_resources.black_image = image_cache_insert(Image_Inline_Info {name = "Fully black image"}, black_image_index)

	// Fully white image
	white_image := image_create_from_bytes(1, 1, .R8G8B8A8_UNORM, []u8 {
		255, 255, 255, 255,
	})

	vk_name_object(white_image.image, "Fully white image")
	vk_name_object(white_image.memory, "Fully white image memory")

	white_image_index := Image_Data { image_index = object_data_buffer_insert_texture(white_image, .R8G8B8A8_UNORM) }
	static_resources.white_image = image_cache_insert(Image_Inline_Info {name = "Fully white image"}, white_image_index)

	// Fully grey image
	grey_image := image_create_from_bytes(1, 1, .R8G8B8A8_UNORM, []u8 {
		127, 127, 127, 255,
	})

	vk_name_object(grey_image.image, "Fully grey image")
	vk_name_object(grey_image.memory, "Fully grey image memory")

	grey_image_index := Image_Data { image_index = object_data_buffer_insert_texture(grey_image, .R8G8B8A8_UNORM) }
	static_resources.grey_image = image_cache_insert(Image_Inline_Info {name = "Fully grey image"}, grey_image_index)
}

static_resources_deinitialize :: proc () {
	assert(static_resources != nil)

	image_cache_unref(static_resources.checkerboard_image)
	image_cache_unref(static_resources.default_normal_image)
	image_cache_unref(static_resources.black_image)
	image_cache_unref(static_resources.white_image)
	image_cache_unref(static_resources.grey_image)

	free(static_resources)
	static_resources = nil
}

static_resources_get_checkerboard_image :: #force_inline proc() -> Image_Ref {
	return image_cache_ref(static_resources.checkerboard_image)
}

static_resources_get_default_normal_image :: #force_inline proc() -> Image_Ref {
	return image_cache_ref(static_resources.default_normal_image)
}
