package nt2

import vk "vendor:vulkan"

import "core:log"
import "core:path/filepath"

MAX_OBJECTS :: 1024
TEXTURES_PER_OBJECT :: 2
MAX_TEXTURE_DESCRIPTORS :: MAX_OBJECTS * TEXTURES_PER_OBJECT

Texture_Index   :: distinct i32

Object_Data_Buffer :: struct {
	// Texture array
	texture_array_descriptor_set_layout: vk.DescriptorSetLayout,
	texture_array_descriptor_pool:       vk.DescriptorPool,
	texture_array_descriptor_set:        vk.DescriptorSet,

	textures: #soa[MAX_TEXTURE_DESCRIPTORS]struct {
		allocated: bool,
		image: Image,
		image_view: vk.ImageView,
		sampler: vk.Sampler,
	},
}

object_data_buffer: ^Object_Data_Buffer

object_data_buffer_initialize :: proc() {
	if object_data_buffer != nil {
		log.panic("Object data buffer initialized twice!")
	}

	object_data_buffer = new(Object_Data_Buffer)

	// Texture array descriptor pool.
	// @todo: We say here that this descriptor set can be partially bound, but it might
	// make more sense to fill the entire array with dummy textures.
	// This could also help with dealing with when to write to the descriptor set,
	// as at all times a valid texture handle exists at every index.
	object_data_buffer.texture_array_descriptor_set_layout = descriptor_set_layout_create(
		[]Descriptor_Set_Layout_Info {
			{ type = .COMBINED_IMAGE_SAMPLER, count = MAX_TEXTURE_DESCRIPTORS, stage_flags = { .FRAGMENT }, flags = { .PARTIALLY_BOUND } },
		})

	texture_array_pool_sizes := []vk.DescriptorPoolSize {
		vk.DescriptorPoolSize { type = .COMBINED_IMAGE_SAMPLER, descriptorCount = MAX_TEXTURE_DESCRIPTORS },
	}

	object_data_buffer.texture_array_descriptor_pool = descriptor_pool_create(texture_array_pool_sizes, 1)
	object_data_buffer.texture_array_descriptor_set = descriptor_sets_allocate_single(
		object_data_buffer.texture_array_descriptor_pool,
		object_data_buffer.texture_array_descriptor_set_layout)

	vk_name_object(object_data_buffer.texture_array_descriptor_set, "Texture array")
}

// @todo: Move
expand_range :: proc(old_min, old_max, new_index: $T) -> (new_min, new_max: T) {
	if old_min == -1 {
		new_min = new_index
		new_max = new_index
	} else {
		if new_index < old_min {
			new_min = new_index
		} else if new_index > old_max {
			new_max = new_index
		}
	}

	return
}

@(private="file")
object_data_buffer_get_free_index :: proc() -> Texture_Index {
	for i in 0..<MAX_TEXTURE_DESCRIPTORS {
		if !object_data_buffer.textures[i].allocated {
			return Texture_Index(i)
		}
	}

	return Texture_Index(-1)
}

@(private="file")
object_data_buffer_write_texture :: proc(texture_index: Texture_Index, image: Image, image_view: vk.ImageView, sampler: vk.Sampler) {
	textures := &object_data_buffer.textures

	assert(!textures[texture_index].allocated)

	textures[texture_index].image = image
	textures[texture_index].image_view = image_view
	textures[texture_index].sampler = sampler
	textures[texture_index].allocated = true

	texture_descriptor_write := vk.WriteDescriptorSet {
		sType           = .WRITE_DESCRIPTOR_SET,
		dstSet          = object_data_buffer.texture_array_descriptor_set,
		dstBinding      = 0,
		dstArrayElement = cast(u32)texture_index,
		descriptorType  = .COMBINED_IMAGE_SAMPLER,
		descriptorCount = 1,
		pImageInfo = &vk.DescriptorImageInfo {
			sampler     = textures[texture_index].sampler,
			imageView   = textures[texture_index].image_view,
			imageLayout = .SHADER_READ_ONLY_OPTIMAL,
		},
	}

	// @todo: Look into when we can do this! With descriptor indexing, we can apparently
	// do this whenever, but there should be no reason to update during the frame.
	vk.UpdateDescriptorSets(device, 1, &texture_descriptor_write, 0, nil)
}

object_data_buffer_allocate_texture :: proc(file_name: string) -> Texture_Index {
	texture_index := object_data_buffer_get_free_index()

	if texture_index == -1 {
		return -1
	}

	image: Image

	switch filepath.ext(file_name) {
	case ".png": {
		_image, err := png_load_from_file(file_name)

		if err != nil {
			return -1
		}

		image = _image
	}
	case ".dds": {
		_image, err := dds_load_from_file(file_name)

		if err != nil {
			return -1
		}

		image = _image
	}
	}

	object_data_buffer_write_texture(
		texture_index,
		image,
		image_create_view(image, {.COLOR}),
		sampler_default_linear)

	return texture_index
}

// Insert previously created image into array. Object data buffer takes ownership,
// and is responsible for destroying the iamge as well.
object_data_buffer_insert_texture :: proc(image: Image, format: vk.Format) -> Texture_Index {
	texture_index := object_data_buffer_get_free_index()

	if texture_index == -1 {
		return -1
	}

	object_data_buffer_write_texture(
		texture_index,
		image,
		image_create_view(image, {.COLOR}),
		sampler_default_linear)

	return texture_index
}

object_data_buffer_release_texture :: proc(texture_index: Texture_Index) {
	textures := &object_data_buffer.textures

	assert(textures[texture_index].allocated)

	textures[texture_index].allocated = false

	image_destroy(textures[texture_index].image)
	vk.DestroyImageView(device, textures[texture_index].image_view, nil)
}

object_data_buffer_deinitialize :: proc() {
	if object_data_buffer == nil {
		log.panic("Object data buffer not initialized!")
	}

	vk.DestroyDescriptorPool(device, object_data_buffer.texture_array_descriptor_pool, nil)
	vk.DestroyDescriptorSetLayout(device, object_data_buffer.texture_array_descriptor_set_layout, nil)

	free(object_data_buffer)
	object_data_buffer = nil
}

object_data_buffer_bind_descriptor_set :: proc(command_buffer: vk.CommandBuffer, pipeline_layout: vk.PipelineLayout) {
	sets := []vk.DescriptorSet { object_data_buffer.texture_array_descriptor_set }
	vk.CmdBindDescriptorSets(command_buffer, .GRAPHICS, pipeline_layout, 2, cast(u32)len(sets), raw_data(sets), 0, nil)
}
