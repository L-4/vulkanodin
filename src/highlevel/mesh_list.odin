package nt2
/*
import vk "vendor:vulkan"

import "core:log"

// MESH INSTANCE
Mesh_Instance_Flags :: bit_set[Mesh_Instance_Flag;u32]
Mesh_Instance_Flag :: enum u32 {
	Shadow_Caster,
	Transparent,
}

Mesh_Instance_Index :: distinct i32

Mesh_Instance_Handle :: struct {
	// There might be multiple, as a single mesh may have sub meshes.
	// They need to operate on shared data some times though, so the underlying
	// data should probably not be touched directly.
	instances: []Mesh_Instance_Index,
}

Mesh_Instance :: struct {
	flags:           Mesh_Instance_Flags,
	transform_index: Transform_Index,
	// @todo: Here we hold on to a mesh ref (which may contain multiple mesh indices)
	// for as long at this instance is around.
	// Really, this should be owned by the container which creates and destroys all
	// of the mesh_instances created from a file.
	mesh_ref:        Mesh_Ref,
	mesh:            Mesh_Index,
	diffuse_texture: Image_Ref,
	normal_texture:  Image_Ref,
	// Sphere, aabb?

	allocated:       bool,
}

// Version of above struct to be uploaded to GPU
// @todo: Pull draw ranges from somewhere else
Gpu_Mesh_Instance :: struct {
	flags:           Mesh_Instance_Flags, // uint
	index_count:     u32,                 // uint
	first_index:     u32,                 // uint
	first_vertex:    i32,                 // int
	transform_index: Transform_Index,     // int
	diffuse_texture: Texture_Index,       // int
	normal_texture:  Texture_Index,       // int

	allocated:       b32,                 // bool (glsl bools are 32 bit)
}

// GPU Only struct containing data written to by collection shaders
Gpu_Mesh_Render_Data :: struct {
	transform_index: Transform_Index,     // int
	diffuse_texture: Texture_Index,       // int
	normal_texture:  Texture_Index,       // int
}

mesh_instance_create :: proc(mesh_list: ^Mesh_List, file_name: string) -> Mesh_Instance_Handle {
	meshes, _ := mesh_cache_get(Mesh_File_Info { path = file_name })

	test_diffuse_texture, _ := image_cache_get(Image_File_Info { path = "assets/helmet/Default_albedo.dds" })
	test_normal_texture, _ := image_cache_get(Image_File_Info { path = "assets/helmet/Default_normal.png" })

	transform_index := transform_system_allocate_transform()

	mesh_instance := Mesh_Instance_Handle {
		instances = make([]Mesh_Instance_Index, len(meshes.mesh_indices)),
	}

	for mesh, mesh_idx in meshes.mesh_indices {
		instance_index := mesh_list_get_instance_index(mesh_list)
		mesh_list_mark_dirty(mesh_list, instance_index)
		mesh_list.instances[instance_index] = Mesh_Instance {
			flags = {},
			transform_index = transform_index,
			mesh_ref = mesh_cache_ref(meshes),
			mesh = mesh,
			diffuse_texture = test_diffuse_texture,
			normal_texture = test_normal_texture,
			allocated = true,
		}
		mesh_instance.instances[mesh_idx] = instance_index
	}

	mesh_cache_unref(meshes)

	return mesh_instance
}

mesh_instance_destroy :: proc(mesh_list: ^Mesh_List, mesh_instance: Mesh_Instance_Handle) {
	for instance_idx in mesh_instance.instances {
		instance := &mesh_list.instances[instance_idx]

		instance.allocated = false

		image_cache_unref(instance.diffuse_texture)
		image_cache_unref(instance.normal_texture)

		transform_system_release_transform(instance.transform_index)
		mesh_cache_unref(instance.mesh_ref)

		mesh_list_mark_dirty(mesh_list, instance_idx)
	}

	delete(mesh_instance.instances)
}

mesh_instance_get_underlying_data :: proc(mesh_list: ^Mesh_List, mesh_instance: Mesh_Instance_Index) -> Mesh_Instance {
	return mesh_list.instances[mesh_instance]
}

mesh_list_static_data: struct {
	// @todo: Better name
	descriptor_set_layout: vk.DescriptorSetLayout,
}

mesh_list_initialize_static_data :: proc() {
	using mesh_list_static_data

	descriptor_set_layout = descriptor_set_layout_create({
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .COMPUTE } },
	})
}

mesh_list_deinitialize_static_data :: proc() {
	using mesh_list_static_data

	vk.DestroyDescriptorSetLayout(device, descriptor_set_layout, nil)
}

// MESH LIST
Mesh_List :: struct {
	instances:       []Mesh_Instance,
	gpu_instances:   []Gpu_Mesh_Instance,
	// @todo: Keep around first/last instance in use.

	// Buffer containing all instance data.
	instance_buffer: struct {
		dirty: [MAX_FRAMES_IN_FLIGHT]struct{
			first, last: Mesh_Instance_Index,
		},
		buffers: [MAX_FRAMES_IN_FLIGHT]vk.Buffer,
		memory: vk.DeviceMemory,

		descriptor_pool:       vk.DescriptorPool,
		descriptor_sets:       [MAX_FRAMES_IN_FLIGHT]vk.DescriptorSet,
	},
}

mesh_list_create :: proc(instance_count: int) -> Mesh_List {
	using mesh_list_static_data
	using mesh_list := Mesh_List {}

	fif_count := FIF_COUNT

	instances = make([]Mesh_Instance, instance_count)
	gpu_instances = make([]Gpu_Mesh_Instance, instance_count)
	instance_buffer_size := vk.DeviceSize(size_of(Gpu_Mesh_Instance) * instance_count)

	// Setup instance buffer gpu stuff
	{
		using mesh_list.instance_buffer

		memory = buffer_create_multiple(
			instance_buffer_size,
			{ .TRANSFER_DST, .STORAGE_BUFFER }, // +.TRANSFER_SRC for resize
			{ .DEVICE_LOCAL },
			buffers[:fif_count])

		descriptor_pool = descriptor_pool_create({{ type = .STORAGE_BUFFER }}, fif_count)
		descriptor_sets_allocate(descriptor_pool, descriptor_set_layout, descriptor_sets[:fif_count])

		for fif_idx in 0..<fif_count {
			descriptor_write := vk.WriteDescriptorSet {
				sType           = .WRITE_DESCRIPTOR_SET,
				dstSet          = descriptor_sets[fif_idx],
				dstBinding      = 0,
				descriptorType  = .STORAGE_BUFFER,
				descriptorCount = 1,
				pBufferInfo     = &vk.DescriptorBufferInfo {
					buffer = buffers[fif_idx],
					offset = 0,
					range = instance_buffer_size,
				},
			}

			dirty[fif_idx] = { first = -1, last = -1 }

			vk.UpdateDescriptorSets(device, 1, &descriptor_write, 0, nil)
		}
	}

	return mesh_list
}

mesh_list_mark_dirty :: proc(mesh_list: ^Mesh_List, index: Mesh_Instance_Index) {
	using mesh_list.instance_buffer

	for fif_idx in 0..<FIF_COUNT {
		dirty[fif_idx].first, dirty[fif_idx].last = expand_range(
			dirty[fif_idx].first,
			dirty[fif_idx].last,
			index)
	}
}

mesh_list_destroy :: proc(mesh_list: ^Mesh_List) {
	using mesh_list_static_data

	vk.DestroyDescriptorPool(device, mesh_list.instance_buffer.descriptor_pool, nil)

	for fif_idx in 0..<FIF_COUNT {
		vk.DestroyBuffer(device, mesh_list.instance_buffer.buffers[fif_idx], nil)
	}
	vk.FreeMemory(device, mesh_list.instance_buffer.memory, nil)

	delete(mesh_list.instances)
	delete(mesh_list.gpu_instances)
}

@(private="file")
mesh_list_get_instance_index :: proc(mesh_list: ^Mesh_List) -> Mesh_Instance_Index {
	for instance_idx in 0..<len(mesh_list.instances) {
		if !mesh_list.instances[instance_idx].allocated {
			return Mesh_Instance_Index(instance_idx)
		}
	}

	log.panic("Couldn't get mesh instance index.")
}

mesh_list_flush_instance_data :: proc(mesh_list: ^Mesh_List) {
	using instance_buffer := &mesh_list.instance_buffer

	current_fif := fif_get_current_index()

	first := cast(int)dirty[current_fif].first
	last := cast(int)dirty[current_fif].last

	// if first == -1 then last == -1
	if first == -1 {
		vk_queue_insert_debug_label(graphics_queue.queue, { 1, 1, 1, 1 }, "No instance data to flush")
		return
	}

	for i in first..<last + 1 {
		instance := mesh_list.instances[i]

		bind_range := mesh_vertex_data_get_bind_range(instance.mesh)

		mesh_list.gpu_instances[i] = Gpu_Mesh_Instance {
			flags           = instance.flags,
			index_count     = cast(u32)bind_range.index_count,
			first_index     = cast(u32)bind_range.first_index,
			first_vertex    = cast(i32)bind_range.first_vertex,
			transform_index = instance.transform_index,
			diffuse_texture = instance.diffuse_texture.image_index,
			normal_texture  = instance.normal_texture.image_index,
			allocated       = cast(b32)instance.allocated,
		}
	}

	staging_buffer_copy_to_buffer_sync(
		mesh_list.gpu_instances[first:last+1],
		buffers[current_fif],
		cast(vk.DeviceSize)(first * size_of(M4)))

	dirty[current_fif].first = -1
	dirty[current_fif].last = -1
}
*/
