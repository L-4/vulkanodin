package nt2

import "core:log"

Image_Data :: struct {
	image_index: Texture_Index,
}

// This cache is hard coded to be for images! This file will be made generic
// once the image implementation is completed.

// Resource containing refcount, backing data, and info to fetch from cache.
Image_Resource :: struct {
	// Ref count and cache info should maybe be using'd?
	ref_count: int,
	using data: Image_Data,
	cache_info: Image_Cache_Info, // Maybe should be pImpl to any cache
}

Image_Ref :: struct {
	using ref: ^Image_Resource,
}

// Data required for default loader
Image_File_Info :: struct {
	path: string,
}

// Data describing image created inline, such as default textures.
Image_Inline_Info :: struct {
	name: string,
}

// Union of all structs which describe cache info for an image
Image_Cache_Info :: union {
	Image_File_Info,
	Image_Inline_Info,
}

Image_Cache :: struct {
	cache: map[Image_Cache_Info]^Image_Resource,
}

Image_Cache_Error :: union {
	Png_Load_Error,
	Cache_Error,
}

image_cache_initialize :: proc() {
	caches.image_cache.cache = make(map[Image_Cache_Info]^Image_Resource)
}

image_cache_deinitialize :: proc() {
	for cache_key, resource in caches.image_cache.cache {
		if resource.ref_count > 0 {
			log.warnf("Resource %v still had '%d' references when the cache was destroyed!", cache_key, resource.ref_count)
		}
	}

	delete(caches.image_cache.cache)
}

image_cache_get :: proc(info: Image_Cache_Info) -> (ref: Image_Ref, err: Image_Cache_Error) {
	using caches.image_cache

	if res, ok := cache[info]; ok {
		ref = image_cache_ref(res)

		log.debugf("Loading cached resource %v", info)

		return
	}

	#partial switch info_specfic in info {
		case Image_File_Info: {
			image_index := object_data_buffer_allocate_texture(info_specfic.path)

			ref = image_cache_insert(info, Image_Data { image_index = image_index })

			return
		}

		case: {
			err = .Cannot_Load_With_Info_Type

			return
		}
	}
}

image_cache_insert :: proc(info: Image_Cache_Info, image: Image_Data) -> Image_Ref {
	using caches.image_cache

	assert(!(info in cache))

	log.debugf("Inserting new resource into cache %v", info)

	new_res := new_clone(Image_Resource {
		ref_count = 1,
		data = image,
		cache_info = info,
	})

	cache[info] = new_res
	return Image_Ref { ref = new_res }
}

// @todo: This should use Image_Ref
// @todo: Procedure group ref/unref
image_cache_ref :: #force_inline proc(res: ^Image_Resource) -> Image_Ref {
	res.ref_count += 1
	log.debugf("References++ -> %d refs: %v", res.ref_count, res.cache_info)
	return Image_Ref { ref = res }
}

image_cache_unref :: proc(ref: Image_Ref) {
	using caches.image_cache

	ref.ref_count -= 1

	log.debugf("References-- -> %d refs: %v", ref.ref_count, ref.cache_info)

	if ref.ref_count == 0 {
		log.debugf("Removed resource from cache %v", ref.cache_info)
		object_data_buffer_release_texture(ref.image_index)
		delete_key(&cache, ref.cache_info)
		free(ref.ref)
	}
}
