package nt2

// Raw_Mesh contains actual mesh data
// @todo: To avoid dynamic allocations, maybe have a Static/Dynamic_Raw_Mesh?
Raw_Mesh :: struct($T: typeid) {
	vertices: [dynamic]T,
	indices:  [dynamic]u16,
}

raw_mesh_allocate :: proc($T: typeid, expected_vertices := 128, expected_indices := 128) -> Raw_Mesh(T) {
	return Raw_Mesh(T) {
		vertices = make([dynamic]T, expected_vertices),
		indices = make([dynamic]u16, expected_indices),
	}
}

raw_mesh_free :: proc(using raw_mesh: Raw_Mesh($T)) {
	delete(vertices)
	delete(indices)
}
