package passes

import vk "vendor:vulkan"

import "core:math/linalg"
import "core:math/rand"

import "game:camera"
import "client:renderer"

vk_assert :: renderer.vk_assert
vk_name_object :: renderer.vk_name_object

// 1. Get list of meshes with non transparent shading model
// 2. Setup and draw

Deferred_Pass :: struct {
    renderpass: vk.RenderPass,

    framebuffer: renderer.Framebuffer,
    pipeline:    renderer.Pipeline,
}

deferred_pass_create :: proc() -> Deferred_Pass {
    using renderer.vk_ctx

    deferred_pass := Deferred_Pass {}

    // FRAMEBUFFER PART I
    // @todo: Find better format for all attachments
    color_formats := []vk.Format { .R16G16B16A16_SFLOAT, .R8G8B8A8_UNORM }
    depth_format := vk.Format.D32_SFLOAT

    // @todo: Hardcoded resolution
    deferred_pass.framebuffer = renderer.framebuffer_create_step_one(3840, 2160, color_formats, depth_format)

    // RENDER PASS
    attachment_descriptions_static: [4]vk.AttachmentDescription
    attachment_descriptions := attachment_descriptions_static[:]
    renderer.framebuffer_fill_attachment_descriptions(&deferred_pass.framebuffer, &attachment_descriptions)

    color_attachment_references_static: [4]vk.AttachmentReference
    color_attachment_references := color_attachment_references_static[:]
    renderer.framebuffer_fill_color_attachment_references(&deferred_pass.framebuffer, &color_attachment_references)

    // Note that the depth attachment must have this layout, unless something special is done.
    depth_attachment_reference: vk.AttachmentReference
    renderer.framebuffer_fill_depth_stencil_attachment_reference(&deferred_pass.framebuffer, &depth_attachment_reference)

    subpass := vk.SubpassDescription {
        pipelineBindPoint = .GRAPHICS,
        inputAttachmentCount = 0,
        pInputAttachments = nil,
        colorAttachmentCount = cast(u32)len(color_attachment_references),
        pColorAttachments = raw_data(color_attachment_references),
        pDepthStencilAttachment = &depth_attachment_reference,
    }

    renderpass_info := vk.RenderPassCreateInfo {
        sType = .RENDER_PASS_CREATE_INFO,
        attachmentCount = cast(u32)len(attachment_descriptions),
        pAttachments = raw_data(attachment_descriptions),
        subpassCount = 1,
        pSubpasses = &subpass,
    }

    vk_assert(vk.CreateRenderPass(device, &renderpass_info, nil, &deferred_pass.renderpass))

    // FRAMEBUFFER PART II
    renderer.framebuffer_create_step_two(&deferred_pass.framebuffer, deferred_pass.renderpass)

    // PIELINE 🥧🥧🥧🥧🥧🥧🥧
	vert_shader := renderer.shader_module_create_from_file("shaders/deferred.vert.spv")
	frag_shader := renderer.shader_module_create_from_file("shaders/deferred.frag.spv")
    defer vk.DestroyShaderModule(device, vert_shader, nil)
    defer vk.DestroyShaderModule(device, frag_shader, nil)

    module_infos := renderer.shader_stage_info_create_default(vert_shader, frag_shader)

	push_constant := vk.PushConstantRange {
		offset = 0,
		size = size_of(renderer.Push_Model),
		stageFlags = {.VERTEX,.FRAGMENT},
	}

    deferred_pass.pipeline = renderer.graphics_pipeline_create(module_infos[:],
                                                               {camera.camera_descriptor_set_layout},
                                                               renderer.Vertex_3D,
                                                               &push_constant,
                                                               2,
                                                               deferred_pass.renderpass)

    // NAME OBJECTS

    for attachment_data, idx in deferred_pass.framebuffer.color_attachments {
        vk_name_object(attachment_data.image.image, "Defferred attachment #%d image (%v)", idx, attachment_data.format)
        vk_name_object(attachment_data.image.memory, "Defferred attachment #%d image memory (%v)", idx, attachment_data.format)
    }

    vk_name_object(deferred_pass.framebuffer.depth_stencil_attachment.image.image, "Defferred depth attachment image (%v)", deferred_pass.framebuffer.depth_stencil_attachment.format)
    vk_name_object(deferred_pass.framebuffer.depth_stencil_attachment.image.memory, "Defferred depth attachment image memory (%v)", deferred_pass.framebuffer.depth_stencil_attachment.format)

    vk_name_object(deferred_pass.framebuffer.framebuffer, "Deferred pass framebuffer")
    vk_name_object(deferred_pass.renderpass, "Deferred pass renderpass")

    return deferred_pass
}

deferred_pass_destroy :: proc(deferred_pass: ^Deferred_Pass) {
    using renderer.vk_ctx

    renderer.graphics_pipeline_destroy(deferred_pass.pipeline)

    renderer.framebuffer_destroy(&deferred_pass.framebuffer)
    vk.DestroyRenderPass(device, deferred_pass.renderpass, nil)
}

deferred_pass_run :: proc(deferred_pass: ^Deferred_Pass, active_camera: ^camera.Camera) {
    using renderer.vk_ctx

    renderer.vk_queue_begin_debug_label(graphics_queue, { 1, 0, 0, 1 }, "Deferred pass")

    command_buffer := renderer.command_buffer_allocate()

    vk.BeginCommandBuffer(command_buffer, &vk.CommandBufferBeginInfo { sType = .COMMAND_BUFFER_BEGIN_INFO })

    clear_values := []vk.ClearValue {
        { color = { float32 = { 0, 0, 0, 0, } } }, // Color buffer
        { color = { float32 = { 0, 0, 0, 0, } } }, // Normal buffer
        { depthStencil = { 1, 0 } }, // Depth
    }

    vk.CmdBeginRenderPass(command_buffer, &vk.RenderPassBeginInfo {
        sType = .RENDER_PASS_BEGIN_INFO,
        renderPass = deferred_pass.renderpass,
        framebuffer = deferred_pass.framebuffer.framebuffer,
        // @todo: Hardcoded resolution
        renderArea = vk.Rect2D { offset = vk.Offset2D { x = 0, y = 0 }, extent = vk.Extent2D { width = 3840, height = 2160 } },
        clearValueCount = cast(u32)len(clear_values),
        pClearValues = raw_data(clear_values),
    }, .INLINE)

    vk.CmdBindPipeline(command_buffer, .GRAPHICS, deferred_pass.pipeline.pipeline_handle)

    viewport := vk.Viewport {
		x = 0, y = 0,
		width = 3840, height = 2160,
		minDepth = 0, maxDepth = 1 } // @todo: Is this even right? Seems to be totally ignored.

	scissor := vk.Rect2D { offset = { 0, 0 }, extent = { 3840, 2160 } }

	vk.CmdSetViewport(command_buffer, 0, 1, &viewport)
	vk.CmdSetScissor(command_buffer, 0, 1, &scissor)
	camera.camera_bind_descriptor_set(active_camera, command_buffer, deferred_pass.pipeline.pipeline_layout)

    // Here be the middle of them there scopes

    vk.CmdEndRenderPass(command_buffer)
    vk_assert(vk.EndCommandBuffer(command_buffer))

	submit_info := vk.SubmitInfo {
		sType = .SUBMIT_INFO,
		commandBufferCount = 1,
		pCommandBuffers = &command_buffer,
	}

    vk_assert(vk.QueueSubmit(renderer.vk_ctx.graphics_queue, 1, &submit_info, 0))

    renderer.vk_queue_end_debug_label(graphics_queue)
}
