package nt2
/*
import vk "vendor:vulkan"

MESH_COLLECTION_DESCRIPTOR_POOL_SIZE :: 16

mesh_collection_data: struct {
	compute_set_layout: vk.DescriptorSetLayout,
	compute_set_pool:   vk.DescriptorPool,

	graphics_set_layout: vk.DescriptorSetLayout,
	graphics_set_pool:   vk.DescriptorPool,

	available_descriptor_sets: [MESH_COLLECTION_DESCRIPTOR_POOL_SIZE]bool,
}

mesh_collection_initialize_global_set_layouts :: proc() {
	using mesh_collection_data

	// Compute
	compute_set_layout = descriptor_set_layout_create({
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .COMPUTE } }, // GPU mesh info
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .COMPUTE } }, // Output draws
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .COMPUTE } }, // Output count
	})

	compute_pool_sizes := []vk.DescriptorPoolSize {
		{ type = .STORAGE_BUFFER, descriptorCount = 3 },
	}

	compute_set_pool = descriptor_pool_create(
		compute_pool_sizes,
		MESH_COLLECTION_DESCRIPTOR_POOL_SIZE,
		{ .FREE_DESCRIPTOR_SET })

	// Graphics
	graphics_set_layout = descriptor_set_layout_create({
		{ type = .STORAGE_BUFFER, count = 1, stage_flags = { .FRAGMENT, .VERTEX } }, // GPU mesh info
	})

	graphics_pool_sizes := []vk.DescriptorPoolSize {
		{ type = .STORAGE_BUFFER, descriptorCount = 1 },
	}

	graphics_set_pool = descriptor_pool_create(
		graphics_pool_sizes,
		MESH_COLLECTION_DESCRIPTOR_POOL_SIZE,
		{ .FREE_DESCRIPTOR_SET })

	available_descriptor_sets = { 0..<MESH_COLLECTION_DESCRIPTOR_POOL_SIZE = true }
}

mesh_collection_deinitialize_global_set_layouts :: proc() {
	using mesh_collection_data

	vk.DestroyDescriptorSetLayout(device, compute_set_layout, nil)
	vk.DestroyDescriptorPool(device, compute_set_pool, nil)

	vk.DestroyDescriptorSetLayout(device, graphics_set_layout, nil)
	vk.DestroyDescriptorPool(device, graphics_set_pool, nil)
}

// A mesh collection is a set of GPU resources required to do a complete drawcall
Mesh_Collection :: struct {
	// Contains array of vk.DrawIndexedIndirectCommand
	indirect_command_buffer: vk.Buffer,
	indirect_command_memory: vk.DeviceMemory,

	// Contains single u32 representing how many elements exist in the command array.
	// Atomically incremented from the compute shader
	indirect_count_buffer: vk.Buffer,
	indirect_count_memory: vk.DeviceMemory,

	// Contains array of other mesh data required for rendering.
	mesh_info_buffer: vk.Buffer,
	mesh_info_memory: vk.DeviceMemory,

	// Index into mesh_collection_data.available_descriptor_sets
	descriptor_index: int,
	compute_descriptor_set:  vk.DescriptorSet, // Contains indirect cmd, cmd count, mesh info
	graphics_descriptor_set: vk.DescriptorSet, // Contains only mesh info

	max_count: int,
}

mesh_collection_create :: proc(count: int) -> Mesh_Collection {
	using mesh_collection_data

	// Make sure that we can allocate descriptor sets
	free_descriptor_index := -1
	for is_available, descriptor_index in available_descriptor_sets {
		if is_available {
			free_descriptor_index = descriptor_index
			break
		}
	}

	assert(free_descriptor_index != -1, "Too many mesh collections!")
	available_descriptor_sets[free_descriptor_index] = false

	// Create buffers
	indirect_command_buffer, indirect_command_memory := buffer_create_single(
		cast(vk.DeviceSize)(size_of(vk.DrawIndexedIndirectCommand) * count),
		{ .INDIRECT_BUFFER, .STORAGE_BUFFER },
		{ .DEVICE_LOCAL })

	indirect_count_buffer, indirect_count_memory := buffer_create_single(
		cast(vk.DeviceSize)size_of(u32),
		{ .TRANSFER_DST, .INDIRECT_BUFFER, .STORAGE_BUFFER },
		{ .DEVICE_LOCAL })

	mesh_info_buffer, mesh_info_memory := buffer_create_single(
		cast(vk.DeviceSize)(size_of(Gpu_Mesh_Render_Data) * count),
		{ .STORAGE_BUFFER },
		{ .DEVICE_LOCAL })

	// Allocate and write to descriptor sets
	compute_descriptor_set := descriptor_sets_allocate_single(compute_set_pool, compute_set_layout)
	graphics_descriptor_set := descriptor_sets_allocate_single(graphics_set_pool, graphics_set_layout)

	descriptor_writes := []vk.WriteDescriptorSet {
		{ // Compute set: mesh info
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = compute_descriptor_set,
			dstBinding      = 0,
			descriptorType  = .STORAGE_BUFFER,
			descriptorCount = 1,
			pBufferInfo     = &vk.DescriptorBufferInfo {
				buffer = mesh_info_buffer,
				offset = 0,
				range = vk.DeviceSize(size_of(Gpu_Mesh_Render_Data) * count),
			},
		},
		{ // Compute set: indirect commands
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = compute_descriptor_set,
			dstBinding      = 1,
			descriptorType  = .STORAGE_BUFFER,
			descriptorCount = 1,
			pBufferInfo     = &vk.DescriptorBufferInfo {
				buffer = indirect_command_buffer,
				offset = 0,
				range = vk.DeviceSize(size_of(vk.DrawIndexedIndirectCommand) * count),
			},
		},
		{ // Compute set: indirect count
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = compute_descriptor_set,
			dstBinding      = 2,
			descriptorType  = .STORAGE_BUFFER,
			descriptorCount = 1,
			pBufferInfo     = &vk.DescriptorBufferInfo {
				buffer = indirect_count_buffer,
				offset = 0,
				range = vk.DeviceSize(size_of(u32)),
			},
		},
		{ // Graphics set: mesh info
			sType           = .WRITE_DESCRIPTOR_SET,
			dstSet          = graphics_descriptor_set,
			dstBinding      = 0,
			descriptorType  = .STORAGE_BUFFER,
			descriptorCount = 1,
			pBufferInfo     = &vk.DescriptorBufferInfo {
				buffer = mesh_info_buffer,
				offset = 0,
				range = vk.DeviceSize(size_of(Gpu_Mesh_Render_Data) * count),
			},
		},
	}

	vk.UpdateDescriptorSets(device, cast(u32)len(descriptor_writes), raw_data(descriptor_writes), 0, nil)

	return Mesh_Collection {
		indirect_command_buffer = indirect_command_buffer,
		indirect_command_memory = indirect_command_memory,

		indirect_count_buffer = indirect_count_buffer,
		indirect_count_memory = indirect_count_memory,

		mesh_info_buffer = mesh_info_buffer,
		mesh_info_memory = mesh_info_memory,

		descriptor_index        = free_descriptor_index,
		compute_descriptor_set  = compute_descriptor_set,
		graphics_descriptor_set = graphics_descriptor_set,

		max_count = count,
	}
}

mesh_collection_destroy :: proc(mesh_collection: ^Mesh_Collection) {
	using mesh_collection_data

	vk.DestroyBuffer(device, mesh_collection.indirect_command_buffer, nil)
	vk.FreeMemory(device, mesh_collection.indirect_command_memory, nil)

	vk.DestroyBuffer(device, mesh_collection.indirect_count_buffer, nil)
	vk.FreeMemory(device, mesh_collection.indirect_count_memory, nil)

	vk.DestroyBuffer(device, mesh_collection.mesh_info_buffer, nil)
	vk.FreeMemory(device, mesh_collection.mesh_info_memory, nil)

	vk_assert(vk.FreeDescriptorSets(device, compute_set_pool, 1, &mesh_collection.compute_descriptor_set))
	vk_assert(vk.FreeDescriptorSets(device, graphics_set_pool, 1, &mesh_collection.graphics_descriptor_set))

	assert(available_descriptor_sets[mesh_collection.descriptor_index] == false)
	available_descriptor_sets[mesh_collection.descriptor_index] = true
}
*/
