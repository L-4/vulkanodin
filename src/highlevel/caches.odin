package nt2

Caches :: struct {
	image_cache: Image_Cache,
	// mesh_cache:  Mesh_Cache,
}

caches: ^Caches

caches_initialize :: proc() {
	assert(caches == nil)
	caches = new(Caches)

	image_cache_initialize()
	// mesh_cache_initialize()
}

caches_deinitialize :: proc() {
	image_cache_deinitialize()
	// mesh_cache_deinitialize()

	free(caches)
	caches = nil
}

Cache_Error :: enum {
	Cannot_Load_With_Info_Type,
}
