package nt2
/*
import vk "vendor:vulkan"

mesh_predicate_static_data: struct {
	// The cull shader. It is static, with specialization constants.
	cull_shader: vk.ShaderModule,
	pipeline_layout: vk.PipelineLayout,
}

mesh_predicate_initialize_static_data :: proc() {
	using mesh_predicate_static_data
	cull_shader = shader_module_create_from_file("engine_assets/shaders/cull_instances.comp.spv") // @todo: Put in engine shaders

	set_layouts := []vk.DescriptorSetLayout {
		mesh_list_static_data.descriptor_set_layout,
		mesh_collection_data.compute_set_layout,
	}

	push_range := vk.PushConstantRange {
		stageFlags = { .COMPUTE },
		offset = 0,
		size = cast(u32)size_of(M4),
	}

	layout_info := vk.PipelineLayoutCreateInfo {
		sType                  = .PIPELINE_LAYOUT_CREATE_INFO,
		setLayoutCount         = cast(u32)len(set_layouts),
		pSetLayouts            = raw_data(set_layouts),
		pushConstantRangeCount = 1,
		pPushConstantRanges    = &push_range,
	}

	vk_assert(vk.CreatePipelineLayout(device, &layout_info, nil, &pipeline_layout))
}

mesh_predicate_deinitialize_static_data :: proc() {
	using mesh_predicate_static_data

	vk.DestroyShaderModule(device, cull_shader, nil)
	vk.DestroyPipelineLayout(device, pipeline_layout, nil)
}

Mesh_Predicate_Info :: struct {
	// Whether to use frustum culling.
	// If true, will use Mesh_Predicate_params.frustum
	frustum_cull: b32,

	// Whether required flags should be used.
	// If true, meshes will only pass if they at least have these flags
	use_required_flags: b32,
	required_flags: Mesh_Instance_Flags,

	// Whether disallowed flags should be used.
	// If true, meshes will not pass if they have any of thsese flags
	use_disallowed_flags: b32,
	disallowed_flags: Mesh_Instance_Flags,
}

Mesh_Predicate_Params :: struct {
	frustum: M4,
}

Mesh_Predicate :: struct {
	compute_pipeline:        vk.Pipeline,
}

mesh_predicate_create :: proc(info: Mesh_Predicate_Info) -> Mesh_Predicate {
	using mesh_predicate_static_data

	using predicate := Mesh_Predicate {}

	// Info for now happens to have the same memory layout as the specialization
	// constant data. So we just pass a pointer to it.
	info := info

	// Pipeline
	// CONSTANTS
	// 0 -> frustum_cull: bool
	// 1 -> use_required_flags: bool
	// 2 -> required_flags: u32 // If use_required_flags
	// 3 -> use_disallowed_flags: bool
	// 4 -> disallowed_flags: u32 // if use_disallowed_flags
	specialization_entries := []vk.SpecializationMapEntry {
		{ constantID = 0, offset = 0,  size = 4 },
		{ constantID = 1, offset = 4,  size = 4 },
		{ constantID = 2, offset = 8,  size = 4 },
		{ constantID = 3, offset = 12, size = 4 },
		{ constantID = 4, offset = 16, size = 4 },
	}

	specialization_info := vk.SpecializationInfo {
		mapEntryCount = cast(u32)len(specialization_entries),
		pMapEntries   = raw_data(specialization_entries),
		dataSize      = size_of(Mesh_Predicate_Info),
		pData         = &info,
	}

	shader_stage_info := vk.PipelineShaderStageCreateInfo {
		sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
		stage = {.COMPUTE},
		module = cull_shader,
		pName = "main",
		pSpecializationInfo = &specialization_info,
	}

	pipeline_info := vk.ComputePipelineCreateInfo {
		sType = .COMPUTE_PIPELINE_CREATE_INFO,
		flags = {},
		stage = shader_stage_info,
		layout = pipeline_layout,
	}

	vk_assert(vk.CreateComputePipelines(device, g_pipeline_cache, 1, &pipeline_info, nil, &compute_pipeline))

	return predicate
}

mesh_predicate_destroy :: proc(mesh_predicate: Mesh_Predicate) {
	vk.DestroyPipeline(device, mesh_predicate.compute_pipeline, nil)
}

// @todo: Allow skipping draw entirely with VK_EXT_conditional_rendering
mesh_predicate_collect :: proc(mesh_list: ^Mesh_List, predicate: Mesh_Predicate, params: ^Mesh_Predicate_Params, collect_into: Mesh_Collection) {
	using mesh_predicate_static_data

	current_fif := fif_get_current_index()

	command_buffer := command_buffer_begin_single_use()
	defer command_buffer_end_single_use(command_buffer)

	// @optimize: Using shared memory, the compute shader could batch the draw count per thread group,
	// then atomically write the count to the buffer. This eliminates most atomic calls.
	// Additionally, the thread group size can be set with specialization constants.
	// Try different sizes and see if there's any noticeable difference.

	zero := 0 // This variable represents the numeric value zero. // @todo: Document in detail
	vk.CmdUpdateBuffer(
		command_buffer,
		collect_into.indirect_count_buffer,
		0,
		vk.DeviceSize(size_of(u32)),
		&zero)

	vk.CmdBindPipeline(command_buffer, .COMPUTE, predicate.compute_pipeline)
	vk.CmdPushConstants(command_buffer, pipeline_layout, { .COMPUTE }, 0, cast(u32)size_of(Mesh_Predicate_Params), params)

	sets_to_bind := []vk.DescriptorSet {
		mesh_list.instance_buffer.descriptor_sets[current_fif],
		collect_into.compute_descriptor_set,
	}

	vk.CmdBindDescriptorSets(
		command_buffer,
		.COMPUTE,
		pipeline_layout,
		0,
		cast(u32)len(sets_to_bind),
		raw_data(sets_to_bind),
		0,
		nil)

	// @todo: Set size to maximum valid instance
	vk.CmdDispatch(command_buffer, cast(u32)len(mesh_list.instances), 1, 1);
}
*/
