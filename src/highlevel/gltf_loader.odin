package nt2
/*
import "lib:cgltf"

import "core:strings"
import "core:log"
import "core:path/filepath"

@(private="file")
v3_to_A2B10G10R10_SNORM_PACK32 :: #force_inline proc(rgba: [3]f32) -> u32 {
	b := cast(u32)(clamp(rgba.z, -1, 1) * 511) & 1023
	g := cast(u32)(clamp(rgba.y, -1, 1) * 511) & 1023
	r := cast(u32)(clamp(rgba.x, -1, 1) * 511) & 1023

	return b << 20 | g << 10 | r
}

@(private="file")
v2_to_2u16 :: #force_inline proc(v2: [2]f32) -> [2]u16 {
	return {u16(v2.x * 65535), u16(v2.y * 65535)}
}

@(private="file")
swizzle_gltf_to_nt2 :: #force_inline proc(v: [3]f32) -> [3]f32 {return v.zxy}

@(private="file")
gltf_get_accessor_data_and_stride :: proc(accessor: ^cgltf.accessor) -> (data: uintptr, stride: uintptr) {
	data = cast(uintptr)accessor.buffer_view.buffer.data + cast(uintptr)accessor.buffer_view.offset
	stride = cast(uintptr)accessor.stride

	return
}

@(private="file")
gltf_access_data :: #force_inline proc(data: uintptr, stride: uintptr, index: int, $T: typeid) -> T {
	return (cast(^T)cast(rawptr)(data + stride * cast(uintptr)index))^
}

@(private="file")
raw_mesh_fill_indices :: proc(accessor: ^cgltf.accessor, raw_mesh: Raw_Mesh(Vertex_3D)) {
	data, stride := gltf_get_accessor_data_and_stride(accessor)

	for i in 0..<cast(int)accessor.count {
		raw_mesh.indices[i] = gltf_access_data(data, stride, i, u16)
	}
}

@(private="file")
gltf_fill_raw_mesh_positions :: proc(accessor: ^cgltf.accessor, raw_mesh: Raw_Mesh(Vertex_3D)) {
	data, stride := gltf_get_accessor_data_and_stride(accessor)

	for i in 0..<cast(int)accessor.count {
		extracted := gltf_access_data(data, stride, i, [3]f32)
		swizzled := swizzle_gltf_to_nt2(extracted)
		raw_mesh.vertices[i].position = swizzled
	}
}

@(private="file")
gltf_fill_raw_mesh_texcoords :: proc(accessor: ^cgltf.accessor, raw_mesh: Raw_Mesh(Vertex_3D)) {
	data, stride := gltf_get_accessor_data_and_stride(accessor)

	for i in 0..<cast(int)accessor.count {
		extracted := gltf_access_data(data, stride, i, [2]f32)
		raw_mesh.vertices[i].texcoord = v2_to_2u16(extracted)
	}
}

@(private="file")
gltf_fill_raw_mesh_normals :: proc(accessor: ^cgltf.accessor, raw_mesh: Raw_Mesh(Vertex_3D)) {
	data, stride := gltf_get_accessor_data_and_stride(accessor)

	for i in 0..<cast(int)accessor.count {
		extracted := gltf_access_data(data, stride, i, [3]f32)
		swizzled  := swizzle_gltf_to_nt2(extracted)
		packed    := v3_to_A2B10G10R10_SNORM_PACK32(swizzled)
		raw_mesh.vertices[i].normal = packed
	}
}

@(private="file")
gltf_fill_raw_mesh_tangents :: proc(accessor: ^cgltf.accessor, raw_mesh: Raw_Mesh(Vertex_3D)) {
	data, stride := gltf_get_accessor_data_and_stride(accessor)

	for i in 0..<cast(int)accessor.count {
		extracted := gltf_access_data(data, stride, i, [3]f32)
		swizzled  := swizzle_gltf_to_nt2(extracted)
		packed    := v3_to_A2B10G10R10_SNORM_PACK32(swizzled)
		raw_mesh.vertices[i].tangent = packed
	}
}

Mesh_Attributes :: bit_set[Mesh_Attribute]
Mesh_Attribute :: enum {
	Position,
	Normal,
	Tangent,
	Bitangent,
	TexCoord0,
}

raw_mesh_from_gltf :: proc(cgltf_file_name: string) -> []Raw_Mesh(Vertex_3D) {
	// @todo: Use odin allocator
	options: cgltf.options

	filename_cstr := strings.clone_to_cstring(cgltf_file_name)
	defer delete(filename_cstr)

	data: ^cgltf.data

	if res := cgltf.parse_file(&options, filename_cstr, &data); res != .success {
		log.panicf("Failed importing gltf: %v", res)
	}

	if res := cgltf.load_buffers(&options, data, filename_cstr); res != .success {
		log.panicf("Failed loading buffers: %v", res)
	}

	defer cgltf.free(data)

	gathered_attributes: Mesh_Attributes

	raw_meshes := make([]Raw_Mesh(Vertex_3D), data.meshes_count)

	assert(data.meshes_count >= 1)
	for mesh, mesh_idx in data.meshes[:data.meshes_count] {

		assert(mesh.primitives_count == 1)
		for primitive in mesh.primitives[:mesh.primitives_count] {

			assert(primitive.type == .triangles)
			assert(primitive.indices.component_type == .r_16u, "Invalid index format")
			assert(primitive.attributes_count > 0)

			vertex_count := primitive.attributes[0].data.count
			index_count  := primitive.indices.count

			raw_mesh := raw_mesh_allocate(Vertex_3D, cast(int)vertex_count, cast(int)index_count)

			// Fill indices
			raw_mesh_fill_indices(primitive.indices, raw_mesh)

			// Fill attributes
			for attribute in primitive.attributes[:primitive.attributes_count] {

				assert(attribute.data.count == vertex_count, "Different attributes with different counts!")

				#partial switch attribute.type {
				case .position: {
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec3, "Invalid position attribute!")

					gltf_fill_raw_mesh_positions(attribute.data, raw_mesh)
					gathered_attributes += { .Position }
				}
				case .normal: {
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec3)

					gltf_fill_raw_mesh_normals(attribute.data, raw_mesh)
					gathered_attributes += { .Normal }
				}
				case .tangent: {
					assert(attribute.data.component_type == .r_32f &&
					      (attribute.data.type == .vec3 || attribute.data.type == .vec4)) // @todo: Why are we getting vec4 tangents?

					gltf_fill_raw_mesh_tangents(attribute.data, raw_mesh)
					gathered_attributes += { .Tangent }
				}
				case .texcoord: {
					// @todo: A few other component types make sense... but maybe not in the gltf bin itself?
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec2, "Invalid texcoords attribute!")

					gltf_fill_raw_mesh_texcoords(attribute.data, raw_mesh)
					gathered_attributes += { .TexCoord0 }
				}
				// case .color:
				// case .joints:
				// case .weights:
				case: continue
				}
			}

			log.infof("Loaded model '%s' with attributes %#v", cgltf_file_name, gathered_attributes)

			raw_meshes[mesh_idx] = raw_mesh
		}
	}

	return raw_meshes
}

mesh_asset_from_gltf :: proc(cgltf_file_name: string) -> Mesh_Asset {
	// @todo: Use odin allocator
	options: cgltf.options

	filename_cstr := strings.clone_to_cstring(cgltf_file_name, context.temp_allocator)

	data: ^cgltf.data

	if res := cgltf.parse_file(&options, filename_cstr, &data); res != .success {
		log.panicf("Failed importing gltf: %v", res)
	}

	if res := cgltf.load_buffers(&options, data, filename_cstr); res != .success {
		log.panicf("Failed loading buffers: %v", res)
	}

	defer cgltf.free(data)

	gathered_attributes: Mesh_Attributes

	mesh_data := raw_mesh_allocate(Vertex_3D)

	Sub_Mesh_With_Index :: struct {
		using sub_mesh: Sub_Mesh,
		mesh_index: int, // As we may have multiple primitives per mesh
	}

	gltf_file_folder := filepath.dir(cgltf_file_name)
	defer delete(gltf_file_folder)

	log.error("ff", gltf_file_folder)
	log.error("ff", cgltf_file_name)

	meshes := make([dynamic]Sub_Mesh_With_Index)

	// @todo: Meshes can overlap attribute data, but we don't care.
	assert(data.meshes_count >= 1)
	for mesh, mesh_idx in data.meshes[:data.meshes_count] {

		assert(mesh.primitives_count == 1)
		for primitive in mesh.primitives[:mesh.primitives_count] {

			assert(primitive.type == .triangles)
			assert(primitive.indices.component_type == .r_16u, "Invalid index format")
			assert(primitive.attributes_count > 0)

			vertex_count := primitive.attributes[0].data.count
			index_count  := primitive.indices.count

			material := primitive.material
			assert(bool(material.has_pbr_metallic_roughness))

			// log.errorf("Diffuse texture: '%s'\n", material.pbr_metallic_roughness.base_color_texture.texture.image.uri)
			log.errorf("Diffuse info %#v\n", material.pbr_metallic_roughness.base_color_texture.texture.image)

			// @todo: Assert that we're not using any of the image view parameters.

			append(&meshes, Sub_Mesh_With_Index {
				sub_mesh = Sub_Mesh {

				},
				mesh_index = mesh_idx,
			})

			// Fill indices
			raw_mesh_fill_indices(primitive.indices, mesh_data)

			// Fill attributes
			for attribute in primitive.attributes[:primitive.attributes_count] {

				assert(attribute.data.count == vertex_count, "Different attributes with different counts!")

				#partial switch attribute.type {
				case .position: {
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec3, "Invalid position attribute!")

					gltf_fill_raw_mesh_positions(attribute.data, mesh_data)
					gathered_attributes += { .Position }
				}
				case .normal: {
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec3)

					gltf_fill_raw_mesh_normals(attribute.data, mesh_data)
					gathered_attributes += { .Normal }
				}
				case .tangent: {
					assert(attribute.data.component_type == .r_32f &&
					      (attribute.data.type == .vec3 || attribute.data.type == .vec4)) // @todo: Why are we getting vec4 tangents?

					gltf_fill_raw_mesh_tangents(attribute.data, mesh_data)
					gathered_attributes += { .Tangent }
				}
				case .texcoord: {
					// @todo: A few other component types make sense... but maybe not in the gltf bin itself?
					assert(attribute.data.component_type == .r_32f &&
					       attribute.data.type == .vec2, "Invalid texcoords attribute!")

					gltf_fill_raw_mesh_texcoords(attribute.data, mesh_data)
					gathered_attributes += { .TexCoord0 }
				}
				// case .color:
				// case .joints:
				// case .weights:
				case: continue
				}
			}
		}
	}

	return Mesh_Asset {}
}

// load_gltf :: proc() -> Mesh_Asset {

// }

Sub_Mesh :: struct {
	diffuse_texture: string,
	normal_texture:  string,
	transform:       Transform,

	first_vertex, vertex_count: int,
	first_index, index_count:   int,

	parent: int, // -1 for none.
}

Mesh_Asset :: struct {
	vertices: []Vertex_3D,
	indices:  []u16,
	// The first mesh is the root
	meshes: []Sub_Mesh,
}
*/
