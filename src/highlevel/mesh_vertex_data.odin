package nt2

import vk "vendor:vulkan"

import "core:log"

mesh_vertex_data: ^Mesh_Vertex_Data

INITIAL_VERTEX_BUFFER_SIZE :: size_of(Vertex_3D) * 50000 // @todo: Dunno
INITIAL_INDEX_BUFFER_SIZE :: size_of(u16) * 50000

Mesh_Bind_Range :: struct {
	first_vertex:  int, // All vertices are Vertex_3D. first_vertex == vertex_offset
	vertex_count:  int, // Used only for buffer tracking
	first_index:   int, // All indices are u16
	index_count:   int,
}

@(private="file")
Mesh_Bind_Range_Internal :: struct {
	using bind_range: Mesh_Bind_Range,
	allocated: bool,
}

Mesh_Vertex_Data :: struct {
	vertex_buffer: vk.Buffer,
	vertex_memory: vk.DeviceMemory,
	vertex_buffer_size: vk.DeviceSize,

	index_buffer:  vk.Buffer,
	index_memory:  vk.DeviceMemory,
	index_buffer_size: vk.DeviceSize,

	bind_ranges: [dynamic]Mesh_Bind_Range_Internal,
}

Mesh_Index :: distinct int

mesh_vertex_data_initialize :: proc() {
	assert(mesh_vertex_data == nil)
	mesh_vertex_data = new(Mesh_Vertex_Data)
	using mesh_vertex_data

	// @todo: Name buffers
	vertex_buffer, vertex_memory = buffer_create_single(
		INITIAL_VERTEX_BUFFER_SIZE,
		{ .TRANSFER_DST, .VERTEX_BUFFER },
		{ .DEVICE_LOCAL })

	vertex_buffer_size = INITIAL_VERTEX_BUFFER_SIZE

	index_buffer, index_memory = buffer_create_single(
		INITIAL_INDEX_BUFFER_SIZE,
		{ .TRANSFER_DST, .INDEX_BUFFER },
		{ .DEVICE_LOCAL })

	index_buffer_size = INITIAL_INDEX_BUFFER_SIZE

	bind_ranges = make([dynamic]Mesh_Bind_Range_Internal, 16)
}

DrawIndexedIndirectCommand :: struct {
	indexCount:    u32,
	instanceCount: u32,
	firstIndex:    u32,
	vertexOffset:  i32,
	firstInstance: u32,
}

mesh_vertex_data_deinitialize :: proc() {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	vk.DestroyBuffer(device, vertex_buffer, nil)
	vk.FreeMemory(device, vertex_memory, nil)

	vk.DestroyBuffer(device, index_buffer, nil)
	vk.FreeMemory(device, index_memory, nil)

	delete(bind_ranges)

	free(mesh_vertex_data)
	mesh_vertex_data = nil
}

mesh_vertex_data_bind :: proc(command_buffer: vk.CommandBuffer) {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	offset := vk.DeviceSize(0)
	vk.CmdBindVertexBuffers(command_buffer, 0, 1, &vertex_buffer, &offset)
	vk.CmdBindIndexBuffer(command_buffer, index_buffer, 0, .UINT16)
}

@(private="file")
mesh_vertex_data_resize_vertex_buffer :: proc(required_size: vk.DeviceSize) {
	log.panic("Waaah, I don't know how to resize")
}

@(private="file")
mesh_vertex_data_resize_index_buffer :: proc(required_size: vk.DeviceSize) {
	log.panic("Waaah, I don't know how to resize")
}

// @todo: For now, this just sees if there's enough room on the top, and cries loudly if there isn't.
@(private="file")
mesh_vertex_data_find_bind_range :: proc(vertex_count, index_count: int) -> Mesh_Bind_Range {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	wanted_vertex_space := size_of(Vertex_3D) * vertex_count
	wanted_index_space := size_of(u16) * index_count

	top_vertex_offset, top_index_offset := 0, 0

	for i := len(bind_ranges) - 1; i >= 0; i -= 1 {
		top_bind_range := bind_ranges[i]

		if !top_bind_range.allocated {
			continue
		}

		top_vertex_offset = (top_bind_range.first_vertex + top_bind_range.vertex_count)
		top_index_offset = (top_bind_range.first_index + top_bind_range.index_count)

		break
	}

	top_vertex_offset_size := top_vertex_offset * size_of(Vertex_3D)
	top_index_offset_size := top_index_offset * size_of(u16)

	new_vertex_buffer_size := cast(vk.DeviceSize)(top_vertex_offset_size + wanted_vertex_space)
	if new_vertex_buffer_size > vertex_buffer_size {
		mesh_vertex_data_resize_vertex_buffer(new_vertex_buffer_size)
	}

	new_index_buffer_size := cast(vk.DeviceSize)(top_index_offset_size + wanted_index_space)
	if new_index_buffer_size > index_buffer_size {
		mesh_vertex_data_resize_index_buffer(new_index_buffer_size)
	}

	return Mesh_Bind_Range {
		first_vertex = top_vertex_offset,
		vertex_count = vertex_count,
		first_index  = top_index_offset,
		index_count  = index_count,
	}
}

@(private="file")
mesh_vertex_data_find_index :: proc() -> Mesh_Index {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	for bind_range, mesh_index in bind_ranges {
		if bind_range.allocated {
			continue
		}

		return Mesh_Index(mesh_index)
	}

	next_idx := len(bind_ranges)
	resize(&bind_ranges, cap(bind_ranges) * 2)

	return Mesh_Index(next_idx)
}

mesh_vertex_data_allocate :: proc(raw_mesh: Raw_Mesh(Vertex_3D)) -> Mesh_Index {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	new_bind_range := mesh_vertex_data_find_bind_range(len(raw_mesh.vertices), len(raw_mesh.indices))

	vertex_buffer_upload_offset := cast(vk.DeviceSize)new_bind_range.first_vertex * size_of(Vertex_3D)
	staging_buffer_copy_to_buffer_sync(raw_mesh.vertices[:], vertex_buffer, vertex_buffer_upload_offset)

	index_buffer_upload_offset := cast(vk.DeviceSize)new_bind_range.first_index * size_of(u16)
	staging_buffer_copy_to_buffer_sync(raw_mesh.indices[:], index_buffer, index_buffer_upload_offset)

	mesh_index := mesh_vertex_data_find_index()
	bind_range := &bind_ranges[mesh_index]

	bind_range.bind_range = new_bind_range
	bind_range.allocated = true

	return mesh_index
}

mesh_vertex_data_free :: proc(mesh_index: Mesh_Index) {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	bind_range := &bind_ranges[mesh_index]
	assert(bind_range.allocated)

	bind_range.allocated = false
}

mesh_vertex_data_get_bind_range :: proc(mesh_index: Mesh_Index) -> Mesh_Bind_Range {
	assert(mesh_vertex_data != nil)
	using mesh_vertex_data

	assert(bind_ranges[mesh_index].allocated)

	return bind_ranges[mesh_index].bind_range
}
